# ASDPN3 (research/draft)
A System Dynamics Process(ing) Notation 3

# Description
Celebrating 50th anniversary, Systems Dynamics is a scientific methodology
to understand complex systems over time by using feedback loops.

## Why?
- absence and costs of commercial software
- the motivation to comprehend system dynamics
- lack of simple and easy to use open source software in this area
- a minimalistic language / notation
- alternative to heavy software packages and/or alternative to huge suites
- easy and simple for teaching and education
- readable and processable in future
- self explaining models

## What?
- used software depencies are (YACC OR equivalent) AND Flex AND a C-compiler
- ASDPN3 is a compiler to parse and process System Dynamics models
- preserve knowledge by processing models stored as ASCII files
- model serveral real world problem using well known methodology
- expanding the methodology (i.e. eulers, mean, pi, sin, sum)
- from model to real simulation, to data, to visualization
- information retrieval on system dynamics models
- declarative approach
- one-model-includes-all philosophy (data and model are seen as one unit)
- models are processed to retrieve data (visualize data using i.e. gnuplot)
- models are visualized as directed graphs by using DOT/gv files
- command line interface / tool

## Who?
- educators and students as a target group (academic usage)
- modelers from different disciplines: PHY, PSY, ECO, MED, SOC, ENG
- archeologists / digital preservation
- decision makers
- documentation / historian

## About
System Dynamics was created by Prof. J.W.Forrester during mid 1950's and
gained special interest in 1972 through the publication 'The Limits to 
Growth' by Club of Rome (see below for World3 model criticism).

## Build and Install
See full details in **[./INSTALL.md](./INSTALL.md)**.

## Usage
Get help by running

```console
$ ./build/ASDPN3 -h
```

to list all parameters ASDPN3 can perform.

A model can be parsed and processed by calling ASDPN3 with the parameter p.

```console
$ ./build/ASDPN3 -p ./models/teacup.mdl
```

Hence the model teacup.mdl is parsed and processed. Resulting values can be
written to comma seperated value file by redirecting the output using '>'.

To get a single comma seperated value file into a new export directory
proceed with creating the directory first.

```console
$ mkdir exports
$ ./build/ASDPN3 -p ./models/teacup.mdl > ./exports/teacup.csv
$ cat ./exports/teacups.csv
```

## Example interests.mdl
### Syntax
The model 'interests.mdl' is a simple easy to use and demonstrates the syntax.

```console
mod interests;
stk s_a = 1000;
flo f_b;
par p_i = 0.02;

eqn f_b = s_a * p_i;
eqn s_a = s_a + f_b;
end;

opt interests;                                            # options.
sim steps = 15;
dis s_a;                                                  # display / show vars.
end;                                                      # end options.
```

Proceed with with the following.

#### Processing the model
Issue using the flag '-p' for processing the model.

```console
$ ./build/ASDPN3 -p ./models/interests.mdl
x,s_a
0,1000.000000
1,1020.000000
.
.
54,2913.461426
55,2971.730713
```

Make sure to create the directory 'exports' by running 'mkdir'.

```console
$ mkdir ./exports
```

Before following this step, to save the data as csv file 'interests.csv'.

```console
$ ./build/ASDPN3 -p ./models/interests.mdl > ./exports/interests.csv
```

#### Plot the data
Finally plot the data using the 'plot-interests.sh' script.
```console
$ ./exports/plot-interests.sh
```

#### View the plot
![./plots/interests.svg](./plots/interests.svg "./plots/interests.svg").


# Contributing
At current point in time (2022-05-24T12-33-39Z), the author appreciates
feedback via email. The further development of this software is not set.

Feel free to make contact to the author and please be patient with responses.
Thanks in advance sharing the projects name or link with colleagues in
scientific or educational environments.

# Authors
Copyright (C) 2019-2022 M.E.Rosner; Berlin; Germany
marty[at]rosner[dot]io

# Licenses
## ASDPN3
-  The version 2022Q4 of ASDPN3 is released under MIT License.
## Requirements / Licenses of other software
- C compiler: See various vendors for licenses.
- DOT/gz: Common Public License Version 1.0
- Flex: BSD (BSD-2-Clause)
- Yacc: BSD
- Byacc: is public domain
- gnuplot: 'Gnuplot is freeware in the sense that you don’t have to pay for it.' see:
  + https://github.com/gnuplot/gnuplot/blob/master/Copyright
  + http://www.gnuplot.info/faq/index.html

accessed: 2022-12-21T13-18-02Z.

See full details in **[./LICENSES.md](./LICENSES.md)**.

# Documentation
The code itself is documented. Check these

## Files
- lexer.l
- main.c
- parser.y
- utils.h
- utils.c

in the 'src' directory.

# Acknowledgments
Thanks to my family and friends.

# TLDR;
This project is free research software. It is influenced by System Dynamics.
System Dynamics was created by Prof. J.W. Forrester during the 1950's. The
System Dynamics methodology can be used for modeling complex systems. This
project enables modeling by parsing syntax. Resulting models can be processed
and selected data be saved. Resulting data can be visualized and analyzed.
The project includes a small collection of models. Related concepts and
methods can be found visiting the following links.

# References
see **[./REFERENCES.md](./REFERENCES.md)**.

# Time stamp
2022-11-28T13-35-00Z M.E.Rosner
