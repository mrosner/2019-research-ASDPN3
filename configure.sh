#!/bin/sh

################################################################################
# Name        : ASDPN3/configure.sh                                            #
# Author      : M.E.Rosner                                                     #
# E-Mail      : marty[at]rosner[dot]io                                         #
# Version     : 2022Q3                                                         #
# Copyright   : Copyright (C) 2019-2022 M.E.Rosner; Berlin; Germany            #
# License     : The MIT License                                                #
# Description : A System Dynamics Process(ing) Notation                        #
################################################################################

cat headr.in >  Makefile
echo ""      >> Makefile
./include.sh >> Makefile
echo ""      >> Makefile
cat bodya.in >> Makefile
echo ""      >> Makefile
cat bodyb.in >> Makefile
echo ""      >> Makefile
cat bodyc.in >> Makefile
echo ""      >> Makefile
cat footr.in >> Makefile
