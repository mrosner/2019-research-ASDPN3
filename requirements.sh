#!/bin/sh

################################################################################
# Name        : ASDPN3/requirements.sh                                         #
# Author      : M.E.Rosner                                                     #
# E-Mail      : marty[at]rosner[dot]io                                         #
# Version     : 2022Q3                                                         #
# Copyright   : Copyright (C) 2019-2022 M.E.Rosner; Berlin; Germany            #
# License     : The MIT License                                                #
# Description : A System Dynamics Process(ing) Notation                        #
################################################################################

# as is commands
required='yacc cc flex dot'

echo "Following requirements for building ASDPN3 are met:"
# IFS=' ' read -ra REQCMDS <<< "$required"
# for i in "${REQCMDS[@]}"; do
for i in $required; do
  if ( $(command -v "$i" > /dev/null 2>&1) )
  then
    echo "Found" "	" "$(which $i)"
  else
    notfound="$i $notfound"
  fi
done
echo ""

if [ "${notfound}" != "" ]; then
  echo "Make sure to install missing software, using the operating system's"
  echo "package manager. Frequent package managers are apt-get, dnf, emerge,"
  echo "pacman or pkg <pkg-mgr>. To install run the following command line."
  echo ""
  echo -n "# <pkg-mgr> install"
fi

# IFS=' ' read -ra NOTAVAIL <<< "$notfound"
# for i in "${NOTAVAIL[@]}"; do
for i in $notfound; do
  # echo "# <pkg-mgr> install"
  # echo ""
  # echo "$i" "can not be found!"
  if [ "${i}" = "dot" ];
  then
    echo -n " " "graphviz"
  else
    echo -n " " "${i}"
  fi
done
echo ""

exit
