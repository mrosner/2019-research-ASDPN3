# Requirements / Licenses of other software
## C-Compiler
- See various vendors for licenses.

## DOT/gz
- Company:  AT&T Labs - Research, Florham Park, NJ, 07932, USA
- Authors:  Ellson, John and
            Gansner, Emden R. and
            Koutsofios, Eleftherios and
            North, Stephen C. and
            Woodhull, Gordon
- Editor:   Jünger, Michael and
            Mutzel, Petra
- Title:    Graphviz and Dynagraph ---
            Static and Dynamic Graph Drawing Tools
- BookTitle:Graph Drawing Software
- Year:     2004
- Publisher:Springer Berlin Heidelberg
- Address:  Berlin, Heidelberg
- Pages:    127 - 148
- Abstract: Graphviz is a collection of software for
            viewing and manipulating abstract graphs. It provides
            graph visualization for tools and web sites in
            domains such as software engineering, networking,
            databases, knowledge representation, and 
            bioinformatics. Hundreds of thousands of copies have
            been distributed under an open source license.
- ISBN:     978-3-642-18638-7
- DOI:      10.1007/978-3-642-18638-7_6
- URL:      https://doi.org/10.1007/978-3-642-18638-7_6
- License:  Common Public License Version 1.0
- URL:      https://graphviz.org/license/
- Accessed: 2022-07-29T18-26-06Z

## Flex
- Author:   Vern Paxson (original author), Will Estes
- Title:    Flex - A fast scanner generator
            Version:  2.6.4
            License:  BSD
            Year:     2022
            URL:      https://github.com/westes/flex
            Time:     2022-05-25T15-29-13Z
            URL:      https://github.com/westes/flex/blob/master/COPYING
            Accessed: 2022-05-25T15-30-52Z

## Yacc
- Author:   Stephen C. Johnson
- Title:    Yacc: Yet Another Compiler-Compiler
                  Technical report.
                  Murray Hill, New Jersey: AT&T Bell Laboratories. 32.
- License:  BSD
- Year:     1975
- URL:      http://dinosaur.compilertools.net/yacc/yacc.ps
- Accessed: 2022-05-25T15-41-26Z AND
- URL:      https://www.tuhs.org/cgi-bin/utree.pl?file=V6/usr/source/yacc/source
- Accessed: 2022-05-25T16-03-21Z
  ALSO SEE
- Author:   Stephen C. Johnson
- Title:    Yacc meets C
- Book:     in UNIX around the World, Proc. Spring 1988 EUUG Conference
- Year:     1988
- Pages:    53--57

## gnuplot
- Author:   Thomas Williams, Colin Kelley, Russell Lang,
            Dave Kotz, John Campbell, Gershon Elber,
            Alexander Woo and many others.
- Title:    gnuplot version 5
- License:  Gnuplot is authored by a collection of volunteers, who cannot make
            any legal statement about the compliance or non-compliance of
            gnuplot or its uses. There is no warranty whatsoever. Use at your 
            own risk. Gnuplot is freeware in the sense that you don’t have to
            pay for it.
            You can use or modify gnuplot as you like, however certain
            restrictions apply to further distribution of modified versions.
            Please read and abide by the modification and redistribution terms
            in the Copyright file. Some individual source files are explicitly
            dual-licensed; in those cases alternative terms for redistribution
            of code in that specific file are listed at the head of the file.
            + https://github.com/gnuplot/gnuplot/blob/master/Copyright
            + http://www.gnuplot.info/faq/index.html

# Time stamp
2022-12-21T13-18-02Z M.E.Rosner
