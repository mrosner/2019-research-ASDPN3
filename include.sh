#!/bin/sh

################################################################################
# Name        : ASDPN3/include.sh                                              #
# Author      : M.E.Rosner                                                     #
# E-Mail      : marty[at]rosner[dot]io                                         #
# Version     : 2022Q3                                                         #
# Copyright   : Copyright (C) 2019-2022 M.E.Rosner; Berlin; Germany            #
# License     : The MIT License                                                #
# Description : A System Dynamics Process(ing) Notation                        #
################################################################################

echo 'CD=	' "$(which cd | tail -n1)"
echo 'LS=	' "$(which ls | tail -n1)"
echo 'RM=	' "$(which rm | tail -n1)" ' ' '-Rf'
echo 'MV=	' "$(which mv | tail -n1)"
echo 'MKDIR=	' "$(which mkdir | tail -n1)"
echo 'CP=	' "$(which cp | tail)"

echo 'CC=	' "$(which cc | tail -n1)"
echo 'CFLAGS=	' '-o'

echo 'FLEX=	' "$(which flex | tail -n1)"
echo 'FFLAGS=	' '-8'

# echo 'BISON=	' "$(which bison | tail -n1)"
# echo 'BFLAGS=	' '-d -v'

echo 'YACC=	' "$(which yacc | tail -n1)"
echo 'YFLAGS=	' '-d -v'
