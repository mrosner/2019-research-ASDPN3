# References
## AMPL - Another Mathmatical Programming Language (influence)
- https://ampl.com/ (AMPL homepage; accessed 2021-02-04T16:04:12Z)
- https://en.wikipedia.org/wiki/AMPL (Wikipedia on AMPL; accessed 2021-02-04T16:04:12Z)

## Backus-Naur Form
- http://www.cs.man.ac.uk/~pjj/bnf/bnf.html (Backus Naur Form by Pete Jinks; accessed 2021-02-04T15:04:00Z)
- http://www.cs.man.ac.uk/~pjj/bnf/ebnf.html (BNF / EBNF variants Pascal; C; Basic by Pete Jinks; accessed 2021-02-04T15:03:47Z)
- https://stackoverflow.com/questions/2575044/bnf-vs-ebnf-vs-abnf-which-to-choose/2575064#2575064 (BNF vs EBNF vs ABNF: which to choose?; ask by Jason Bacon; accessed 2021-02-04T15:10:10Z)
- http://dotnet.jku.at/applications/Visualizer/#Simple (EBNF Visualizer project; accessed 2021-02-04T15:12:04Z)
- http://leesei.github.io/bnf/ or https://github.com/leesei ; accessed 2021-02-04T15:19:48Z) (Great Page from leesei!)
- https://www.wikiwand.com/en/Backus%E2%80%93Naur_form (BNF; accessed 2021-02-04T15:19:48Z)
- https://www.wikiwand.com/en/Formal_language (formal language; accessed
    2021-02-04T15:19:48Z)
- https://www.wikiwand.com/en/Context-free_grammar (context free grammar;
    accessed 2021-02-04T15:19:48Z)
- https://www.wikiwand.com/en/Extended_Backus%E2%80%93Naur_form (extended
    Backus-Naur Form; accessed 2021-02-04T15:19:48Z)
- https://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf (ISO standard 14977 Extended
    BNF; accessed 2021-02-04T15:19:48Z)
- https://docs.python.org/3/reference/introduction.html#notation (special BNF
    python; accessed 2021-02-04T15:19:48Z)
- https://tools.ietf.org/html/rfc5234 (Augmented BNF for Syntax
    Specifications: ABNF; RFC; accessed 2021-02-04T15:19:48Z)
- https://www.garshol.priv.no/download/text/bnf.html (BNF and EBNF: What are
    they and how do they work?; accessed 2021-02-04T15:19:48Z)
- http://matt.might.net/articles/grammars-bnf-ebnf/ (The language of
    languages; accessed 2021-02-04T15:19:48Z)
- http://www.cs.cmu.edu/~pattis/misc/ebnf.pdf (Richard E. Pattis homepage;
    Extended Backus-Naur Form; accessed 2021-02-04T15:19:48Z)
- http://www.cs.man.ac.uk/~pjj/bnf/ebnf.html (Pete Jinks homepage on Extended
    Backus-Naur Form; accessed 2021-02-04T15:19:48Z)
- https://www.bottlecaps.de/rr/ui (Gunther Rademacher homepage on ui;
    Railroad Diagram Generator rr; accessed 2021-02-04T15:19:48Z)
- https://github.com/GuntherRademacher/rr (Gunther Rademacher on github;
    Railroad Diagram Generator rr; accessed 2021-02-04T15:19:48Z)
- https://karmin.ch/ebnf/index (Vincent Tscherter homepage; EBNF Parser &
    Syntax Diagram Renderer; accessed 2021-02-04T15:19:48Z)
## C-Programming
- https://en.wikipedia.org/wiki/C_(programming_language) (Wikipedia on
    C-programming language; accessed 2021-02-04T15:45:57Z)
## Documentation style
- https://tu-dresden.de/ing/elektrotechnik/ifa/ressourcen/dateien/richtlinie_wiss_arbeiten/DOXYGEN?lang=en; accessed 2022-03-18T15:58:15Z)

## File in / out operator
- https://www.cyberciti.biz/faq/howto-save-ouput-of-linux-unix-command-to-file/ ; accessed 2022-03-15T14:35:53Z

## Lex & YACC
- http://dinosaur.compilertools.net/ (The Lex & Yacc Page; accessed 2021-02-04T15:53:35Z)
- https://cse.iitkgp.ac.in/~bivasm/notes/LexAndYaccTutorial.pdf (Bivas Mitra homepage; Tom Niemann; Lex & YACC tutorial; accessed 2021-02-04T15:49:39Z)
- https://berthub.eu/lex-yacc/cvs/lex-yacc-howto.html (Bert Hubert; Lex &
    YACC primer / HowTo; accessed 2021-02-04T15:52:57Z)

## Models
- See models themselves - the Author(s).
### System Dynamics
- https://systemdynamics.org/ (System Dynamics Society; accessed
    2021-02-04T15:41:00Z)
- https://en.wikipedia.org/wiki/System_dynamics (Wikipedia on System
    Dynamics; accessed 2021-02-04T15:41:00Z)
- https://en.wikipedia.org/wiki/World3#Criticism_of_the_model (World3
    critcism; accessed 2021-02-04T15:41:00Z)
- https://www.clubofrome2022-05-24T21-05-39Z.org/blog-post/herrington-world-model/
    (World3 model, Gaya Herrington; accessed 2022-05-24T21-05-39Z)

## Other relevant pages
- http://graphml.graphdrawing.org/
- https://www.yworks.com/products/yed
- https://www.w3.org/TR/MathML3/
- https://dlmf.nist.gov/LaTeXML/
- https://www-sop.inria.fr/marelle/tralics/quadrat/tralics-math.xml
- http://hutchinson.belmont.ma.us/tth/mml/
- http://hutchinson.belmont.ma.us/tth/index.html
accessed 2021-02-04T16:37:29Z

## Crafted by
- https://stackoverflow.com/questions/38021348/how-can-i-echo-out-things-without-a-newline
- https://stackoverflow.com/questions/2237080/how-to-compare-strings-in-bash
- https://stackoverflow.com/questions/918886/how-do-i-split-a-string-on-a-delimiter-in-bash

# Time stamp
2022-11-28T13-35-00Z M.E.Rosner
