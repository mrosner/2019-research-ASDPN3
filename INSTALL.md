# Build and install
## Clone the repository
Open a shell and clone the respository into a local directory.

```console
$ git clone https://gitlab.com/mrosner/2019-research-ASDPN3.git
```

To enter the ASDPN3 project folder, change the directory accordingly.

```console
$ cd 2019-research-ASDPN3
```

Making all shell scripts executable will be done by using a wildcard.

```console
$ chmod +x ./*.sh
```

## Requirements
This project depends on software. Access to a package manager or short
<pkg-mgr> might be needed to solve the software dependencies. One might want
to consult the vendors' operating system page.

To check, if the requirements are fulfilled use the script requirements.sh.

```console
$ ./requirements.sh
```

Requirements are met, if all path for required software packages are found.

## Configure
Before the build can be performed make sure to run configure.

```console
$ ./configure.sh
```

Checking for the Makefile will be done using list.

```console
$ ls ./Makefile
```

## Build
To compile the code the requirements are to be met. See section above. Make
will require a generated Makefile (see section Configure).

```console
$ make clean && make build
```

To check for the binary, simply list the build directory or binary file.

```
$ ls ./build/ASDPN3
```

The build results in the 'ASDPN3' binary. Running it with the option flag '-h'
will print the help options.

```
$ ./build/ASDPN3 -h
```

Add this path to your environment variable if needed (PATH=$PATH:./build)
or copy the binary. To clean up the build directory see below.

## Clean up
To clean up the project directory use the following command.

```console
$ make clean
```

The command above will remove the build directory containing object files and
the binary ASDPN3.

# Time stamp
2022-11-28T13-35-00Z M.E.Rosner
