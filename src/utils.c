/*******************************************************************************
* Name        : ASDPN3/src/utils.c                                             *
* Author      : M.E.Rosner                                                     *
* E-Mail      : marty[at]rosner[dot]io                                         *
* Version     : 2022Q4                                                         *
* Copyright   : Copyright (C) 2019-2022 M.E.Rosner; Berlin; Germany            *
* License     : The MIT License                                                *
* Description : A System Dynamics Process(ing) Notation                        *
*******************************************************************************/

 /*!
 *  \file utils.c
 *  \brief a collection of utils; node, linked list, records
 *  \author    M.E.Rosner
 *  \version   2022Q4
 *  \date      2022/10/05
 *  \bug       No known bugs.
 *  \copyright Copyright 2019-2022 M.E.Rosner. All rights reserved.
 *  \license   .
 */
#include "utils.h"

record *p_u_init = NULL;
record *p_u_show = NULL;
record *p_u_eqns = NULL;

list *p_u_AST = NULL;
list *p_u_OPT = NULL;

bool b_u_exit = false;
bool b_u_reset = false;
bool b_u_show_all = false;

bool b_u_inta = false;
bool b_u_dots = false;
bool b_u_pars = false;

char *p_u_buff = NULL;

/*int i_u_exit = 0;
int i_u_inte = 0;

void
set_i_u_exit(int i_exit)
{
  i_u_exit = i_exit;
}

int
get_i_u_exit()
{
  return i_u_exit;
}*/

/*void
set_i_u_inte(int i_inte)
{
  i_u_inte = i_inte;
}

int
get_i_u_inte()
{
  return i_u_inte;
}
*/

/*
 * testing purposes
 *
int main(int argc, char **argv )
{
  float a = 1;
  float b = 2;
  float res = -888.888;

  res = add_f(a, b);
  printf("test-calcs.c; add_f(a, b) %f\n");
}
 *
 */

float
abs_f(float f_val)
{
  float f_res = -888.888888f;
  double d_res = -888.888888;
  d_res = fabs(f_val);
  f_res = (float) d_res;

  return f_res;
}


float
add_f(float f_las, float f_ras)
{
  float res = -888.888888f;
  if (f_las != res && f_ras != res)
  {
    res = f_las + f_ras;
  }
  return res;
}

float
sub_f(float f_lss, float f_rss)
{
  float res = -888.888888f;
  if (f_lss != res && f_rss != res)
  {
    res = f_lss - f_rss;
  }
  return res;
}

float
mul_f(float f_lts, float f_rts)
{
  float res = -888.888888f;
  if (f_lts != res && f_rts != res)
  {
    res = f_lts * f_rts;
  }
  return res;
}

float
div_f(float f_lds, float f_rds)
{
  float res = -888.888888f;
  if (f_lds != res && f_rds != res)
  {
    res = f_lds / f_rds;
  }
  return res;
}

float
lt_f(float f_lds, float f_rds)
{
  if ( f_lds < f_rds )
  {
    /* true */
    return 1.0f;
  }
  else
  {
    /* false */
    return 0.0f;
  }
}

float
gt_f(float f_lds, float f_rds)
{
  if ( f_lds > f_rds )
  {
    /* true */
    return 1.0f;
  }
  else
  {
    /* false */
    return 0.0f;
  }
}

float
min_f(float f_ils, float f_irs)
{
  /*
   * necessary to get the correct
   * minimum from parse tree
   */
  float res, res_default = -888.888888f;
  if ( f_ils == res_default && f_irs != res_default )
  {
    res = f_irs;
  }
  else if ( f_irs == res_default && f_ils != res_default )
  {
    res = f_ils;
  }
  else if ( f_ils <= f_irs )
  {
    res = f_ils;
  }
  else
  {
    res = f_irs;
  }
  return res;
}

float
max_f(float f_als, float f_ars)
{
  float res, res_default = -888.888888f;
  if ( f_als == res_default && f_ars != res_default )
  {
    res = f_ars;
  }
  else if ( f_ars == res_default && f_als != res_default )
  {
    res = f_als;
  }
  else if ( f_als >= f_ars )
  {
    res = f_als;
  }
  else
  {
    res = f_ars;
  }
  return res;
}

float
sigmoid(float x)
{
  float res_default = -888.888888f;
  float S = res_default;
  S = 1.0f / ( 1.0f + powf ( exp(1.0f), -x ) );
  return S;
}

float
sigmoid_OBSOLETE(float t)
{
  float res = -888.888888f;
  res = ( 1.0f / 2.0f ) * ( 1.0f + tanh((t/2.0f)));
  // printf("%i%f\n", res);	// printf
  return res;
}

float
double_s(float x, float a, float b)
{
  /*
   * see:      Double S-Curve Function
   * url:      https://math.stackexchange.com/questions/2136967/double-s-curve-function
   * authors:  profaisal (Q) && John Hughes (A)
   * accessed: 2022-03-28T09-16-09Z mer
   */
  float res = -888.888888f;
  /*
   * printf("utils.c, double_s\n");
   */
  float A = 1.0f / M_PI;
  /*
   * printf("utils.c, double_s, 1.0f / M_PI, %f\n", A);
   */
  float q = 100.0f;
  float r = 100.0f;

  res = ( A * atan( q * (x - a)) + A * atan( r * (x -b)) );
  return res;
}

int
urandom_int()
{
  long int li_val = 0;
  system("od -vAn -N4 -tu4 < /dev/urandom | awk -F' ' '{print $1}' > /tmp/urandom_integer_01");
  FILE *fh;
  fh = fopen("/tmp/urandom_integer_01", "r");
  fscanf(fh, "%li", &li_val);
  fclose(fh);
  /* remove */
  system("rm /tmp/urandom_integer_01");
  return li_val;
  /*
   * much simpler! Make use of:
   * -> srand(clock())  // only call ones for init.
   * -> int r = rand()  // get a random integer.
  */
}

float
random_float() // long int li_urandom)
{
  float f_res;
  /*
   * multiple ways to seed using time or clock
   * srand48(time(NULL));
   * srand48(clock());
   * !in doubt USE LINE ABOVE clock().
   */

  /*
   * initial seed using input from /dev/urandom
   * srand48(li_urandom); is inacceptable.
   */

  /*
   * -> clock() // is to be prefered 2021-12-07T19_40_12Z mer
   */

  srand48(clock());

  /*
   * different (dynamic) values for f_res while run time.
   */

  /* casting double drand48(void) to float */
  f_res = (float)drand48();
  return f_res;
}

float
pi()
{
  return M_PI;
}

float pi_OBSOLETE()
{
  /*
   * Decimal for pi.
   * url:      https://oeis.org/A000796/constant
   * url:      https://oeis.org/A000796
   * accessed: 2022-03-23T11-37-12Z mrosner
   */

  static const char *p_pi = "3.14159265358979323846264338327950288419\
			7169399375105820974944592307816406286208\
			99862803482534211706798214";

  char *p_e_pi;
  const long double ld_pi = strtold(p_pi, &p_e_pi);

  float f_pi = (float) ld_pi;
  return f_pi;
}

float
e()
{
  return exp(1.0f);
}

float
e_OBSOLETE()
{
  /*
   * Decimal expansion of e.
   * Formerly M1727 N0684
   * url:      https://oeis.org/A001113
   * accessed: 2021-11-04T11_02_31Z mer
   */

  static const char *p_eu = "2.71828182845904523536028747135266249775\
			7247093699959574966967627724076630353547\
			59457138217852516642742746";

  char *p_e_eu;
  const long double ld_eu = strtold(p_eu, &p_e_eu);
  /*
   * printf("calcs, ld_e %LE\n", ld_eu);
   */

  float f_e = (float)ld_eu;
  return f_e;
}

float
norm(float x_i, float mu, float sigma)
{
  float f_res__ = -888.888888f;
  float f_count ;
  float f_denom ;

  f_count = exp( (-(x_i-mu)*(x_i-mu)) / ((2*((sigma)*(sigma)))) );
  f_denom = sigma * ( sqrt((2*M_PI)) );

  f_res__ = f_count / f_denom;
  return f_res__;
}

float
norm_OBSOLETE(float x, float mu, float sigma)
{
  /*
   * Decimal for pi.
   * url:      https://oeis.org/A000796
   * accessed: 2022-03-23T11-38-43Z mer
   */

  static const char *p_pi = "3.14159265358979323846264338327950288419\
			7169399375105820974944592307816406286208\
			99862803482534211706798214";

  char *p_e_pi;
  const long double ld_pi = strtold(p_pi, &p_e_pi);

  float f_res__ = -888.888888f;     /* (float)ld_eu; */

  float f_count ;
  float f_denom ;

  f_count = exp( (-(x-mu)*(x-mu)) / ((2*((sigma)*(sigma)))) );
  f_denom = sigma * ( sqrt((2*ld_pi)) );

  f_res__ = f_count / f_denom;

  return f_res__;
}

void
screen(void)
{
  printf("********************************************************************************\n");
  printf("*                                                                              *\n");
  printf("*                                                                              *\n");
  printf("********************************************************************************\n");
  printf("* Name        : ASDPN3                                                         *\n");
  printf("* Author      : M.E.Rosner                                                     *\n");
  printf("* E-Mail      : marty[at]rosner[dot]io                                         *\n");
  printf("* Version     : 2022Q4                                                         *\n");
  printf("* Copyright   : Copyright (C) 2019-2022 M.E.Rosner; Berlin; Germany            *\n");
  printf("* License     : The MIT License                                                *\n");
  printf("* Description : A System Dynamics Process(ing) Notation                        *\n");
  printf("********************************************************************************\n");
  printf("*                                                                              *\n");
  printf("*                                                                              *\n");
  printf("*                                                                              *\n");
  printf("*                                                                              *\n");
  printf("*                                                                              *\n");
  printf("*                                                                              *\n");
  printf("*                                                                              *\n");
  printf("*                                           ..  thank you for using ASDPN3.    *\n");
  printf("*                                                            Have a great day! *\n");
  printf("*                                                                              *\n");
  printf("********************************************************************************\n");

}

bool *b_u_interactive;

/*
char str_tree[8192] = "";
*/

/* several counters to free mem */
int i_make_node = 0;
int i_free_node_OBSOLETE = 0;
int i_make_list = 0;
int i_free_list = 0;
int i_make_array = 0;

/*
 * list pointers to handle
 * make, free, set and get
 * node structures between
 * lexer, parser and utils
 */
list *p_u_nodes;

/*
 * record pointers to handle
 * make, free, set and get
 * node structures between
 * lexer, parser and utils
 */
record *p_u_flex;

/* p_nodes = make_list(NULL, NULL); // see main.. */

node *
make_node(Opera e_opera, char *p_label, float f_value, node *left, node *center, node *right)
{
  node *p_new = (node *) calloc(1, sizeof(node));
  if (p_new != NULL) // memory allocated
  {
    p_new->e_opera = e_opera;
    p_new->p_label = p_label;
    p_new->b_init = false;
    p_new->f_value = f_value;
    p_new->left	= left;
    p_new->center = center;
    p_new->prev	= NULL;    
    p_new->next	= NULL;
    p_new->right = right;

    /*
     * printf("make_node, make	%i", p_new->i_id);
     * printf(" e_opera: %c, p_label: %s\n", p_new->e_opera, p_new->p_label);
     */

    p_new->i_id = i_make_node;
    /*
     * printf("make_node, i_make_node %i\n", i_make_node);
     */
    i_make_node = i_make_node + 1;

    p_u_nodes = (list *) make_list(p_new, p_u_nodes);

    return p_new;
  }
  else
  {
    /*
     * no memory allocated
     */
    printf("Memory overflow in make_node().\n");
    exit(100);
  }
}

void
xprint_enum()
{
  printf("xprint_enum, e_NONE %i\n", e_NONE);
  printf("xprint_enum, e_EXPR %i\n", e_EXPR);
  printf("xprint_enum, e_LEFT %i\n", e_LEFT);
  printf("xprint_enum, e_VAR_ %i\n", e_VAR_);
  printf("xprint_enum, e_ADD_ %i\n", e_ADD_);
  printf("xprint_enum, e_SUB_ %i\n", e_SUB_);
  printf("xprint_enum, e_MUL_ %i\n", e_MUL_);
  printf("xprint_enum, e_DIV_ %i\n", e_DIV_);
  printf("xprint_enum, e_LT__ %i\n", e_LT__);
  printf("xprint_enum, e_GT__ %i\n", e_GT__);
  printf("xprint_enum, e_MOD_ %i\n", e_MOD_);
  printf("xprint_enum, e_MIN_ %i\n", e_MIN_);
  printf("xprint_enum, e_MAX_ %i\n", e_MAX_);
  printf("xprint_enum, e_SUM_ %i\n", e_SUM_);
  printf("xprint_enum, e_SIN_ %i\n", e_SIN_);
  printf("xprint_enum, e_COS_ %i\n", e_COS_);
  printf("xprint_enum, e_MOID %i\n", e_MOID);
  printf("xprint_enum, e_MEAN %i\n", e_MEAN);
  printf("xprint_enum, e_ABS_ %i\n", e_ABS_);
  printf("xprint_enum, e_SQRT %i\n", e_SQRT);
  printf("xprint_enum, e_DEV_ %i\n", e_DEV_);
  printf("xprint_enum, e_BRAC %i\n", e_BRAC);
  printf("xprint_enum, e_STR_ %i\n", e_STR_);
  printf("xprint_enum, e_FLT_ %i\n", e_FLT_);
  printf("xprint_enum, e_AND_ %i\n", e_AND_);
  printf("xprint_enum, e_OPTS %i\n", e_OPTS);
  printf("xprint_enum, e_RAND %i\n", e_RAND);
  printf("xprint_enum, e_RR__ %i\n", e_RR__);
  printf("xprint_enum, e_DN__ %i\n", e_DN__);
  printf("xprint_enum, e_PI__ %i\n", e_PI__);
  printf("xprint_enum, e_EUL_ %i\n", e_EUL_);
  printf("xprint_enum, e_ITE_ %i\n", e_ITE_);
};

void
free_node_OBSOLETE(node *p_tree)
{
  // i_free_node_OBSOLETE = i_free_node_OBSOLETE + 1;
  // printf("free_node_OBSOLETE, free	%i\n", i_free_node_OBSOLETE);

  if(   p_tree->left == NULL &&    \
        p_tree->center == NULL &&  \
        p_tree->right == NULL    )
  {
    if (p_tree->e_opera == e_VAR_ && \
        p_tree->p_label != NULL)  // it's a variable containing a p_label
    {
      free(p_tree->p_label); p_tree->p_label = NULL;
    }
    else if (p_tree != NULL)
    {
      free(p_tree); p_tree = NULL;
    }
  }
  else if(  p_tree->left != NULL && \
            p_tree->center != NULL && \
            p_tree->right != NULL )
  {
    free_node_OBSOLETE(p_tree->left);
    free_node_OBSOLETE(p_tree->center);
    free_node_OBSOLETE(p_tree->right);
    /* if (p_tree != NULL) { free(p_tree); p_tree = NULL; } */
    /* free_node_OBSOLETE(p_tree); */
  }
  else if(  p_tree->left != NULL && \
            p_tree->center == NULL && \
            p_tree->right != NULL )
  {
    free_node_OBSOLETE(p_tree->left);
    free_node_OBSOLETE(p_tree->right);
    /* if (p_tree != NULL) { free(p_tree); p_tree = NULL; } */
    /* free_node_OBSOLETE(p_tree); */
  }
  else if(  p_tree->left == NULL && \
            p_tree->center != NULL && \
            p_tree->right == NULL )
  {
    free_node_OBSOLETE(p_tree->center);
    /*
    if (p_tree->e_opera == 'e' && p_tree->p_label != NULL)	// it's a variable containing a p_label
    {
      printf("free_node_OBSOLETE; p_tree->e_opera == 'e'%s\n", p_tree->p_label);
      free(p_tree->p_label); p_tree->p_label = NULL;
    }
    */
    /* if (p_tree != NULL) { free(p_tree); p_tree = NULL; } */
    /* free_node_OBSOLETE(p_tree); */
  }
  printf("utils.c, free_node_OBSOLETE, i_free_node_OBSOLETE\n");
  i_free_node_OBSOLETE = i_free_node_OBSOLETE + 1;
}

list *
make_list(node *p_tree, list *p_next)
{
  list *p_new = (list *) calloc(1, sizeof(list));
  if (p_new != NULL) // memory allocated
  {
    p_new->center = p_tree;
    p_new->next	= p_next;
    p_new->i_id = i_make_list;
    /*
     * printf("utils.c, make_list, make	%i\n", p_new->i_id);
     */

    i_make_list = i_make_list + 1;
    return p_new;
  }
  else
  {
    exit(100);
    printf("utils.c, make_list(), Memory overflow in make_list().\n");
  }
}

int
xfree_AST_list(list *p_list)
{
  /* do not remove */
  list *p_iter = p_list;
  list *p_curr = NULL;
  while (p_iter->next)
  {
    /*
    printf("utils.c, xfree_AST_list, p_iter->i_id %i\n", p_iter->i_id);
    free(p_iter->center); p_iter->center = NULL;
    */
    p_curr = p_iter;
    p_iter = p_iter->next;
    free(p_curr->center->p_label); p_curr->center->p_label = NULL;
    /*
    free(p_curr->center); p_curr->center = NULL;
    free(p_curr->next); p_curr->next = NULL;
    printf("utils.c, xfree_AST_list, free free_p_AST %i\n", i_free);
    */
    free(p_curr); p_curr = NULL;
    i_free_list = i_free_list + 1;
  }
  /*
   * printf("utils.c, xfree_AST_list, p_iter->i_id %i\n", p_iter->i_id);
   * printf("utils.c, xfree_AST_list, free free_p_AST %i\n", i_free);
   */
  free(p_iter); p_iter = NULL;
  i_free_list = i_free_list + 1;
  /*
  free(p_AST); p_AST = NULL;
  xfree_list(p_AST);
  */
  return 0;
}

/*
 * make_node(): each node is created
 * and stored in a list
 * using make_list() (for details
 * see make_node()).
 * to free a list of nodes use
 * xfree_list(): it cleans up a
 * list of nodes
 * (i.e. xfree_list(p_pp_nodes),
 * see parser.y).
 * xfree_list() could be renamed
 * into xfree_nodes().
 */
int
xfree_list(list *p_first)
{
  list *p_iter = NULL;
  p_iter = p_first;
  list *p_next = NULL;
  list *p_curr = NULL;

  /*
   * print pointers
   * printf("utils.c, xfree_list, p_iter %s\n", p_iter);
   * printf("utils.c, xfree_list, p_iter %p\n", p_iter);
   */

  while ( p_iter->next )
  {
    if ( p_iter->center != NULL )
    {
      /*
       * ..no need to check for p_label
       * if ( p_iter->center->p_label != NULL )
       * {
       *   free(p_iter->center->p_label); p_iter->center->p_label = NULL;
       * }
       */
      free(p_iter->center); p_iter->center = NULL;
    }
    p_curr = p_iter;
    p_iter = p_iter->next;

    /* cleaup p_curr */
    if ( p_curr != NULL )
    {
      free(p_curr); p_curr = NULL;
    }
    /* count free list elements */
    i_free_list = i_free_list + 1;
  }                             /* end while */

  /*
   * cleanup (last) p_iter
   * testing center and next first
   * followed by p_iter itself
   */

  /* freeing the head ( p_list = make_list(NULL, NULL); ) */
  if ( p_iter->center == NULL && p_iter->next == NULL )
  {
    free(p_iter); p_iter = NULL;
    i_free_list = i_free_list + 1;
  }
  else if ( p_iter != NULL )
  {
    if ( p_iter->center != NULL )
    {
      free(p_iter->center); p_iter->center = NULL;
      i_free_list = i_free_list + 1;
    }
    else if ( p_iter->next != NULL )
    {
      free(p_iter->next); p_iter->next = NULL;
      i_free_list = i_free_list + 1;
    }
  }                             /* end if */

  return 0;
}

record *
make_record(char *p_label, float f_value, record *next)
{
  record *p_new  = (record *) calloc(1, sizeof(record));
  p_new->p_label = p_label;
  p_new->f_value = f_value;
  p_new->left    = NULL;        /* there is no LEFT, */
  p_new->center  = NULL;
  p_new->prev    = NULL;  
  p_new->next    = next;
  p_new->right   = NULL;        /* or RIGHT (node).  */
  p_new->e_opera = e_NONE;
  return p_new;
}

record *
double_list_record(record *p_first)
{  
  /* A - B - C - D - F*/
 
  record *p_iter;
  p_iter = p_first;
  record *p_curr;
  p_curr = NULL;
  
  p_first->prev = NULL;
  p_first->next->prev = p_first;
  p_first->next->next->prev = p_first->next;
  
//  while ( p_iter->next )
//  {
//    printf("utils.c, double_list_record, p_iter->p_label: %s\n", p_iter->p_label);
//    p_iter = p_iter->next;
//  }
  // p_iter->prev = p_iter;
//  printf("utils.c, double_list_record, p_iter->p_label: %s\n", p_iter->p_label);

  p_iter = p_first;
  
  while ( p_iter->next )
  {
    // printf("utils.c, double_list_record, p_iter->p_label: %s\n", p_iter->p_label);
 
    p_curr = p_iter;
    p_iter->next->prev = p_curr;
    p_iter = p_iter->next;    
  }
// ORIG  p_iter->prev = p_curr; ORIG
  p_iter->prev = p_curr; // TESTING
  
  return p_first;
}


record *
last_record(record *p_first)
{  
  /* A - B - C - D - F*/
 
  record *p_iter;
  p_iter = p_first;
  // printf("utils.c, last_record\n");
  while ( p_iter->next )
  {
    p_iter = p_iter->next;
    // printf("%s,", p_iter->p_label);
  }
  // printf("%s", p_iter->p_label);
  /*
   * TODO: make sure that there is exist a clean double list
   * containg no (NULL, NULL) elements.
   */
  record *p_last = NULL;
  p_last = p_iter->prev;
  // p_last = p_iter;
  // printf("\n");
  return p_last;
}


int
xfree_records_eqns(record *p_first)
{
// start_here
  record *p_curr = NULL;
  record *p_iter = p_u_eqns;
  while ( p_iter->next )
  {
    p_curr = p_iter;
    p_iter = p_iter->next;

    if (p_curr != NULL)
    {
      // if (p_curr->p_label != NULL)
      //{
        /*
         * printf("utils.c, xfree_records, p_curr->p_label, %s\n", p_curr->p_label);
         */
        // free(p_curr->p_label); p_curr->p_label = NULL;
        /*
         * printf("utils.c, xfree_records, p_curr->p_label, %p\n", p_curr->p_label);
         */
      //}
      /*
       * printf("utils.c, xfree_records, p_curr, %s\n", p_curr);
       */
      free(p_curr); p_curr = NULL;
      /*
       * printf("utils.c, xfree_records, p_curr, %p\n", p_curr);
       */
    }
  }
  if (p_iter !=NULL )
  {
    //if (p_iter->p_label != NULL)
    //{
        /*
         * printf("utils.c, xfree_records, p_iter->p_label, %s\n", p_iter->p_label);
         */
        free(p_iter->p_label); p_iter->p_label = NULL;
        /*
         * printf("utils.c, xfree_records, p_iter->p_label, %p\n", p_iter->p_label);
         */
    //}
    /*
     * printf("utils.c, xfree_records, p_iter, %s\n", p_iter);
     */
    free(p_iter); p_iter = NULL;
    /*
     * printf("utils.c, xfree_records, p_iter, %p\n", p_iter);
     */
  }
  return 0;
}

int
xfree_records(record *p_first)
{
  record *p_iter;
  p_iter = p_first;
  record *p_next = NULL;
  record *p_curr = NULL;
  while ( p_iter->next )
  {
    p_curr = p_iter;
    p_iter = p_iter->next;

    if (p_curr != NULL)
    {
      if (p_curr->p_label != NULL)
      {
        /*
         * printf("utils.c, xfree_records, p_curr->p_label, %s\n", p_curr->p_label);
         */
        free(p_curr->p_label); p_curr->p_label = NULL;
        /*
         * printf("utils.c, xfree_records, p_curr->p_label, %p\n", p_curr->p_label);
         */
      }
      /*
       * printf("utils.c, xfree_records, p_curr, %s\n", p_curr);
       */
      free(p_curr); p_curr = NULL;
      /*
       * printf("utils.c, xfree_records, p_curr, %p\n", p_curr);
       */
    }
  }
  if (p_iter !=NULL )
  {
    if (p_iter->p_label != NULL)
    {
        /*
         * printf("utils.c, xfree_records, p_iter->p_label, %s\n", p_iter->p_label);
         */
        free(p_iter->p_label); p_iter->p_label = NULL;
        /*
         * printf("utils.c, xfree_records, p_iter->p_label, %p\n", p_iter->p_label);
         */
    }
    /*
     * printf("utils.c, xfree_records, p_iter, %s\n", p_iter);
     */
    free(p_iter); p_iter = NULL;
    /*
     * printf("utils.c, xfree_records, p_iter, %p\n", p_iter);
     */
  }

  /*
  if (p_first !=NULL )
  {
    free(p_first); p_first = NULL;
  }
  */

  return 0;
}

bool
is_valid_key(record *p_first, char *p_key)
{
  bool b_contains = false;
  record *p_iter = p_first;
  while (p_iter->next) {
    char *p_some = p_iter->p_label;
    float f_some = p_iter->f_value;
    if ( strcmp(p_key, p_some) == 0 )
    {      
      /*
       * printf("utils.c; is_valid_key, found pair.. :\n");
       * printf("utils.c, is_valid_key, (key, value) : (%s, %f)\n", p_some, f_some);
       */
      b_contains = true;
    }
    p_iter = p_iter->next;
  }
  return b_contains;
}

float
get_value(record *p_first, char *p_key)
{
  float res_default = -888.888888f;
  float res = res_default;
  record *p_iter = p_first;
  while (p_iter->next) {
    char *p_some = p_iter->p_label;
    float f_some = p_iter->f_value;
    if ( strcmp(p_key, p_some) == 0 )
    {
      /*
       * printf("utils.c get_value, found pair.. :\n");
       * printf("utils.c get_value, (key, value) : (%s, %f)\n", p_some, f_some);
       */
      res = f_some;
      break;
    }
    p_iter = p_iter->next;
  }
  /*
   * printf("utils.c, get_value, res %f\n",res);
   */
  return res;
}

record *
del_record(record *p_first, char *p_delete_key)
{
  record *p_iter = p_first;
  record *p_prev = NULL;
  record *p_next = NULL;

  int res = -1;
  char *p_found_key = NULL;
  /*
   * as long, as there is a next one
   */
  while (p_iter->next)
  {
    /*
     * check if the first one has to be deleted
     */

    p_found_key = p_iter->p_label;
    res = strcmp(p_delete_key, p_found_key);

    /*
     * printf("utils.c, del_record, res: %i\n", res);
     */

    /* match in search */
    if ( res == 0 )
    {
      /*
       * printf("utils.c, del_record, res == 0\n");
       * printf("utils.c, del_record, p_found_key: %s\n", p_found_key);
       * printf("utils.c, del_record, p_delete_key: %s\n", p_delete_key);
      */

      if ( p_iter == p_first )
      {
        /* record is first */
        p_first = p_iter->next;
        /*
         * printf("utils.c, del_record, found key: p_iter == p_first\n");
         */
      }
      else
      {
        p_prev->next=p_iter->next; // p_iter = NULL;?
      }

      /* clean up mem for p_iter */
      if ( p_iter->p_label != NULL )
      {
        free(p_iter->p_label); p_iter->p_label = NULL;
      }
      free(p_iter); p_iter = NULL;

      break;
    }
    p_prev = p_iter;
    p_iter = p_iter->next;
  }
  return p_first;
}

record *
get_record(record *p_first, char *p_search_key)
{
  record *p_iter = p_first;
  while (p_iter->next) {
    char *p_found_key = p_iter->p_label;
    int res = strcmp(p_search_key, p_found_key);
    if (res == 0)
    {
      break;
    }
    p_iter = p_iter->next;
  }

  /*
   * printf("utils.c, get_keys, done looking for (key, value) ..\n");
   */

  return p_iter;
}

node *
get_entry(list *p_first, char *p_search)
{
  int found = -1;
  list *p_iter = p_first;
  node *p_return = NULL;
  while (p_iter->next)
  {
    char *p_label = NULL;
    if ( p_iter->center->p_label != NULL)
    p_label = p_iter->center->p_label;

    /*
     * printf("\n");
     * printf("utils.c, get_entry, p_label %p\n", p_label);
     * printf("utils.c, get_entry, p_label %s\n", p_label);
     * printf("\n");
     * printf("utils.c, get_entry, p_search %p\n", p_search);
     * printf("utils.c, get_entry, p_search %s\n", p_search);
     */

    found = strcmp(p_label, p_search);

    /* found node using string compare */
    if ( found == 0 )
    {
        p_return = p_iter->center;

        /*
         * printf("utils.c, get_entry, found: p_iter->center->p_label %s\n", \
         *        p_iter->center->p_label);
         * printf("utils.c, get_entry, found: p_iter->return->p_label %s\n", \
         *        p_return->p_label);
         */

        break;
    }
    p_iter = p_iter->next;
    found = -1;
  }
  return p_return;
}

record *
update_record(record *p_first, char *p_update_key, float f_value)
{
  record *update = get_record(p_first, p_update_key);
  update->f_value = f_value;

  /*
   * printf("utils.c, update_record, new value, %s: %f\n", p_update_key, f_value);
   */

  return p_first;
}

int
get_length(record *p_first)
{
  int len = 0;
  record *p_iter = p_first;
  while (p_iter->next)
  {
    len += 1;
    p_iter = p_iter->next;
  }
    return len;
}

int
get_unsolved(record *p_first)
{
  float res_default = -888.888888f;
  int unsolved = 0;
  record *p_iter = p_first;
  while (p_iter->next)
  {
    if ( p_iter->f_value == res_default )
    {
      unsolved += 1;
    }
    p_iter = p_iter->next;
  }
  return unsolved;
}

void
print_keys(record *p_first)
{
  record *p_last = NULL;
  p_first = double_list_record(p_first);
  p_last = last_record(p_first);

  record *p_iter = p_last;
  while (p_iter->prev)
  {
    // get the label
    char *p_some = p_iter->p_label;

    // check for prev in list
    if ( p_iter->prev->prev != NULL )
    {
      //
      // filter "x" and "steps" from
      // key print as "x" is printed
      // in front of table and steps is
      // not of interest.
      //
      char *p_x = p_iter->prev->p_label;
      // int i_is_x = strcmp("x", p_x);
      int i_is_s = strcmp("steps", p_some);

      if ( p_x != NULL ) // && i_is_x != 0 )
      {
        if ( i_is_s != 0 )                // exclude steps from print
        {
          printf("%s,", p_some);
        }
      }
      else
      {
        printf("%s", p_some);
      }
    }
    else
    {
      int i_is_s = strcmp("x", p_some);
      if ( i_is_s != 0 )
      {
        printf("%s", p_some);             // last element
      }
    }
    p_iter = p_iter->prev;
  }
  printf("\n");
}

/*
void
print_keys(record *p_first)
{
  record *p_last = NULL;
  p_first = double_list_record(p_first);
  p_last = last_record(p_first);

  record *p_iter = p_last;
  while ( p_iter->prev )
  {
    //if ( p_iter->prev->prev )
    //{
    printf("%s,", p_iter->p_label);
    //}
    p_iter = p_iter->prev;
  }
  printf("%s", p_iter->p_label);
  printf("\n");
}
*/

/*void
print_keys(record *p_first)
{
  record *p_iter;
  p_iter = p_first;
  while (p_iter->next)
  {
    // get the label
    char *p_some = p_iter->p_label;

    // check for next in list
    if ( p_iter->next->next != NULL )
    {
      //
      // filter "x" and "steps" from
      // key print as "x" is printed
      // in front of table and steps is
      // not of interest.
      //
      char *p_x = p_iter->next->p_label;
      int i_is_x = strcmp("x", p_x);
      int i_is_s = strcmp("steps", p_some);

      if ( p_x != NULL && i_is_x != 0 )
      {
        if ( i_is_s != 0 )
        {
          printf("%s,", p_some);
        }
      }
      else
      {
        printf("%s", p_some);
      }
    }
    else
    {
      int i_is_s = strcmp("x", p_some);
      if ( i_is_s != 0 )
      {
        printf("%s", p_some);
      }
    }
    p_iter = p_iter->next;
  }
  printf("\n");
}*/



void
print_values(record *p_first)
{
  record *p_last = NULL;
  p_first = double_list_record(p_first);
  p_last = last_record(p_first);

  record *p_iter = p_last;
  while (p_iter->prev)
  {
    // get the value
    float f_some = p_iter->f_value;
    char *p_some = p_iter->p_label;

    int i_is_x = strcmp("x", p_some);
    int i_is_s = strcmp("steps", p_some);

    // check for next in list
    if ( p_iter->prev->prev != NULL )
    {
      if ( i_is_x == 0 )
      {
        printf("%i,", (int)f_some);
      }
      else
      {
        printf("%f,", f_some);
      }
    }
    else
    {
      if ( i_is_x == 0 )
      {
        printf("%i", (int)f_some);
      }
      else
      {
        printf("%f", f_some);
      }
    }
    p_iter = p_iter->prev;
  }
  printf("\n");
}

/*
void
print_values(record *p_first)
{
  record *p_iter;
  p_iter = p_first;
  while (p_iter->next)
  {
    // get the value
    float f_some = p_iter->f_value;
    char *p_some = p_iter->p_label;

    // check for next in list
    if ( p_iter->next->next != NULL )
    {
      //
      // filter "x" and "steps" from
      // key print as "x" is printed
      // in front of table and steps is
      // not of interest.
      //
      char *p_x = p_iter->next->p_label;
      int i_is_x = strcmp("x", p_x);
      int i_is_s = strcmp("steps", p_some);

      if ( p_x != NULL && i_is_x != 0 )
      {
        if ( i_is_s != 0 )
        {
          printf("%f,", f_some);
        }
      }
      else
      {
        printf("%f", f_some);
      }
    }
    else
    {
      int i_is_s = strcmp("x", p_some);
      if ( i_is_s != 0 )
      {
        printf("%f", f_some);
      }
    }
    p_iter = p_iter->next;
  }
  printf("\n");
}
*/


/* TODO: add new param delimiter ( '\t', ',', '-' )
void
print_values(record *p_first)
{
  record *p_iter;
  p_iter = p_first;
  while (p_iter->next) {
    float f_some = p_iter->f_value;
    char *p_some = p_iter->p_label;

    // avoid ending semicolon
    if (p_iter->next->next != NULL)
    {

      //
      // printf("%f\t", f_some);

      //   do not print "steps" values
      if ( strcmp("steps", p_some) != 0 )
      {
        printf("%f,", f_some);
      }
    }
    else
    {
      printf("%f", f_some);
    }
    p_iter = p_iter->next;
  }
  printf("\n");
}
*/


void
print_inta_values(record *p_first)
{
  // printf("utils.c, print_inta_values, b_u_inta: %i\n", b_u_inta);
  record *p_iter;
  p_iter = p_first;
  while (p_iter->next)
  {
    float f_some = p_iter->f_value;
    char *p_some = p_iter->p_label;

    /* print out each variable value */
    printf("%s = %f\n", p_some, f_some);

    p_iter = p_iter->next;
  }
}


float *opt1(int count) {
    float *p_f_arr = (float *) calloc(count, sizeof(float));
    if(!p_f_arr)
        return NULL;

    for(int i = 0; i < count; ++i)
        p_f_arr[i] = i*1.0f;

    return p_f_arr;
}

/* record to array */
float *
make_array(record *p_first)
{
  /* get length for records */
  int i_len = -1;
  i_len = get_length(p_first);

  float *p_f_arr = calloc(i_len, sizeof(float));
  if(!p_f_arr)
  {
    p_f_arr = NULL;
  }

  int i_index = 0;

  record *p_iter;
  p_iter = p_first;
  while (p_iter->next)
  {
    float f_val = p_iter->f_value;
    p_f_arr[i_index] = f_val;

    /*
     * printf("utils.c, make_array, while %f\n", f_val);
     */

    p_iter = p_iter->next;
    i_index = i_index + 1;
  }

  /*
   * a test print
   * for (int i=0; i<i_index; i++)
   * {
   *   printf("utils.c, make_array, for %f\n", p_f_arr[i]);
   * }
   */

  i_make_array = i_make_array + 1;
  return p_f_arr;
}

int xfree_array(float *p_f_arr)
{
  free(p_f_arr);  p_f_arr = NULL;

  i_make_array = i_make_array - 1;
  return 0;
}

bool
is_a_value(node *p_tree)
{
  bool is_value = false;
  if(p_tree->left == NULL && p_tree->center == NULL && p_tree->right == NULL)
  {
    is_value = true;
  }
  return is_value;
}

float
eval(node *p_tree, record *p_first)
{
  /*
   * printf("utils.c, eval; p_tree->p_opera:: %s\n", p_tree->p_opera);
   * printf("utils.c, eval; p_tree->p_label:: %s\n", p_tree->p_label);
   * printf("utils.c, eval; p_tree->f_value:: %f\n", p_tree->f_value);
   */

  /* default value */
  float res_default = -888.888888f;
  float res = res_default;
  float res_negate = res_default;
  float res_bracket = res_default;
  float res_sum = res_default;

  /*
   * float res_node = res_default;
   * float res_function = res_default;
   * float f_center = res_default;
   */

  /* floats for expressions */
  float f_las = res_default;
  float f_ras = res_default;
  float f_lss = res_default;
  float f_rss = res_default;
  float f_lts = res_default;
  float f_rts = res_default;
  float f_lds = res_default;
  float f_rds = res_default;

  float f_ils = res_default;
  float f_irs = res_default;
  float f_als = res_default;
  float f_ars = res_default;

  float f_sls = res_default;
  float f_srs = res_default;

  float f_zrs = res_default;

  float f_vrs = res_default;

  float f_sin = res_default;
  float f_cos = res_default;
  float f_moi = res_default;
  float f_abs = res_default;
  float f_srt = res_default;

  Opera got_op = e_NONE;

  char *p_label = NULL;

  /*
   * tree is a single node
   * printf("utils.c, eval, xprint_node(p_tree)\n");
   * xprint_node(p_tree);
   */

  /* single node, i.e. node contains float value */
  if( p_tree->left == NULL && \
      p_tree->center == NULL && \
      p_tree->right == NULL )
  {
    /*
     * printf("utils.c, eval\n");
     * printf("utils.c, eval, p_tree->p_opera: %s\n", p_tree->p_opera);
     */
    got_op = p_tree->e_opera;
    /*
     * printf("utils.c, eval, p_tree->e_opera, %c\n", got_op);
     */
    switch(got_op)
    {
      case e_FLT_:
        /*
         * node contains a float value
         * printf("utils.c, eval, e_FLT_\n");
         * printf("utils.c, eval, e_FLT_, p_tree->p_label: %s\n", p_tree->p_label);
         * printf("utils.c, eval, e_FLT_, f_value%f\n", p_tree->f_value);
         */

        /* f_center = eval(p_tree->center, p_first); */

        res = p_tree->f_value;
        break;
      case e_VAR_:
        /* node contains variable only (what's the value for it) */
        /*
         * printf("utils.c, eval, e_VAR_\n");
         * printf("utils.c, eval, e_VAR_, eval(p_tree->center)\n");
         * printf("utils.c, eval, e_VAR_, p_tree->p_label: %s\n", p_tree->p_label);
         * bracket expression.. TODO.
         */

        f_vrs = get_value(p_first, p_tree->p_label);
        res = f_vrs;

        /*
         * printf("utils.c, eval, e_VAR_, res: %f\n", res);
         */

        break;
      case e_EXPR:
        break;
      case e_LEFT:
        break;
      case e_ADD_:
        break;
      case e_SUB_:
        break;
      case e_MUL_:
        break;
      case e_DIV_:
        break;
      case e_LT__:
        break;
      case e_GT__:
        break;
      case e_MOD_:
        break;
      case e_SUM_:
        break;
      case e_MIN_:
        break;
      case e_MAX_:
        break;
      case e_SIN_:
        break;
      case e_COS_:
        break;
      case e_MOID:
        break;
      case e_STR_:
        break;
      case e_BRAC:
        break;
      case e_NONE:
        break;
      case e_AND_:
        break;
      case e_OPTS:
        break;
      case e_RAND:
        break;
      case e_MEAN:
        break;
      case e_SQRT:
        break;
      case e_DEV_:
        break;
      case e_RR__:
        break;
      case e_DN__:
        break;
      case e_PI__:
        break;
      case e_EUL_:
        break;
      case e_ABS_:
        break;
      case e_ITE_:
        break;
    }                           /* end switch */
  }                             /* end if */
  /*
   * center node only
   * handles expressions of type e_EXPR only
   * exceptions are e_SUB_ types
   */
  if( p_tree->left == NULL && \
      p_tree->center != NULL && \
      p_tree->right == NULL )
  {
    got_op = p_tree->e_opera;

    /*
     * printf("utils.c, eval; %c\n", got_op);
     */

    switch(got_op)
    {
      case e_SUB_:
        res_negate = eval(p_tree->center, p_first);

        /*
         * printf("utils.c, eval, e_SUB_\n");
         * negate expression
         * printf("utils.c, eval, res_negate: %f\n", res_negate);
         */

        res = -1.0f * res_negate;
        break;
      case e_EXPR:

        /*
         * node contains single node (leading to an expr)
         * printf("utils.c, eval, e_EXPR\n");
         * printf("utils.c, eval, e_EXPR, p_tree->p_label: %s\n", p_tree->p_label);
         */

        res = eval(p_tree->center, p_first);
        break;
      case e_VAR_:
        break;
      case e_FLT_:
        break;
      case e_LEFT:
        break;
      case e_ADD_:
        break;
      case e_MUL_:
        break;
      case e_DIV_:
        break;
      case e_LT__:
        break;
      case e_GT__:
        break;
      case e_MOD_:
        break;
      case e_SUM_:
        break;
      case e_MIN_:
        break;
      case e_MAX_:
        break;
      case e_SIN_:
        break;
      case e_COS_:
        break;
      case e_MOID:
        break;
      case e_STR_:
        break;
      case e_BRAC:
        break;
      case e_NONE:
        break;
      case e_AND_:
        break;
      case e_OPTS:
        break;
      case e_RAND:
        break;
      case e_MEAN:
        break;
      case e_SQRT:
        break;
      case e_DEV_:
        break;
      case e_RR__:
        break;
      case e_DN__:
        break;
      case e_PI__:
        break;
      case e_EUL_:
        break;
      case e_ABS_:
        break;
      case e_ITE_:
        break;
    }                           /* end switch */
  }                             /* end if */
  /*
   * left and right nodes,
   * having content,
   * no center node
   */
  if( p_tree->left != NULL && \
      p_tree->center == NULL && \
      p_tree->right != NULL )
  {
    got_op = p_tree->e_opera;
    switch(got_op)
    {
      case e_MIN_:
        /*
         * minimum, i.e. min(val1, val2);
         * printf("utils.c, eval, e_MIN_\n");
         * printf("utils.c, eval, e_MIN_, before manipulation,          \
         *         p_tree->left->e_opera %c\n", p_tree->left->e_opera);
         * printf("utils.c, eval, e_MIN_, p_tree->left->p_label %s\n",  \
         *         p_tree->left->p_label);
         */

        /*
         * parse tree manipulation
         * changes e_opera from e_EXPR to e_MIN_
         * to ensure the evaluation of the
         * correct minimum (recursively)
         * TODO: refactor manipulation for e_opera
         */
        if ( p_tree->left->e_opera != e_EXPR )
        {
          p_tree->left->e_opera = e_MIN_;
        }
        if ( p_tree->right->e_opera != e_EXPR )
        {
          p_tree->right->e_opera = e_MIN_;
        }

        /*
         * printf("utils.c, eval, e_MIN_, after manipulation,           \
         *         p_tree->left->e_opera %c\n", p_tree->left->e_opera);
         */

        f_ils = eval(p_tree->left, p_first);
        f_irs = eval(p_tree->right, p_first);

        /*
         * res = min_f(f_ils, f_irs);
         * printf("utils.c, eval, minimum 'f_ils' %f\n", f_ils);
         * printf("utils.c, eval, minimum 'f_irs' %f\n", f_irs);
         */

        res = min_f(f_ils, f_irs);

        /*
         * printf("utils.c, eval, minimum; result, %f\n", res);
         */

        break;
      case e_MAX_:
        /*
         * maximum max(val1, val2);
         * printf("utils.c, eval, maximum 'a'\n");
         * printf("utils.c, eval, before manipulation,                 \
         *         p_tree->left->e_opera %c\n", p_tree->left->e_opera);
         * printf("utils.c, eval, before manipulation,                 \
         *         p_tree->left->p_label %s\n", p_tree->left->p_label);
         */
        if ( p_tree->left->e_opera != e_EXPR )
        {
          p_tree->left->e_opera = e_MAX_;
        }
        if ( p_tree->right->e_opera != e_EXPR )
        {
          p_tree->right->e_opera = e_MAX_;
        }

        /*
         * printf("utils.c, eval, after manipulation,                   \
         *         p_tree->left->e_opera %c\n", p_tree->left->e_opera);
         */

        f_als = eval(p_tree->left, p_first);
        f_ars = eval(p_tree->right, p_first);

        /*
         * res = max_f(f_ils, f_irs);
         * printf("utils.c, eval, maximum, f_als: %f\n", f_als);
         * printf("utils.c, eval, maximum, f_ars: %f\n", f_ars);
         */
        res = max_f(f_als, f_ars);

        /*
         * printf("eval, maximum; result, %f\n", res);
         */

        break;
      case e_SUM_:
        /*
         * sum, SUM(1, 2, 3);
         */
        if ( p_tree )
        {
          /*
           * printf("utils.c, eval, switch, e_SUM_, compiler expression.\n");
           */
        }
        int i_s_num = 0;
        node *p_p_s_it = p_tree;
        record *p_f_s_init = NULL; // = p_f_init;
        p_f_s_init = make_record(NULL, 0.0f, NULL);
        record *p_f_s_it_a = NULL;
        p_f_s_it_a = p_f_s_init;
        /*
         * TODO: refactor to (why) records should use arrays directly
         */
        while ( p_p_s_it->left != NULL )
        {
          if ( p_p_s_it->left->right != NULL)
          {
            float f_ev_a = res_default;
            f_ev_a = eval(p_p_s_it->left->right->left, p_first);
            /*
             * printf("utils.c, eval, e_SUM_, f_ev_a: %f\n", f_ev_a);
             */
            p_f_s_it_a = make_record(NULL, f_ev_a, p_f_s_it_a);
            i_s_num = i_s_num + 1;              /* array / record length */
          }
          p_p_s_it = p_p_s_it->left;
        }
        float f_ev_s_b = res_default;
        f_ev_s_b = eval(p_p_s_it, p_first);
        if ( f_ev_s_b != res_default )
        {
          /*
           * printf("utils.c, eval, f_ev_s_b:: %f\n", f_ev_s_b);
           */
           p_f_s_it_a = make_record(NULL, f_ev_s_b, p_f_s_it_a);
           i_s_num = i_s_num + 1;
        }
        float *p_f_s_arr = NULL;
        /*
         * p_f_s_it_a - a record structure
         * p_f_s_arr  - an array base on record
         * TODO: more intense use of make_array()
         */
        p_f_s_arr = make_array(p_f_s_it_a);
        int i_s_len = i_s_num;                  /* sizeof(p_f_arr); */
        /*
         * for ( int i=0; i<i_s_len; i++ )
         * {
         *   printf("utils.c, eval, p_f_s_arr[i]: %f\n", p_f_s_arr[i]);
         * }
         */
        float f_sum = -0.0f;

        f_sum = sum(p_f_s_arr, i_s_len);

        /*
         * printf("utils.c, f_sum %f\n", f_sum);
         */

        res = f_sum;

        /*
         * clean up memory
         * free float array (p_f_s_arr)
         * free records (p_f_s_it_a)
         */

        xfree_array(p_f_s_arr);
        xfree_records(p_f_s_it_a);
        break;
      case e_SIN_:
        /*
         * sinus, sin(x);
         */
        if ( p_tree )
        {
          /*
           * printf("utils.c, eval, switch, e_SIN_, compiler expression.\n");
           */
        }
        f_sin = eval(p_tree->left, p_first);
        if ( f_sin != res_default )
        {
          res = sin(f_sin);
        }
        else
        {
          res = res_default;
        }
        /*
         * printf("utils.c, eval, e_SIN_\n")
         */
        break;
      case e_COS_:
        /* cosinus, cos(x) */
        f_cos = eval(p_tree->left, p_first);
        if ( f_cos != res_default )
        {
          res = cos(f_cos);
        }
        else
        {
          res = res_default;
        }
        /*
         * lines left statement $<anode>2 only!
         */
        break;
      case e_MOID:
        /*
         * sigmoid(x)
         */
        f_moi = eval(p_tree->left, p_first);
        /*
         * printf("utils.c, eval, e_MOID, f_moi: %f\n", f_moi);
         */
        if ( f_moi != res_default )
        {
          res = sigmoid(f_moi);
          /*
           * printf("utils.c, eval, e_MOID, res: %f\n", res);
           */
        }
        else
        {
          res = res_default;
        }
        break;
      case e_MEAN:
        /*
         * mean, mean(1, 2, 3) == 2;
         *
         * calculates the mean(x,y,z) using
         * sum(x,y,z) / number_of_paramters(x,y,z)
         * res = sum(x,y,z) / i_stel
         * res = res / i_s_m;
         * printf("eval, sum_of, f_res, %f\n", res);
         */
        if ( p_tree )
        {
          /*
           * printf("utils.c, eval, e_MEAN, compiler expression.\n");
           */
        }
        int i_m_num;
        i_m_num = 0;
        node *p_p_m_it = p_tree;
        record *p_f_m_init = NULL;
        p_f_m_init = make_record(NULL, 0.0f, NULL);
        record *p_f_m_it_a = NULL;
        p_f_m_it_a = p_f_m_init;
        /* TODO: refactor to (why) records should use arrays directly */
        while ( p_p_m_it->left != NULL )
        {
          if ( p_p_m_it->left->right != NULL)
          {
            /*
             * printf("utils.c, eval, e_MEAN, f_ev: %f\n", f_ev);
             */
            float f_ev_a = res_default;
            f_ev_a = eval(p_p_m_it->left->right->left, p_first);
            /*
             * printf("utils.c, eval, e_MEAN, f_ev_a: %f\n", f_ev_a);
             */

            /* add new record */
            p_f_m_it_a = make_record(NULL, f_ev_a, p_f_m_it_a);
            /* array length */
            i_m_num = i_m_num + 1;
          }
          p_p_m_it = p_p_m_it->left;
        }
        float f_ev_m_b = res_default;
        f_ev_m_b = eval(p_p_m_it, p_first);
        if ( f_ev_m_b != res_default )
        {
          /*
           * printf("utils.c, eval, f_ev_m_b: %f\n", f_ev_m_b);
           */
          p_f_m_it_a = make_record(NULL, f_ev_m_b, p_f_m_it_a);
          i_m_num = i_m_num + 1;
        }
        float *p_f_m_arr = NULL;
        p_f_m_arr = make_array(p_f_m_it_a);

        int i_m_len = i_m_num;            /* sizeof(p_f_arr); */

        /*
         * for ( int i=0; i<i_m_len; i++ )
         * {
         *   printf("utils.c, p_arr_a[0], %f\n", p_f_m_arr[i]);
         * }
         */

        float f_mean = -0.0;

        f_mean = mean(p_f_m_arr, i_m_len);
        /*
         * printf("utils.c, f_sum %f\n", f_sum);
         */
        res = f_mean;

        /*
         * free float array (p_f_m_arr)
         * free records (p_f_m_it_a)
         */

        /* clean up memory */
        xfree_array(p_f_m_arr);
        xfree_records(p_f_m_it_a);
        break;
      case e_ABS_:
        /*
         * absolute value abs(x)
         */
        f_abs = eval(p_tree->left, p_first);
        /*
         * printf("eval, f_moi, %f\n", f_moi);
         */
        if ( f_abs != res_default )
        {
          res = abs_f(f_abs);
          /*
           * printf("eval, res, %f\n", res);
           */
        }
        else
        {
          res = res_default;
        }
        break;
      case e_SQRT:
        /*
         * squareroot sqrt(x)
         */
        f_srt = eval(p_tree->left, p_first);
        /*
         * printf("eval, f_moi, %f\n", f_moi);
         */
        if ( f_srt != res_default )
        {
          res = sqrt(f_srt);
          /*
           * printf("eval, res, %f\n", res);
           */
        }
        else
        {
          res = res_default;
        }
        break;
      case e_DN__:
        /*
         * general normal distribution
         * utils.h, norm(float x, float mu, float variance);
         */
        if ( p_tree )
        {
        /*
         * printf("utils.c, eval, e_DN__, compiler expression.\n");
         */
        }
        float f_x;
        f_x = -888.888888f;

        /*
         * evaluate x
         */
        f_x = eval(p_tree->left, p_first);
        /*
         * printf("utils.c, eval, e_DN__, f_value: left %f\n", f_x);
         * printf("utils.c, eval, e_DN__, f_value: left %f\n",                \
                   p_tree->f_value);
         */

        /*
         * evaluate mu
         */
        float f_mu;
        f_mu = -888.888888f;
        f_mu = eval(p_tree->left->left->right->left, p_first);
        /*
         * printf("utils.c, eval, e_DN__, f_value:                            \
         *         left left right left %f\n", f_muuu);
         */

        /*
         * evaluate variance
         */
        float f_var;
        f_var = -888.888888f;
        f_var = eval(p_tree->left->right->left, p_first);
        /*
         * printf("utils.c, eval, e_DN__, f_value:                            \
         *         left right left %f\n", f_var_);
         */

        res = norm(f_x, f_mu, f_var);
        break;
      case e_ITE_:
        /*
         * if then else, if-then-else(1>2, 4, 8);
         */
        res = res_default;
        /*
         * get evaluate expression.
         * if true: 1.0f; else false: 0.0f.
         */
        /*
         * xprint_tree(p_tree);
         */
        float f_bool = res_default;
        f_bool = eval(p_tree->left->left->right, p_first);
        /* printf("utils.c, eval, e_ITE__, f_bool: %f\n", f_bool); */
        if ( f_bool == 1.0f )
        {
          float f_true;
          f_true = -888.888888f;
          f_true = eval(p_tree->left->left->left->left->right, p_first);
          /* printf("utils.c, eval, e_ITE__, f_true: %f\n", f_true); */
          res = f_true;
        }
        if ( f_bool == 0.0f )
        {
          float f_false;
          f_false = -888.888888f;
          f_false = eval(p_tree->left->left->left->left->left->left->right, p_first);
          /* printf("utils.c, eval, e_ITE__, f_false: %f\n", f_false); */
          res = f_false;
        }
        /* printf("utils.c, eval, e_ITE__, res: %f\n", res); */
        /* res; */
        break;
      case e_RR__:
        /*
         * random range, randrange(2, 8);
         */
        if ( p_tree )
        {
          /*
           * printf("utils.c, eval(), switch, e_RR__, compiler expression.\n");
           */
        }
        float f_rrx;
        f_rrx = -888.888888f;
        float f_rrl;
        f_rrl = f_rrx;
        f_rrl = eval(p_tree->left, p_first);

        /*
         * printf("utils.c, e_RR_, f_rrl, %f\n", f_rrl);
         */

        float f_rrr = f_rrx;
        f_rrr = eval(p_tree->left->right->left, p_first);
        /*
         * printf("utils.c, e_RR_, f_rrr, %f\n", f_rrr);
         */

        /*
         * parse tree navigation example
         *
         * 1
         * f_xxx1 = eval(p_tree->left, p_first);
         * printf("utils.c, eval, e_DN__, f_value: left %f\n", f_xxx1);
         * printf("utils.c, eval, e_DN__, f_value: left %f\n", p_tree->f_value);
         *
         * 2
         * f_xxx2 = eval(p_tree->left->left, p_first);
         * printf("utils.c, eval, e_DN__, f_value: left left %f\n", f_xxx2);
         * printf("utils.c, eval, e_DN__, f_value: %f\n",                     \
         *         p_tree->left->left->left->f_value);
         * printf("utils.c, eval, e_DN__, i_id: %i\n", p_tree->left->i_id);
         *
         * 3
         * float f_muuu = -888.888888f;
         * f_muuu = eval(p_tree->left->left->right->left, p_first);
         * printf("utils.c, eval, e_DN__, f_value:                            \
         *         left left right left %f\n", f_muuu);
         *
         * 4
         * float f_var_ = -888.888888f;
         * f_var_ = eval(p_tree->left->right->left, p_first);
         * printf("utils.c, eval, e_DN__, f_value:                            \
         *         left right left %f\n", f_var_);
         */

        float f_rand = random_float();    /* s_random_(); OR frand(); (obs.) */
        /*
         * printf("utils.c, e_RR__, random_float(): %f\n", f_frand);
         */
        res = random_range(f_rand, f_rrl, f_rrr);
        break;
      case e_DEV_:
        /*
         * standard deviation, stdev(2, 4, 3, 3) (sigma);
         *
         * see:      Standard Deviation Calculator
         * url:      https://www.calculator.net/standard-deviation-calculator.html
         * accessed: 2022-03-18T15-35-24Z mer
         */
        if ( p_tree )
        {
          /*
           * printf("utils.c, eval, e_DEV_, compiler expression.\n");
           */
        }
        /*
         * xprint_tree(p_tree);
         * xprint_enum();
         */

        /*
         * parse tree example navigation
         *
         * float f_TEST = res_default;
         *
         * 1
         * f_TEST = eval(p_tree->left, p_first);
         * printf("utils.c, eval, e_DEV_, f_value: left %f\n", f_TEST);
         * printf("utils.c, eval, e_DEV_, f_value: left %f\n", p_tree->f_value);
         *
         * 2
         * f_TEST = eval(p_tree->left->left->left->right->left, p_first);
         * printf("utils.c, eval, f_value:                                    \
         *         left left left right left %f\n", f_TEST);
         * printf("utils.c, eval,  e_DEV_, f_value:%f\n",                     \
         *         p_tree->left->left->left->f_value);
         * printf("utils.c, eval, e_DEV_, i_id: %i\n", p_tree->left->i_id);
         *
         * 3
         * f_TEST = eval(p_tree->left->left->right->left, p_first);
         * printf("utils.c, eval, e_DEV_, f_value: left left right left %f\n", \
                   f_TEST);
         * 4
         * f_TEST = eval(p_tree->left->right->left, p_first);
         * printf("utils.c, eval, e_DEV_, f_value: left right left %f\n", f_TEST);
         *
         * record *p_f_d_init;	// pointer to a dynamic float array
         * p_f_d_init = make_record(NULL, 0.0f, NULL);
         */

        /*
         * TODO: refactor to (why) records should use arrays directly
         */
        int i_d_num = 0;
        node *p_p_d_it = p_tree;
        record *p_f_d_init = NULL;
        p_f_d_init = make_record(NULL, 0.0f, NULL);
        record *p_f_d_it_a = NULL;
        p_f_d_it_a = p_f_d_init;
        while ( p_p_d_it->left != NULL )
        {
          if ( p_p_d_it->left->right != NULL)
          {
            float f_ev_d_a = res_default;
            f_ev_d_a = eval(p_p_d_it->left->right->left, p_first);
            /*
             * printf("utils.c, eval, f_ev_d_a: %f\n", f_ev_d_a);
             */
            p_f_d_it_a = make_record(NULL, f_ev_d_a, p_f_d_it_a);
            i_d_num = i_d_num + 1;
          }
          p_p_d_it = p_p_d_it->left;
        }
        float f_ev_d_b = res_default;
        f_ev_d_b = eval(p_p_d_it, p_first);
        if ( f_ev_d_b != res_default )
        {
          /*
           * printf("utils.c, eval, f_ev_d_b:: %f\n", f_ev_d_b);
           */
          p_f_d_it_a = make_record(NULL, f_ev_d_b, p_f_d_it_a);
          i_d_num = i_d_num + 1;
        }
        /*
         * end refactor code
         */

        float *p_f_d_arr = NULL;
        p_f_d_arr = make_array(p_f_d_it_a);

        /* sizeof(p_f_d_arr); */
        int i_d_len = i_d_num;
        /*
         * for ( int i=0; i<i_d_len; i++ )
         * {
         *   printf("utils.c, p_arr_a[0], XYZ %f\n", p_f_d_arr[i]);
         * }
         */

        float f_d_mean;
        f_d_mean = -888.888888f;
        f_d_mean = mean(p_f_d_arr, i_d_len);
        /*
         * printf("utils.c, eval, e_DEV_, f_mean, %f\n", f_d_mean);
         */

        /*
         * calculate the variance.
         */
        float f_variance;
        f_variance = -888.888888f;
        f_variance = variance(p_f_d_arr, i_d_len);

        float f_stdev;
        f_stdev = -888.888888f;
        f_stdev = stdev(f_variance);
        /*
         * printf("utils.c, eval, e_DEV_, f_stdev, %f\n", f_stdev);
         */

        /*
         * free float array (p_f_d_arr)
         * free records (p_f_d_it_a)
         */
        xfree_array(p_f_d_arr);
        xfree_records(p_f_d_it_a);

        res = f_stdev;
        break;
      case e_RAND:
        /*
         * random float using library functions
         *
         * printf("utils.c, eval, e_RANDl\n");
         */

        /*
         * tested several alternatives,
         * which resulted in random_float()
         * see: s_random_(); frand(); etc. (all obsolete)
         */
        res = random_float();
        break;
      case e_EUL_:
        /*
         * eulers number
         */
        res = e();
        /*
         * printf("utils.c, eval, e_EUL_\n");
         */
        break;
      case e_PI__:
        /*
         * pi
         */
        res = pi();
        /*
         * printf("utils.c, eval, e_PI__\n");
         */
        break;
      case e_LEFT:
        /*
         * evaluate e_LEFT
         */
        f_las = eval(p_tree->left, p_first);
        res = f_las;
        /*
         * printf("utils.c, eval, e_LEFT l\n");
         * lines left statement $<anode>2 only!
         */
        break;
      case e_ADD_:
        /* arity in sum, sum(a, b, c); */

        /*
         * printf("utils.c, eval, e_ADD_\n");
         */

        f_las = eval(p_tree->left, p_first);
        f_ras = eval(p_tree->right, p_first);
        res = add_f(f_las, f_ras);

        /*
         * printf("utils.c, eval, e_ADD_, left '+' right: %f\n", res);
         */
        break;
      case e_NONE:
        break;
      case e_EXPR:
        break;
      case e_VAR_:
        break;
      case e_MUL_:
        break;
      case e_SUB_:
        break;
      case e_FLT_:
        break;
      case e_DIV_:
        break;
      case e_LT__:
        // printf("utils.c, eval, e_LT__ '/'\n");
        f_lds = eval(p_tree->left->left->right, p_first);
        // printf("utils.c, e_LT__, f_lds: %f\n", f_lds);
        f_rds = eval(p_tree->left->left->left->left->right, p_first);
        // printf("utils.c, e_LT__, r_rds: %f\n", f_rds);
        res = lt_f(f_lds, f_rds);
        // printf("utils.c, eval, e_LT__, %f\n", res);
        break;
      case e_GT__:
        // printf("utils.c, eval, e_GT__ '/'\n");
        f_lds = eval(p_tree->left->left->right, p_first);
        // printf("utils.c, e_LT__, f_lds: %f\n", f_lds);
        f_rds = eval(p_tree->left->left->left->left->right, p_first);
        // printf("utils.c, e_LT__, r_rds: %f\n", f_rds);
        res = gt_f(f_lds, f_rds);
        // printf("utils.c, eval, e_GT__, %f\n", res);
        break;
      case e_MOD_:
        break;
      case e_STR_:
        break;
      case e_BRAC:
        break;
      case e_AND_:
        break;
      case e_OPTS:
        break;
    }                           /* end switch */
  }                             /* end if */
  /*
   * only for special case 'T_LPS expr T_RPS' where
   *  'make_node("b", NULL, 0.0f, $<snd>1, $<snd>2, $<snd>3);'
   */
  if( p_tree->left !=NULL && \
      p_tree->center != NULL &&
      p_tree->right != NULL )
  {
    got_op = p_tree->e_opera;
    /*
     * printf("utils.c, eval, get_opt: %c\n", got_op);
     */
    switch(got_op)
    {
      case e_ADD_:
        /*
         * printf("utils.c, eval, e_ADD '+'\n");
         */
        f_las = eval(p_tree->left, p_first);
        f_ras = eval(p_tree->right, p_first);
        res = add_f(f_las, f_ras);
        break;
      case e_SUB_:
        /*
         * printf("utils.c, eval, e_SUB_ '-'\n");
         */
        f_lss = eval(p_tree->left, p_first);
        f_rss = eval(p_tree->right, p_first);
        res = sub_f(f_lss, f_rss);
        break;
      case e_MUL_:
        /*
         * printf("utils.c, eval, e_MUL_ '*'\n");
         */
        f_lts = eval(p_tree->left, p_first);
        f_rts = eval(p_tree->right, p_first);
        res = mul_f(f_lts, f_rts);
        break;
      case e_DIV_:
        /*
         * printf("utils.c, eval, e_DIV_ '/'\n");
         */
        f_lds = eval(p_tree->left, p_first);
        f_rds = eval(p_tree->right, p_first);
        res = div_f(f_lds, f_rds);
        break;
      case e_LT__:
        //printf("utils.c, eval, e_LT__ '/'\n");
        // f_lds = eval(p_tree->left, p_first);
        // f_rds = eval(p_tree->right, p_first);
        // res = lt_f(f_lds, f_rds);
        printf("utils.c, eval, e_LT__, %f\n", res);
        break;
      case e_GT__:
        // f_lds = eval(p_tree->left, p_first);
        // f_rds = eval(p_tree->right, p_first);
        // res = gt_f(f_lds, f_rds);
        printf("utils.c, eval, e_GT__, %f\n", res);
        break;
      case e_MOD_:
        /*
         * printf("utils.c, eval, e_MOD_ '%'\n");
         */
        f_lds = eval(p_tree->left, p_first);
        f_rds = eval(p_tree->right, p_first);
        /* f_lds % f_rds, modulo operation, float */
        res = fmodf(f_lds, f_rds);
        break;
      case e_BRAC:
        /*
         * printf("utils.c, eval, e_BRAC '()'\n");
         * bracket expression.
         */
        res_bracket = eval(p_tree->center, p_first);
        res = res_bracket;
        /*
         * printf("utils.c, eval, e_BRAC, res:%f\n", res);
         */
        break;
      case e_EXPR:
        break;
      case e_LEFT:
        break;
      case e_VAR_:
        break;
      case e_MIN_:
        break;
      case e_MAX_:
        break;
      case e_SUM_:
        break;
      case e_SIN_:
        break;
      case e_COS_:
        break;
      case e_MOID:
        break;
      case e_STR_:
        break;
      case e_FLT_:
        break;
      case e_AND_:
        break;
      case e_NONE:
        break;
      case e_OPTS:
        break;
      case e_MEAN:
        break;
      case e_SQRT:
        break;
      case e_DEV_:
        break;
      case e_RAND:
        break;
      case e_RR__:
        break;
      case e_DN__:
        break;
      case e_PI__:
        break;
      case e_EUL_:
        break;
      case e_ABS_:
        break;
      case e_ITE_:
        break;
    }                           /* end switch */
  }                             /* end if */

  /*
   * printf("utils.c, eval, return, res:%f\n", res);
   * get_keys(p_first);
   */

  return res;
}

void
xprint_node(node *p_node)
{
  printf("( NODE i_id:    %i\n  \
               e_ENUM: p: %i\n	\
                label: p: %p\n	\
                label: s: %s\n	\
                f_val: f: %f\n	\
           \n", p_node->i_id, \
                p_node->e_opera, \
                p_node->p_label, \
                p_node->p_label, \
                p_node->f_value);
}

char *
n_print(node *p_node)
{
  /*
   * either there is a char * or a float value
   * in the current node; return float as char *
   */
  char *ptr_ret = "\0";
  /* ptr_ret = (char *)"\0";
   * node operator
   */
  /*
  if (p_node->p_opera != NULL)
  {
    ptr_ret = p_node->p_opera;
  }
  */
  /* ptr_ret = (void *) p_node->e_opera; */

  /*
   * node label
   */
  if (p_node->p_label != NULL)
  {
    ptr_ret = p_node->p_label;
  }

  /*
   * node value
   * convert float to char array
   */
  if (p_node->f_value != 0.0f)
  {
    char buffer[128];
    char *ptr_buffer = buffer;
    int result = snprintf(buffer, sizeof buffer, "%f", p_node->f_value);
    /*
     * printf("utils.c, n_print(), result %i:", result);
     * something went wrong
     */
    if (result < 0)
    {
      // return -1;
      ptr_ret = NULL;
    }
    if (result >= 0)	// alright
    {
      ptr_ret = ptr_buffer;
    }
    if (result >= sizeof buffer)
    {
      /*
       * result truncated
       * need for resizing buffer
       * and retrying
       */
    }
  }
  return ptr_ret;
}

void
xprint_tree(node *p_tree)
{
  if ( p_tree->left == NULL && \
       p_tree->center == NULL && \
       p_tree->right == NULL )
  {
    /*
     * use these lines for detailed node overview.
     */
    /* printf("(%s)\n", n_print(p_tree)); */
    printf("( NODE i_id: %i\n  \
  	e_ENUM: p: %i\n	\
	label: p: %p\n	\
	label: s: %s\n	\
	f_val: f: %f\n	\
	\n", p_tree->i_id, p_tree->e_opera, p_tree->p_label, p_tree->p_label, p_tree->f_value);
  }
  else
  {
    printf("(%s", n_print(p_tree));
    printf("");
    if (p_tree->left != NULL)
    {
      printf(" ");
      xprint_tree(p_tree->left);        /* else print NULL */
    }
    if (p_tree->center != NULL)
    {
      printf(" ");
      xprint_tree(p_tree->center);      /* else print NULL */
    }
    if (p_tree->right != NULL)
    {
      printf(" ");
      xprint_tree(p_tree->right);       /* else print NULL */
    }
    printf(")\n");
  }
}

record *
walk_init(record *p_init, list *p_list)
{
  //
  // check p_list
  // list *p_liter = p_list;
  // while ( p_liter->next )
  // {
  //   printf("walk:: p_list:: list *next; p_iter->center->p_label: %s\n", \
  //   p_liter->center->p_label);
  //   p_liter = p_liter->next;
  // }
  // printf("walk:: p_list:: list *next; p_liter->center: %p\n", \
  // p_liter->center);
  // printf("walk:: p_list:: list *next; p_liter->next: %p\n", \
  // p_liter->next);
  
//  record *p_last = NULL;
//  p_first = double_list_record(p_first);
//  p_last = last_record(p_first);

  record *p_iter;
  p_u_eqns = get_p_u_eqns();
//  p_iter = p_u_eqns;
//  while ( p_iter->next )
//  {
//    printf("p_u_eqns, normal: %s\n", p_iter->p_label);
//    p_iter = p_iter->next;
//  }
  
//  print_keys(p_u_eqns);
  
  record *p_last = NULL;
  p_u_eqns = double_list_record(p_u_eqns);
  p_last = last_record(p_u_eqns);

//  p_iter = p_last;
//  while ( p_iter->prev )
//  {
//    printf("p_u_eqns, revert: %s\n", p_iter->p_label);
//    p_iter = p_iter->prev;
//  }
//  printf("p_u_eqns, revert: %s\n", p_iter->p_label);

//  record *p_iter = p_init;
  p_iter = p_last; // p_last;
  while (p_iter->prev)
  {
    // printf("p_label: %s\n", p_iter->p_label);
    
    
    //
    // for each item in p_list exists a parse tree.
    //
    char *p_search = NULL;
    float f_new;
    p_search = p_iter->p_label;
    // printf("internal: %s\n", p_iter->p_label);
    node *p_tree;
    p_tree = NULL;
    p_tree = get_entry(p_list, p_search);
    //
    // xprint_tree(p_tree);
    // printf("FOUND WALK\n");

    // printf("WALK:: p_tree %p\n", p_tree);
    // printf("FOUND WALK:: p_tree->f_value %f\n", p_tree->f_value);
    //
    // if found node is not a value node
    // having no center
    //
    if ( p_tree != NULL )
    {
      // printf("WALK:: TREE\n");
      // xprint_tree(p_tree);
      //
      // printf("WALK:: p_tree->p_label %p\n", p_tree->p_label);
      // printf("WALK:: p_tree->p_opera %p\n", p_tree->p_opera);
      // printf("WALK:: p_tree->p_label %s\n", p_tree->p_label);
      // printf("WALK:: p_tree->p_opera %s\n", p_tree->p_opera);
      // printf("WALK:: p_tree->f_value %f\n", p_tree->f_value);
      // printf("WALK:: p_tree->center %p\n", p_tree->center);
      // printf("WALK:: p_tree->center->f_value %f\n", p_tree->center->f_value);
      //

      f_new = eval(p_tree, p_init);
      //
      // printf("asdml; f_new: %f;;;;\n", f_new);
      // standard update for p_init.
      //
      p_init = update_record(p_init, p_search, f_new);
      //
      // printf("utils.c; walk_init; loop; p_init; print_keys and print_values..\n");
      // print_keys(p_init);
      // print_values(p_init);
      // printf("utils.c; walk_init; loop; p_init; print_keys and print_values done!\n");
      //
    }
    p_iter = p_iter->prev;
  }
  char *p_search = NULL;
  p_search = p_iter->p_label;
  // printf("internal: %s\n", p_iter->p_label);
  node *p_tree = NULL;
  p_tree = get_entry(p_list, p_search);
  if ( p_tree != NULL )
  {
    float f_new = eval(p_tree, p_init);
    p_init = update_record(p_init, p_search, f_new);    
  }
  return p_init;
}

/*
record *
walk_init(record *p_init, list *p_list)
{
  //
  // check p_list
  // list *p_liter = p_list;
  // while ( p_liter->next )
  // {
  //   printf("walk:: p_list:: list *next; p_iter->center->p_label: %s\n", \
  //   p_liter->center->p_label);
  //   p_liter = p_liter->next;
  // }
  // printf("walk:: p_list:: list *next; p_liter->center: %p\n", \
  // p_liter->center);
  // printf("walk:: p_list:: list *next; p_liter->next: %p\n", \
  // p_liter->next);
  
//  record *p_last = NULL;
//  p_first = double_list_record(p_first);
//  p_last = last_record(p_first);

  record *p_last = NULL;
  p_init = double_list_record(p_init);
  p_last = last_record(p_init);

//  record *p_iter = p_init;
  record *p_iter = p_init; // p_last;
  while (p_iter->next)
  {
    // printf("p_label: %s\n", p_iter->p_label);
    
    
    //
    // for each item in p_list exists a parse tree.
    //
    char *p_search = NULL;
    p_search = p_iter->p_label;
    node *p_tree = NULL;
    p_tree = get_entry(p_list, p_search);
    //
    // xprint_tree(p_tree);
    // printf("FOUND WALK\n");

    // printf("WALK:: p_tree %p\n", p_tree);
    // printf("FOUND WALK:: p_tree->f_value %f\n", p_tree->f_value);
    //
    // if found node is not a value node
    // having no center
    //
    if ( p_tree != NULL )
    {
      // printf("WALK:: TREE\n");
      // xprint_tree(p_tree);
      //
      // printf("WALK:: p_tree->p_label %p\n", p_tree->p_label);
      // printf("WALK:: p_tree->p_opera %p\n", p_tree->p_opera);
      // printf("WALK:: p_tree->p_label %s\n", p_tree->p_label);
      // printf("WALK:: p_tree->p_opera %s\n", p_tree->p_opera);
      // printf("WALK:: p_tree->f_value %f\n", p_tree->f_value);
      // printf("WALK:: p_tree->center %p\n", p_tree->center);
      // printf("WALK:: p_tree->center->f_value %f\n", p_tree->center->f_value);
      //

      float f_new = eval(p_tree, p_init);
      //
      // printf("asdml; f_new: %f;;;;\n", f_new);
      // standard update for p_init.
      //
      p_init = update_record(p_init, p_search, f_new);
      //
      // printf("utils.c; walk_init; loop; p_init; print_keys and print_values..\n");
      // print_keys(p_init);
      // print_values(p_init);
      // printf("utils.c; walk_init; loop; p_init; print_keys and print_values done!\n");
      //
    }
    p_iter = p_iter->next;
  }
  //
  // print_keys(p_init);
  //
  return p_init;
}*/

/*
record *
walk_init(record *p_init, list *p_list)
{
  //
  // check p_list
  // list *p_liter = p_list;
  // while ( p_liter->next )
  // {
  //   printf("walk:: p_list:: list *next; p_iter->center->p_label: %s\n", \
  //   p_liter->center->p_label);
  //   p_liter = p_liter->next;
  // }
  // printf("walk:: p_list:: list *next; p_liter->center: %p\n", \
  // p_liter->center);
  // printf("walk:: p_list:: list *next; p_liter->next: %p\n", \
  // p_liter->next);
  
//  record *p_last = NULL;
//  p_first = double_list_record(p_first);
//  p_last = last_record(p_first);


  record *p_iter = p_init;
  while (p_iter->next)
  {
    //
    // for each item in p_list exists a parse tree.
    //
    char *p_search = NULL;
    p_search = p_iter->p_label;
    node *p_tree = NULL;
    p_tree = get_entry(p_list, p_search);
    //
    // xprint_tree(p_tree);
    // printf("FOUND WALK\n");

    // printf("WALK:: p_tree %p\n", p_tree);
    // printf("FOUND WALK:: p_tree->f_value %f\n", p_tree->f_value);
    //
    // if found node is not a value node
    // having no center
    //
    if ( p_tree != NULL )
    {
      // printf("WALK:: TREE\n");
      // xprint_tree(p_tree);
      //
      // printf("WALK:: p_tree->p_label %p\n", p_tree->p_label);
      // printf("WALK:: p_tree->p_opera %p\n", p_tree->p_opera);
      // printf("WALK:: p_tree->p_label %s\n", p_tree->p_label);
      // printf("WALK:: p_tree->p_opera %s\n", p_tree->p_opera);
      // printf("WALK:: p_tree->f_value %f\n", p_tree->f_value);
      // printf("WALK:: p_tree->center %p\n", p_tree->center);
      // printf("WALK:: p_tree->center->f_value %f\n", p_tree->center->f_value);
      //

      float f_new = eval(p_tree, p_init);
      //
      // printf("asdml; f_new: %f;;;;\n", f_new);
      // standard update for p_init.
      //
      p_init = update_record(p_init, p_search, f_new);
      //
      // printf("utils.c; walk_init; loop; p_init; print_keys and print_values..\n");
      // print_keys(p_init);
      // print_values(p_init);
      // printf("utils.c; walk_init; loop; p_init; print_keys and print_values done!\n");
      //
    }
    p_iter = p_iter->next;
  }
  //
  // print_keys(p_init);
  //
  return p_init;
}
*/

record *
init_p_show(record *p_show, record *p_init)
{
//  record *p_last = NULL;
//  p_init = double_list_record(p_init);
//  p_last = last_record(p_init);

  /* print_keys(p_init);
   * print_values(p_init);
   */
   
   record *p_iter = NULL;
   //p_iter = p_show;
   //while ( p_iter->next )
   //{
   //  printf("utils.c, init_p_show, p_label: %s\n", p_iter->p_label);
   //  p_iter = p_iter->next;
   //}
   //printf("utils.c, init_p_show, p_label: %s\n", p_iter->p_label);
   
   //print_keys(p_show);
   /*
    * print_values(p_show);
    */
  p_iter = p_show;
  while ( p_iter->next )
  {
    if ( true == is_valid_key(p_init, p_iter->p_label) )
    {
      float f_value = get_value(p_init, p_iter->p_label);
      /* print_keys(p_show);
       *
       * float f_value = get_value(p_init, p_tree->p_label);
       *
       * printf("utils.c; init_p_show; p_label %s\n", p_iter->p_label);
       * printf("utils.c; init_p_show; f_value %f\n", f_value);
       *
       * print_keys(p_init);
       * print_values(p_init);
       */
      p_show = update_record(p_show, p_iter->p_label, f_value);
    }
    p_iter = p_iter->next;
  }
  return p_show;
}

/* preface of pick_p_show().
 *	00 make use of e_VAR_ enum for ->
 *	01 recursively_get_all_vars_from_OPT
 *	02 p_show will contain these vars
 *	03 redo walk_show using p_init matching p_show.
 *
 *       04 ex.
 *       %show	p_valA;
 *       test-MODEL-A.sh
*/

/* p_tree = OPT_iter->center */
record *
pick_p_show(node *p_tree, record *p_show, record *p_init)
{
   //record *p_iterrr;
   //p_iterrr = p_show;
   //while ( p_iterrr->next )
   //{
   //  printf("utils.c, pick_p_show, p_label: %s\n", p_iterrr->p_label);
   //  p_iterrr = p_iterrr->next;
   //}
   //printf("utils.c, pick_p_show, p_label: %s\n", p_iterrr->p_label);

  /*
   * printf("(%d", e_VAR_);
   */
  float res_default = -888.888888f;

  if ( p_tree->left == NULL   && \
       p_tree->center == NULL && \
       p_tree->right == NULL  && \
       b_u_show_all == false )
  {
    if ( p_tree->e_opera == e_VAR_ )
    /*
     *  && is_valid_key(p_show, p_tree->p_label) == true )
     */
    {
//      if (  strcmp(p_tree->p_label, "all") == 0 ||
//            strcmp(p_tree->p_label, "ALL") == 0    )
//      {
//        b_u_show_all = true;
//      }
      bool is_valid = false;
//      is_valid = is_valid_key(p_show, p_tree->p_label);
      is_valid = is_valid_key(p_init, p_tree->p_label);
      /*
       * printf("utils.c, pick_p_show, is_valid %s", is_valid ? "true" : "false");
       * printf("utils.c, pick_p_show, p_tree->p_label %s", p_tree->p_label);
       */
      char *p_label = (char *) calloc(1, strlen(p_tree->p_label) +1);
      strcpy(p_label, p_tree->p_label);

      /*
       * printf("get_show_record, four\n");
       * mem..!
       * float f_value = get_value(p_init, p_tree->p_label);
       * using default value only - value unimportant
       */

      float f_value = res_default;
//      if ( is_valid == false )
// resume 2022-04-13T20_39_00Z mer
      //bool is_valid_key_in_p_init = false;
      //is_valid_key_in_p_init = is_valid_key(p_init, p_tree->p_label);
      f_value = get_value(p_init, p_tree->p_label);
//      if ( is_valid_key_in_p_init == true && f_value != res_default )
//      {
      if ( ( is_valid == true ) && ( f_value != res_default ) )
      {
        /*
         * printf("utils.c; pick_p_show; is_valid == false\n");
         * printf("utils.c; pick_p_show; make_record\n");
         */
          p_show = make_record(p_label, f_value, p_show);
      }
//      }
    }

    /*
     * printf("(%s)\n", n_print(p_tree));
     * printf("(	opera: p: %i\n	\
     * e_VAR_: %i\n \
     * label: p: %p\n	\
     * label: s: %s\n	\
     * label: i: %i\n	\
     * \n", p_tree->e_opera, e_VAR_, p_tree->p_label, p_tree->p_label, p_tree->p_label);
     */

  }
  else
  {
    /*
     * printf("(%s", n_print(p_tree));
     * printf("(%d", p_tree->e_opera);
     */

    /*
    if ( p_tree->e_opera == e_VAR_ )
    {
      printf("utils.c; pick_p_show; p_tree->p_label %s", p_tree->p_label);
      char *p_label = (char *) calloc(1, strlen(p_tree->p_label) +1);
      strcpy(p_label, p_tree->p_label);
      // printf("get_show_record, four\n");

      // float f_value = get_value(p_init, p_tree->p_label);
      // using default value only - value unimportant

      float f_value = res_default;
      p_show = make_record(p_label, f_value, p_show);
    }
    */

    /*
     * printf("");
     */
    if (p_tree->left != NULL)
    {
      /* printf(" "); */
      p_show = pick_p_show(p_tree->left, p_show, p_init); // else print NULL
    }
    if (p_tree->center != NULL)
    {
      /* printf(" "); */
      p_show = pick_p_show(p_tree->center, p_show, p_init); // else print NULL
    }
    if (p_tree->right != NULL)
    {
      /* printf(" "); */
      p_show = pick_p_show(p_tree->right, p_show, p_init); // else print NULL
    }
    /* printf(")\n"); */
  }
  return p_show;
}

/*
void
talk_STD(record *p_init, list *p_AST) // , list *p_OPT)
{
  print_keys(p_init);
  print_values(p_init);

  int steps = get_value(p_init, "steps");
  int i = 1;
  while ( i < steps + 1 )
  {
    p_init = update_record(p_init, "x", i);
    p_init = walk_init(p_init, p_AST);

//    printf("%i,", i);
    print_values(p_init);
    i++;
  }
}*/

/*
void
talk_STD(record *p_init, list *p_AST) // , list *p_OPT)
{
  // printf("utils.c, p_show, %p\n", p_show);
  // printf("utils.c, p_show->center, %p\n", p_show->center);
  // printf("utils.c, p_show->next, %p\n", p_show->next);
  // printf("x\t");


//  printf("x,");
//  print_keys(p_init);
  //
  // printf("0\t");
  print_keys(p_init);


//  printf("0;");
  print_values(p_init);


  // steps after 0

  int steps = get_value(p_init, "steps");
  int i = 1;
  while ( i < steps + 1 )
  {
    //
    // printf("utils.c, talk_STD, while, i: %i\n", i);
    // printf("i: %i\n", i);

    p_init = update_record(p_init, "x", i);
    p_init = walk_init(p_init, p_AST);
    //
    // printf("%i\t", i);

//    printf("%i,", i);
    print_values(p_init);
    i++;
  }
}
*/

void
talk_OPT(record *p_init, record *p_show, list *p_AST)
{
  //printf("utils.c, talk_OPT\n");
  //print_values(p_show);
  
  //record *p_iter = p_show;
  //while ( p_iter->next )
  //{
  //  printf("utils.c, talk_OPT, p_label: %s\n", p_iter->p_label);
  //  p_iter = p_iter->next;
  //}
  //printf("utils.c, talk_OPT, p_label: %s\n", p_iter->p_label);

  //bool b_c = is_valid_key(p_show, "x");
  //printf("utils.c; is_valid_key, is valid key %i\n", b_c);

  //bool b_d = is_valid_key(p_init, "x");
  //printf("utils.c; is_valid_key, is valid key %i\n", b_d);

  
  //float f_t = get_value(p_show, "x");  
  //printf("utils.c, talk_OPT, f_value: %f\n", f_t);
  
  int i_length = get_length(p_u_init);

  if ( i_length > 0 )
  {
    //char *p_x = "x";
    //printf("%s,", p_x);
    record *p_show = NULL;
    p_show = get_p_u_show();
    //printf("utils.c, talk_OPT, p_u_show\n");
    
    // int i = 0;
    
    print_keys(p_u_show);
    // print_values(p_u_show);
    
    float f_i = get_value(p_u_show, "x");
    int i = (int) f_i;
    //printf("utils.c, talk_OPT, x/i: %i\n", i);
    // printf("%i,", i);
    print_values(p_u_show);

    // steps after 0

    int steps = get_value(p_init, "steps");
    i = i + 1;
    while ( i < steps + 1 )
    {
      p_init = update_record(p_init, "x", i);
      p_init = walk_init(p_init, p_AST);

      record *p_iter = p_show;
      // iterate and update p_show
      while ( p_iter->next )
      {
        float f_value = get_value(p_init, p_iter->p_label);
        p_show = update_record(p_show, p_iter->p_label, f_value);
        p_iter = p_iter->next;
      }
      // printf("%i,", i);
      print_values(p_show);
      i++;
    }
  }
}


void
talk_STD(record *p_init, list *p_AST) // , list *p_OPT)
{
  print_keys(p_init);
  print_values(p_init);

  int steps = get_value(p_init, "steps");
  int i = 1;
  while ( i < steps + 1 )
  {
    p_init = update_record(p_init, "x", i);
    p_init = walk_init(p_init, p_AST);

//    printf("%i,", i);
    print_values(p_init);
    i++;
  }
}



/*
void
talk_OPT(record *p_init, record *p_show, list *p_AST)
{
   // printf("utils.c, p_show, %p\n", p_show);
   // printf("utils.c, p_show->center, %p\n", p_show->center);
   // printf("utils.c, p_show->next, %p\n", p_show->next);
   // printf("x\t");

  int i_length = get_length(p_u_init);

  // printf("utils.c, talk_STD, i_length: %i\n", i_length);

  if ( i_length > 0 )
  {
    // printf("utils.c, talk_STD\n");
    //print_values(p_init);

    char *p_x = "x";
    printf("%s,", p_x);
    print_keys(p_show);

    // printf("0\t");

    // printf("0,");
    float f_i = get_value(p_init, "x");
    int i = (int) f_i;
    // printf("utils.c, talk_OPT, x/i: %i\n", i);
    printf("%i,", i);
    print_values(p_show);

    // steps after 0

    int steps = get_value(p_init, "steps");
    i = i + 1;
    while ( i < steps + 1 )
    {
      // printf("utils.c, talk_OPT, x/i: %i\n", i);
      //
      // printf("utils.c, talk_STD, while, i: %i\n", i);
      // printf("i: %i\n", i);

      //
      // added x / t as a variable to p_init records
      // 2022-04-27T14-44-23Z mer

      p_init = update_record(p_init, "x", i);
      p_init = walk_init(p_init, p_AST);

      record *p_iter = p_show;
      // iterate and update p_show
      while ( p_iter->next )
      {
        float f_value = get_value(p_init, p_iter->p_label);
        p_show = update_record(p_show, p_iter->p_label, f_value);
        p_iter = p_iter->next;
      }

      //
      // printf("%i\t", i);
      printf("%i,", i);
      print_values(p_show);
      i = i + 1;
    }
  }
}
*/

void
xprint_overview()
{
  /*
   * all nodes are captured in lists
   * freeing memory falls back to lists
   * i_free_node_OBSOLETE might be zero
   */
  printf("utils.c, xprint_overview()\n");
  printf("utils.c, i_make_node %i\n", i_make_node);
  printf("utils.c, i_free_node_OBSOLETE %i\n", i_free_node_OBSOLETE);

  printf("utils.c, i_make_list %i\n", i_make_list);
  printf("utils.c, i_free_list %i\n", i_free_list);
}

void
set_p_u_nodes(list *p_list)
{
  p_u_nodes = (list *) p_list;
}

list *
get_p_u_nodes()
{
  return (list *) p_u_nodes;
}

void
set_u_flex(record *p_l_flex)
{
  p_u_flex = (record *) p_l_flex;
}

record *
get_u_flex()
{
  return (record *) p_u_flex;
}

float
sum(float *p_f_arr, int i_len)
{
  float f_sum = -0.0f;

  for ( int i=0; i<i_len; i++ )
  {
    float f_val = p_f_arr[i];
    /*
     * printf("utils.c, sum, %f\n", f_val);
     */
    f_sum = f_sum + f_val;
  }
  return f_sum;
}

float
mean(float *p_f_arr, int i_len)
{
  float f_mean = -888.888888f;
  float f_sum = -0.0;

  for ( int i=0; i<i_len; i++ )
  {
    float f_val = p_f_arr[i];
    /*
     * printf("utils.c, mean(), all integer, %f\n", f_val);
     */
    f_sum = f_sum + f_val;
  }
  f_mean = f_sum / i_len;

  return f_mean;
}

float variance(float *p_f_arr, int i_len)
{
  float f_var = -0.0f;
  float f_mean = -0.0f;
  f_mean = mean(p_f_arr, i_len);

  for (int i=0; i<i_len; i++ )
  {
    float f_delta = -0.0f;
    float f_value = -0.0f;
    f_value = p_f_arr[i];
    f_delta = f_value - f_mean;
    float f_squar = -0.0f;
    f_squar = f_delta * f_delta;
    f_var = f_var + f_squar;
  }
  f_var = f_var / i_len;

  return f_var;
}

float stdev(float f_variance)
{
  float f_stdev = -0.0f;
  f_stdev = sqrt(f_variance);
  return f_stdev;
}

float
random_range(float f_s, float f_i, float f_j)
{
  float f_res = -888.888888f;
  /*
   * printf("%f\n", f_val);
   * f_val = 2.0+(f_val*(7.0-2.0));
   */
  if ( f_i < f_j )
  {
    f_res = f_i + ( f_s * ( f_j - f_i ) );
  }
  else
  {
    printf("random, left range GT right range!\n");
  }
  /*
   * printf("%f\n", f_res);
   *
   * i_val = (int)round_float(f_res);
   * printf("%i\n", i_val);
   * printf("%f\n", f_s);
   *
   * printf("%f - %f = %f\n", f_j, f_i, f_j - f_i);
   *
   * printf("%f\n", f_j - f_i);
   *
   * printf("%f\n", f_res);
   */
  return f_res;
}

void
set_interactive(bool *p_b_ia)
{
  b_u_interactive = (bool *) p_b_ia;
  /*
   * printf("utils.c, b_u_interactive: %s", \
   *       b_u_interactive ? "true\n" : "false\n");
   */
}

bool *
get_interactive()
{
  return (bool *) b_u_interactive;
}

void
set_p_u_eqns(record *p_m_eqns)
{
  p_u_eqns = p_m_eqns;
}

record *
get_p_u_eqns()
{
  return (record *) p_u_eqns;
}

void
set_p_u_init(record *p_m_init)
{
  p_u_init = p_m_init;
}

record *
get_p_u_init()
{
  return (record *) p_u_init;
}

void
set_p_u_show(record *p_m_show)
{
  p_u_show = p_m_show;
}

record *
get_p_u_show()
{
  return (record *) p_u_show;
}

void
set_p_u_AST(list *p_m_AST)
{
  p_u_AST = p_m_AST;
}

list *
get_p_u_AST()
{
  return (list *) p_u_AST;
}

void
set_p_u_OPT(list *p_m_OPT)
{
  p_u_OPT = p_m_OPT;
}

list *
get_p_u_OPT()
{
  return (list *) p_u_OPT;
}

void
set_b_u_pars(bool b_m_pars)
{

  /*
   * printf("utils.c, set_b_u_pars, b_m_pars is ");
   * printf("%s", b_m_pars ? "true" : "false");
   * printf("\n");
   */


  b_u_pars = b_m_pars;

  /*
   * printf("utils.c, set_b_u_pars, b_u_pars is ");
   * printf("%s", b_u_pars ? "true" : "false");
   * printf("\n");
   */
}

bool
get_b_u_pars()
{

  /*
   * printf("utils.c, get_b_u_pars, b_u_pars is ");
   * printf("%s", b_u_pars ? "true" : "false");
   * printf("\n");
   */

  return b_u_pars;
}

void
set_b_u_dots(bool b_m_dots)
{

  /*
   * printf("utils.c, set_b_u_dots, b_m_dots is ");
   * printf("%s", b_m_dots ? "true" : "false");
   * printf("\n");
   */


  b_u_dots = b_m_dots;

  /*
   * printf("utils.c, set_b_u_dots, b_u_dots is ");
   * printf("%s", b_u_dots ? "true" : "false");
   * printf("\n");
   */
}

bool
get_b_u_dots()
{

  /*
   * printf("utils.c, get_b_u_dots, b_u_dots is ");
   * printf("%s", b_u_dots ? "true" : "false");
   * printf("\n");
   */

  return b_u_dots;
}

void
set_b_u_reset(bool b_p_reset)
{
  /*
   * printf("utils.c, set_b_u_reset, b_p_reset is ");
   * printf("%s", b_p_reset ? "true" : "false");
   * printf("\n");
   */

  b_u_reset = b_p_reset;

  /*
   * printf("utils.c, set_b_u_reset, b_u_reset is ");
   * printf("%s", b_u_reset ? "true" : "false");
   * printf("\n");
   */
}

bool
get_b_u_reset()
{
  /*
   * printf("utils.c, get_b_u_reset, b_u_reset is ");
   * printf("%s", b_u_reset ? "true" : "false");
   * printf("\n");
   */

  return b_u_reset;
}

void
set_b_u_exit(bool b_p_exit)
{
  /*
   * printf("utils.c, set_b_u_exit, b_p_exit is ");
   * printf("%s", b_p_exit ? "true" : "false");
   * printf("\n");
   */

  b_u_exit = b_p_exit;

  /*
   * printf("utils.c, set_b_u_exit, b_u_exit is ");
   * printf("%s", b_u_exit ? "true" : "false");
   * printf("\n");
   */
}

bool
get_b_u_exit()
{
  /*
   * printf("utils.c, get_b_u_exit, b_u_exit is ");
   * printf("%s", b_u_exit ? "true" : "false");
   * printf("\n");
   */

  return b_u_exit;
}


void
set_b_u_inta(bool b_inta)
{
  /*
   * printf("utils.c, set_b_u_inta, b_u_inta is ");
   * printf("%s", b_u_inta ? "true" : "false");
   * printf("\n");
   */

  b_u_inta = b_inta;

  /*
   * printf("utils.c, set_b_u_inta, b_u_inta is ");
   * printf("%s", b_u_inta ? "true" : "false");
   * printf("\n");
   */
}

bool
get_b_u_inta()
{
  /*
   * printf("utils.c, get_b_u_inta, b_u_inta is ");
   * printf("%s", b_u_inta ? "true" : "false");
   * printf("\n");
   */

  return b_u_inta;
}


void pre_parse()
{
  // yydebug = 1;

  /*
   * setting initial first/start elements
   * for record(s) and list(s)
   */

  /*
   * setting initial first/start elements
   * for record(s) and list(s)
   */

  p_u_init = make_record(NULL, 0.0f, NULL);
  p_u_eqns = make_record(NULL, 0.0f, NULL);  
  /*
   * added x / t as a variable to p_init records
   * 2022-04-27T14-44-23Z mer
   *
   * a record for time in steps only, starting with 0.0f, label is "x"
   */
  char *p_i_time = (char *) calloc(1, strlen("x") +1);
  strcpy(p_i_time, "x");
  p_u_init = make_record(p_i_time, 0.0f, p_u_init);
  /*
   * printf("utils.c, pre_parse()\n");
   * print_keys(p_u_init);
   * print_values(p_u_init);
   */

  /* set variable in utils.c */
  /* set_p_u_init(p_m_init); */

  /*
   * printf("parser.y, main, test print_keys and values..\n");
   * print_keys(p_m_init);
   * print_values(p_m_init);
   * printf("parser.y, main, test print_keys and values done!\n");
  */

  p_u_show = make_record(NULL, 0.0f, NULL);
  set_p_u_show(p_u_show);
  /*
   * added x / t as a variable to p_init records
   * 2022-04-27T14-44-23Z mer
   *
   * a record for time in steps only, starting with 0.0f, label is "x"
   */
  // char *p_s_time = (char *) calloc(1, strlen("x") +1);
  // strcpy(p_s_time, "x");
  // p_u_show = make_record(p_s_time, 0.0f, p_u_show);

  //record *p_iterrr;
  //p_iterrr = p_u_show;
  //while (p_iterrr->next)
  //{
    //printf("utils.c, pre_parse, p_u_show, p_label: %s\n", p_iterrr->p_label);
    //p_iterrr = p_iterrr->next;
  //}
  //printf("utils.c, pre_parse, p_u_show, p_label: %s\n", p_iterrr->p_label);  

  set_p_u_show(p_u_show);

//  record *p_last = NULL;
//  p_u_show = double_list_record(p_u_show);
//  p_last = last_record(p_u_show);
  
//  print_keys(p_u_show);

  /*
   * printf("utils.c, pre_parse()\n");
   * print_keys(p_u_show);
   * print_values(p_u_show);
   */

  /* set variable in utils.c */
  /* set_p_u_show(p_u_show); */

  p_u_flex = make_record(NULL, 0.0f, NULL);
  /* set_u_flex(p_u_flex); */

  p_u_AST = make_list(NULL, NULL);
  /* set_p_u_AST(p_m_AST); */

  p_u_OPT = make_list(NULL, NULL);
  /* set_p_u_OPT(p_m_OPT); */

  /* parse nodes p_pp_nodes */
  p_u_nodes = make_list(NULL, NULL);
  /* set_p_u_nodes(p_m_nodes); */
}

int display()
{
  // printf("utils.c, display\n");
  /*
   * if option %show / dis / display variables is set
   * a selection of variables exist in p_show
   */
  p_u_init = get_p_u_init();
  // printf("utils.c, display, print_values(p_u_init)\n");
  // print_values(p_u_init);

  //record *p_iterrr = NULL;
  //p_iterrr = p_u_init;
  //while (p_iterrr->next)
  //{
  //  printf("utils.c, display, p_u_init, p_label: %s\n", p_iterrr->p_label);
  //  p_iterrr = p_iterrr->next;
  //}
  //printf("utils.c, display, p_u_init, p_label: %s\n", p_iterrr->p_label);  
  
  //printf("utils.c, display\n");
  p_u_show = get_p_u_show();  
  //print_values(p_u_show);
     
  //p_iterrr = p_u_show;
  //while (p_iterrr->next)
  //{
  //  printf("utils.c, display, p_u_show, p_label: %s\n", p_iterrr->p_label);
  //  p_iterrr = p_iterrr->next;
  //}
  //printf("utils.c, display, p_u_show, p_label: %s\n", p_iterrr->p_label);  

  p_u_AST = get_p_u_AST();
  p_u_OPT = get_p_u_OPT();

  //
  // printf("utils.c, display, p_u_OPT->center %p\n", p_u_OPT->center);
  //

  if (  b_u_show_all == false    &&       \
        p_u_show->center == NULL &&       \
        p_u_show->next == NULL   &&       \
        p_u_OPT->center != NULL )
  {
    // because this is not working in pre_parse()
    // it is placed here.
    // p_u_show = make_record(NULL, 0.0f, NULL);
    
    // this is not the optimal solution
    // would be better to have this section in pre_parse()
    // as print_keys() is filtering steps last variables get lost.
    // good workaround for displaying n variables.
    char *p_s_time = (char *) calloc(1, strlen("x") +1);
    strcpy(p_s_time, "x");
    p_u_show = make_record(p_s_time, 0.0f, p_u_show);

    // pick variables p_show    
    p_u_show = pick_p_show(p_u_OPT->center, p_u_show, p_u_init);
    float f_steps = 888.888f;
    f_steps = get_value(p_u_init, "steps");

    char *p_s_steps = (char *) calloc(1, strlen("steps") +1);
    strcpy(p_s_steps, "steps");
    p_u_show = make_record(p_s_steps, f_steps, p_u_show);
    
    //print_values(p_u_show);
    //print_values(p_u_init);
    // f_steps = get_value(p_u_show, "steps");
    // printf("utils.c, display, steps: %f\n", f_steps);
    
  //p_iterrr = p_u_show;
  //while (p_iterrr->next)
  //{
  //  printf("utils.c, display, if, p_u_show, p_label: %s\n", p_iterrr->p_label);
  //  p_iterrr = p_iterrr->next;
  //}
  //printf("utils.c, display, if, p_u_show, p_label: %s\n", p_iterrr->p_label);

    // p_u_show = make

    //
    // print_keys(p_u_show);
    //
    // init values p_show
    p_u_show = init_p_show(p_u_show, p_u_init);
    //
    //print_keys(p_u_show);
    // printf("utils.c, display, talk_OPT\n");
    //
  }

  //
  // print_keys(p_show);
  //

  if (  p_u_show->next != NULL &&
        b_u_show_all == false )
  {
    talk_OPT(p_u_init, p_u_show, p_u_AST); // , p_m_OPT);
    //
    // printf("parser.y, main, using talk_OPT..\n");
    //
  }

  /* STD, print all variables and data values */
  if ( b_u_show_all == true )
        //
        // p_u_show->center == NULL &&       \
        // p_u_show->next == NULL   &&       \
        // b_u_show_all == true     ||       \
        // p_u_OPT->center == NULL )
        //
  {
    talk_STD(p_u_init, p_u_AST); // , p_m_OPT);
    //
    // printf("utils.c, display, talk_STD\n");
    // print_keys(p_m_init);
    // print_values(p_m_init);
    // printf("parser.y, main, using talk_STD..\n");
    //
  }
  return 0;
}

void
post_parse()
/*
 * record *p_m_init,
 * record *p_m_show,
 * record *p_m_flex,
 * list *p_m_AST,
 * list *p_m_OPT )
 */
{
  /*
   * to print the parse tree use the following
   * -> xprint_tree(p_root);
   */

  /* freeing memory */
  xfree_records(p_u_init);
  xfree_records(p_u_show);
//  xfree_records_eqns(p_u_eqns);
  xfree_records(p_u_eqns);

  /*
   * changed from
   * -> xfree_list(p_m_AST); to
   * -> xfree_AST_list(p_m_AST);
   */

  xfree_AST_list(p_u_AST);
  xfree_AST_list(p_u_OPT);

  p_u_nodes = get_p_u_nodes();
  xfree_list(p_u_nodes); // for printing use free_list

  /* xfree_parse_list */
  p_u_flex = get_u_flex();
  xfree_records(p_u_flex);
}

void
set_b_u_show_all(bool show_all)
{
  b_u_show_all = show_all;
}

bool
get_b_u_show_all()
{
  return b_u_show_all;
}


void
set_p_u_buff(char *p_m_buff)
{
  p_u_buff = p_m_buff;
}

char *
get_p_u_buff()
{
  return p_u_buff;
}

void
print_abs_fn_length(char *p_abs_fn)
{
  /*
   * char fn = "ASDPN3/path/afilename.txt";
   * char fn[] = "ASDPN3/path/afilename.txt";
   * char *p_abs_fn = fn;
   * print_length(p_abs_fn);
   */

  int i_len;
  i_len = strlen(p_abs_fn);
  printf("i_len: %d\n", i_len);
}

void
print_dot_header(char *p_abs_fn)
{
  int i_pad;
  /* total - path - basename - extension - last ( '#' )  */
  i_pad = 80 - 28 - strlen(p_abs_fn) - 4 - 1;

  printf("################################################################################\n");

  /*
   * padding of spaces
   * "# Name        : ASDPN3/dots/adaption.dot                                       #" */
  printf("# Name        : ASDPN3/dots/%s.dot", p_abs_fn);

  for ( int i = 0; i < i_pad; i++)
  {
    printf(" ");
  }
  printf("#\n");

  printf("# Author      : M.E.Rosner                                                     #\n");
  printf("# E-Mail      : marty[at]rosner[dot]io                                         #\n");
  printf("# Version     : 2022Q4                                                         #\n");
  printf("# Copyright   : Copyright (C) 2019-2022 M.E.Rosner; Berlin; Germany            #\n");
  printf("# License     : The MIT License                                                #\n");
  printf("# Description : A System Dynamics Process(ing) Notation                        #\n");
  printf("################################################################################\n");
}

/* $ASDPN3: src/utils.c,v 2022Q4 2022/10/05 11:25:09 mer Exp $ */
