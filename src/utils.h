/*******************************************************************************
* Name        : ASDPN3/src/utils.h                                             *
* Author      : M.E.Rosner                                                     *
* E-Mail      : marty[at]rosner[dot]io                                         *
* Version     : 2022Q4                                                         *
* Copyright   : Copyright (C) 2019-2022 M.E.Rosner; Berlin; Germany            *
* License     : The MIT License                                                *
* Description : A System Dynamics Process(ing) Notation                        *
*******************************************************************************/

/*!
 *  \file utils.h
 *  \brief Header file for utils.c
 *  \author    M.E.Rosner
 *  \version   2022Q4
 *  \date      2022/10/05
 *  \bug       No known bugs.
 *  \copyright Copyright 2019-2022 M.E.Rosner. All rights reserved.
 *  \license   Released under The MIT License.
 */

/* C utils (node, linked list, math, etc.) */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
/* sin, cos, tanh, etc. */
#include <math.h>
/* clock */
#include <time.h>

/*!
 *  \brief add_f The function returns the sum of two floats.
 *  \details A parse tree with a Left / Right side.
 *  \param f_las The float left side.
 *  \param f_ras The float right side.
 *  \see add_f()
 *
 *  \date 2022-03-09T13_56_32Z
 *  \return res The sum of the two parameters ( default is -888.888888f ).
 */
float
add_f(float f_las, float f_ras);

/*!
 *  \brief sub_f The function returns the difference of two floats.
 *  \details A parse tree with a Left / Right side.
 *  \param f_lss The float left side.
 *  \param f_rss The float right side.
 *  \see sub_f()
 *
 *  \return res The difference for two leafs of a parse tree (default is -888.888888f).
 */
float
sub_f(float f_lss, float f_rss);

/*!
 *  \brief mul_f The function returns the factor of two multiplied floats.
 *  \details A parse tree with a Left / Right side.
 *  \param f_lts The float left side.
 *  \param f_rts The float right side.
 *  \see mul_f()
 *
 *  \return res Returns the product for two leafs of a parse tree (default is -888.888888f).
 */
float
mul_f(float f_lts, float f_rts);

/*!
 *  \brief div_f The function returns the value of two divided floats.
 *  \details A parse tree with a Left / Right side.
 *  \param f_lds The float left side.
 *  \param f_rds The float right side.
 *  \see div_f()
 *
 *  \return res Returns the value for dividing two leafs of a parse tree (default is -888.888888f).
 */
float
div_f(float f_lds, float f_rds);

/*!
 *  \brief lt_f The function returns the less than value as a float (1.0f true).
 *  \details A parse tree with a Left / Right side.
 *  \param f_lds The float left side.
 *  \param f_rds The float right side.
 *  \see lt_f()
 *
 *  \return res Returns 1.0f, if f_lds < f_rds, else 0.0f.
 */
float
lt_f(float f_lds, float f_rds);

/*!
 *  \brief gt_f The function returns the greater than value as a float (1.0f true).
 *  \details A parse tree with a Left / Right side.
 *  \param f_lds The float left side.
 *  \param f_rds The float right side.
 *  \see gt_f()
 *
 *  \return res Returns 1.0f, if f_lds > f_rds, else 0.0f.
 */
float
gt_f(float f_lds, float f_rds);

/*!
 *  \brief min_f The function returns the minimum value of two floats.
 *  \details A parse tree with a Left / Right side.
 *  \param f_ils The float left side.
 *  \param f_irs The float right side.
 *  \see min_f()
 *
 *  \return res Returns the minimum value for two leafs of a parse tree (default is -888.888888f).
 */
float
min_f(float f_ils, float f_irs);

/*!
 *  \brief max_f The function returns the maximum value of two floats.
 *  \details A parse tree with a Left / Right side.
 *  \param f_als The float left side.
 *  \param f_ars The float right side.
 *  \see max_f()
 *
 *  \return res Returns the maximum value for two leafs of a parse tree (default is -888.888888f).
 */
float
max_f(float f_als, float f_ars);

/*!
 *  \brief Float values to draw a s-curve from one parameter.
 *  \details Returns the sigmoid / s-curve for a series of float values, using atan() function.
 *  \param t A point in time.
 *  \see sigmoid()
 *  \see https://de.wikipedia.org/wiki/Sigmoidfunktion (accessed: 2022-02-14T13_48_13Z mrosner)
 *
 *  \return res A resulting y-value to draw the double-s curve (default is -888.888888f).
 */
float
sigmoid(float t);

/*!
 *  \brief Float values to draw a double-s curve from three parameters.
 *  \details Returns the multiple s-curve for a series of float values, using atan() function.
 *  \param x The time value.
 *  \param a The float a.
 *  \param b The float b. 
 *  \see https://math.stackexchange.com/questions/2136967/double-s-curve-function (accessed: 2022-02-14T13_45_03Z mer)
 *
 *  \return res A resulting y-value to draw the double-s curve (default is -888.888888f).
 */
float
double_s(float t, float a, float b);

/*!
 *  \brief A TOOLSET to retrieve a random integer value from /dev/urandom.
 *  \details Fileoperations in /tmp needed. Usage of od -vAn -N4 -tu4.
 *  \see https://stackoverflow.com/questions/822323/how-to-generate-a-random-int-in-c/39475626#39475626 (accessed: 2022-02-14T13_32_44Z mer)
 *
 *  \return li_val A random integer value from /dev/urandom.
 */
int
urandom_int();

//float []
//get_f_arr(int n);

/* TODO: write a better stdev
seen as a reminder:
https://math.stackexchange.com/questions/2148877/iterative-calculation-of-mean-and-standard-deviation
https://en.wikipedia.org/wiki/Standard_deviation#Identities_and_mathematical_properties

n   - Anzahl der Werte
mu_n - als Mittelwert
S_1 - als Summe ueber x_i
S_2 - als Summe ueber x_i^2

mu_n - S_1 / n
sigma_n - sqrt( ( S_2 / n ) - ( S_1 / n )^2 )

float
stdev(n, mu_n, S_1, S_2);
*/


// get a random integer.
// adopted from
// https://stackoverflow.com/questions/822323/how-to-generate-a-random-int-in-c/39475626#39475626
// int
// rand();


/*!
 *  \brief f_res A random float value within the interval of zero and one.
 *  \details Using the clock() function as input for srand48().
 *  \see random_float()
 *  \see srand48()
 *  \see clock()
 *  \see https://stackoverflow.com/questions/10056221/rand-issue-in-c-programming (accessed: 2021-11-03T16_01_53Z mer)
 *
 *  \return f_res A random float value within zero and one.
 */
float
random_float();

// srand(clock());
// https://stackoverflow.com/questions/10056221/rand-issue-in-c-programming
// 2021-11-03T16_01_53Z mer
float
frand();
/*
int
myrand();

float
nextrand();
*/

/*void
norm();*/

/*!
 *  \brief pi The constant function returns the value for pi.
 *  \details Using the variable p_e_pi (string pi number) as foundation.
 *  \see pi()
 *  \see https://oeis.org/A000796/constant
 *  \see https://oeis.org/A000796
 *
 *  \return f_pi The constant pi.
 */
float
pi();

/*!
 *  \brief e The constant function returns the value for euler's constant.
 *  \details Using the variable p_eu (string euler's number) as foundation.
 *  \see e()
 *  \see https://oeis.org/A001113/constant
 *
 *  \return f_e The constant e.
 */
float
e();

/*!
 *  \brief norm The (statisticial) function returns the normal distribution for given parameters.
 *  \details Using the variable p_pi (string for pi) as foundation.
 *  \param x The float x for a given value
 *  \param mu The float mu for a given mean.
 *  \param sigma The float sigma for a given standard deviation.
 *
 *  \see norm()
 *  \return res The result computed by using the given formula.
 *
 *  \see https://www.itl.nist.gov/div898/handbook/eda/section3/eda3661.htm
 *  \see https://oeis.org/A000796/constant (pi)
 *  \see https://oeis.org/A001113/constant (e)
 */
float
norm(float x_i, float mu, float variance);
/* END calcs.h END */

enum opera_enum
{
  e_NONE,                               /* like NULL */
  e_EXPR,
  e_LEFT,
  e_VAR_,
  e_ADD_,
  e_SUB_,
  e_MUL_,
  e_DIV_,
  e_LT__,
  e_GT__,
  e_MOD_,                               /* modulo */
  e_MIN_,
  e_MAX_,
  e_SUM_,
  e_SIN_,
  e_COS_,
  e_MOID,                               /* sigmoid */
  e_MEAN,
  e_ABS_,
  e_SQRT,
  e_DEV_,
  e_BRAC,                               /* bracket */
  e_STR_,                               /* string */
  e_FLT_,                               /* float */
  e_AND_,
  e_OPTS,
  e_RAND,                               /* random */
  e_RR__,                               /* random range */
  e_DN__,                               /* dnorm - normal distribution */
  e_PI__,                               /* pi */
  e_EUL_,                               /* eulers */
  e_ITE_,                               /* if-then-else */
};

/*!
 *  \brief xprint_enum The function prints out all enums as internal integer representation.
 *  \details An approach to get an overview about all enums. The print out can differ through time.
 *
 *  \see xprint_enum()
 */
void
xprint_enum();

typedef enum opera_enum Opera;

struct array
{
  float *f_arr;
  int i_len;
};

typedef struct array array;

struct node
{
  bool b_init;
  Opera e_opera;
  char *p_label;
  float f_value;
  struct node *left;
  struct node *center;
  struct node *prev;  
  struct node *next;
  struct node *right;
  int i_id;
};

typedef struct node node;
typedef struct node record;

struct list
{
  struct node *center;
  struct list *next;
  int i_id;
};
typedef struct list list;

/*!
 *  \brief make_node The function creates/allocates a node (structure having an operator, label, value, left, center or right neighbor).
 *  \details Allocates and remembers memory for a node/tree structure. Nodes are remembered by a list (p_up_nodes).
 *  \param e_opera The operator to be set (internal parse tree).
 *  \param p_label The label to be set (for T_IDS token).
 *  \param f_value The value to be set (for T_DGT token).
 *  \param node *left The left neighbor (in parse tree representation).
 *  \param node *center The center neighbor (in parse tree representation).
 *  \param node *right The right neighbor (in parse tree representation).
 *
 *  \see make_node()
 *  \return node* A pointer to a node structure (containing label, value, left, center and right neighbor).
 */
node *
make_node(Opera e_opera, char *p_label, float f_value, node *left, node *center, node *right);

/*!
 *  \brief free_node The function recursivly frees the memory of a given node/tree.
 *  \details Freeing the memory is for all left, center and right subtrees.
 *
 *  \see free_node()
 */
void
free_node(node *node);


/* NEW */

/*!
 *  \brief double_list_record The function add a double linked list to a record structure.
 *  \details double_list_record The function add a double linked list to a record structure by adding previous (prev) pointer(s).
 *  \param record *p_first A pointer to the first record.
 *
 *  \see double_list_record()
 *  \return p_first A pointer containing the record structure next and the new record.
 */
record *
double_list_record(record *p_first);

/*!
 *  \brief last_record The function returns a pointer to the last record.
 *  \details last_record The function returns a pointer to the last record (to use prev instead of next).
 *  \param record *p_first A pointer to the first record.
 *
 *  \see last_record()
 *  \return p_last A pointer containing the record structure (tail).
 */
record *
last_record(record *p_first);

/*!
 *  \brief make_record The function allocates memory for a record structure.
 *  \details Each record is a tuple of a label, a value and pointer to the next record.
 *  \param p_label A record label (string).
 *  \param f_value A float value.
 *  \param record *next A pointer to the next record.
 *
 *  \todo Implement a key-value store (as a more appropriate structure)!
 *
 *  \see make_record()
 *  \return p_new A pointer containing the record structure next and the new record.
 */
record *
make_record(char *p_str, float f_val, record *next);

/*!
 *  \brief make_list The function enables tracking (to afterwards free memory) by adding each node to a list.
 *  \details Creating lists by allocating memory for new list items (simple list having self/center and next).
 *  \param p_tree A pointer to a node (say a parse tree).
 *  \param p_next A pointer to the next (node item in list).
 *
 *  \see make_list()
 *  \return list* A pointer to a list structure (having p_next pointer to next node).
 */
list *
make_list(node *p_tree, list *p_next);
// node *get_tree(treelist *p_first, char *p_search);

/*!
 * \brief xfree_AST_list The function frees memory for a list of abstract syntax trees.
 * \details Creating lists by allocating memory for new list items (simple list having self/center and next).
 * \param p_list A pointer to a node (say a parse tree).
 *
 * \see xfree_AST_list()
 * \return list* A pointer to a list structure (having p_next pointer to next node).
 */
int
xfree_AST_list(list *p_list);

/*!
 *  \brief get_entry returns a node pointer for a given search key.
 *  \details The function returns a node using string compare.
 *  \param p_first The list pointer to the first element of a list structure.
 *  \param p_search The search key string.
 *
 *  \see get_record()
 *
 *  \return p_iter If key found, a record pointer will be returned.
 */
node *
get_entry(list *p_first, char *p_search);

/*!
 *  \brief eval Returns a float result using a given parse tree and key value record(s) for lookups.
 *  \details The parse tree structure results by default from node / tree structure.
            Each node has a type, i.e. operator, expressed by opera_enum. Hence, trees can be evaluated.
            Therefore the nodes left, center and right are evaluated recursively.
            See details in implementation. For more details see utils.h
 *  \param p_tree A node pointer to a node or tree structure.
 *  \param p_first A pointer to a record structure, which can be considered as key-value store.
 *
 *  \todo Implement a real non-linear complex key-value store.
 *  \todo Cleaning of printf.
 *
 *  \see eval()
 *  \return res A float, the evaluated result of eval using parse tree and record structure.
 */
float
eval(node *p_tree, record *p_first);

/*!
 *  \brief n_print The function returns a char string based on node label.
 *  \details The function returns a char string based on node label.
 *
 *  \todo Maybe, rewrite to get result only and return parameter as IN and OUT. 
 *
 *  \param p_node A pointer to a node or parse tree.
 *
 *  \see n_print()
 *  \return ptr_ret A string pointer.
 */
char *
n_print(node *p_node);

/*!
 *  \brief xprint_tree The function prints out a parse tree recursively.
 *  \details The function prints out a parse tree recursively.
 *
 *  \todo maybe, this function might be buggy or p_root have issues.
 *
 *  \param p_tree A pointer to a node or parse tree.
 *  \see xprint_tree()
 */
void
xprint_tree(node *p_tree);

/*!
 *  \brief xprint_node The function prints out most of a nodes attributes.
 *  \details The function prints out most of a nodes attributes.
 *  \param p_node A pointer to a node or parse tree.
 *  \see xprint_node()
 */
void
xprint_node(node *p_node);

/*!
 *  \brief get_value If a key is found, the value will be returned.
 *  \details The function checks for a valid key in a given record structure and returns the stored value.
 *  \param p_first The record pointer to the first element of a record structure.
 *  \param p_key The key to be check for.
 *
 *  \todo check using is_valid_key
 *
 *  \see get_value()
 *
 *  \return res The value, if key was found (if not found default valaue is -8888.0f).
 */
float
get_value(record *p_first, char *p_key);

/*!
 *  \brief print_keys Print out key value pairs for given records.
 *  \details Print out key value pairs for given records by iteration.
 *  \param p_first A pointer to the first record.
 *
 *  \todo Add a parameter delimiter.
 *
 *  \see print_keys()
 */
void
print_keys(record *p_first);	// another way to print the keys line by line

/*!
 *  \brief print_values Print out values for given records.
 *  \details Print out values for given records by iteration.
 *  \param p_first A pointer to the first record.
 *
 *  \see print_values()
 */
void
print_values(record *p_first);	// another way to print the keys line by line

/*!
 *  \brief opt1 Generate an array for a given number.
 *  \details Returns an array and allocate memory for a given count/length of floats.
 *  \param count The length of the array.
 *
 *  \see opt1()
 *  \return p_f_arr A float array of size count.
 */
float
*opt1(int count);

/*!
 *  \brief make_array Convert a record into a float array.
 *  \details Returns a pointer to float array using memory allocation.
 *  \param p_first A pointer to the first record.
 *
 *  \todo Free memory after usage.
 *
 *  \see make_array()
 *  \return p_f_arr A float array of size length records.
 */
float *
make_array(record *p_first);	// converting a given record structure to array.

/*!
 *  \brief xfree_array Free memory for a float array.
 *  \details Freeing memory for a given float array and decreasing i_make_array.
 *  \param p_f_arr A pointer to a float array.
 *
 *  \see xfree_array()
 *  \return Returns 0, if correctly memory freed correctly and decreased i_make_array.
 */
int
xfree_array(float *p_f_arr);

/*!
 *  \brief is_valid_key The function checks for a valid key in a given record structure.
 *  \details If record(s) contains a key return, then return true, else false.
 *  \param p_first The record pointer to the first element of a record structure.
 *  \param p_key The key to be check for.
 *
 *  \see is_valid_key()
 *
 *  \return b_contains If key was not found, value is false, else true.
 */
bool
is_valid_key(record *p_first, char *p_key);

/*!
 *  \brief get_record returns a record pointer for a search key.
 *  \details The function returns search key element pointer for given record(s).
 *  \param p_first The record pointer to the first element of a record structure.
 *  \param p_search_key The search key string.
 *
 *  \see get_record()
 *
 *  \return p_iter If key found, a record pointer will be returned.
 */
record *
get_record(record *p_first, char *p_search_key);

/*!
 *  \brief del_record If a key is found, the record and the value will be removed.
 *  \details The function checks for a valid key in a given record structure and removes key and value if found.
 *  \param p_first The record pointer to the first element of a record structure.
 *  \param p_delete_key The key to be check and deleted.
 *
 *  \todo check using is_valid_key
 *  \todo avoid memory leaks
 *
 *  \see del_record()
 *
 *  \return p_first If key found the altered record structure, without the p_delete_key and associated value.
 */
record *
del_record(record *p_first, char *p_delete_key);

/*!
 *  \brief update_record The function returns an updated record structure.
 *  \details Updating a record using update key and given float value returning record pointer
 *  \param p_first A pointer to the first record.
 *  \param p_update_key The key string to be updated.
 *  \param f_value The float value to be updated to.
 *
 *  \see update_record()
 *  \return record* A pointer to a record structure.
 */
record *
update_record(record *p_first, char *p_update_key, float f_value);

/*!
 *  \brief get_length The length for a given record.
 *  \details Counting the length for a given record by iteration.
 *  \param p_first A pointer to the first record.
 *
 *  \see get_length()
 *  \return int The length of a record as integer.
 */
int
get_length(record *p_first);

/*!
 *  \brief get_unsolved The number of unsolved records by using default value (-888.888888ff).
 *  \details Counting the length of default values (-888.888888ff) by iteration.
 *  \param p_first A pointer to the first record.
 *
 *  \see get_unsolved()
 *  \return int The number of unsolved / default values in a given record structure as integer.
 */
int
get_unsolved(record *p_record);

/*!
 *  \brief is_a_value Returns true if the given tree is a single node.
 *  \details The return value of type bool is true, if if the node itself
 *          has no left, center or right node; else false.
 *  \param p_tree A pointer to a node or tree.
 *
 *  \see is_a_value()
 *  \return is_value Is true, if the node itself has no left, center or right node.
 */
bool
is_a_value(node *p_tree);

/*!
 *  \brief walk_init The function calculates new results for each key.
 *  \details The function calculates new results for each key.
 *  \param p_init A pointer to a record structure.
 *  \param p_list A pointer to a list of abstract syntax trees.
 *  \see walk_init()
 *
 *  \return p_init A pointer for a record structure containing keys and values.
 */
record *
walk_init(record *p_init, list *p_list);

/*!
 *  \brief init_p_show The function returns a pointer for a record structure.
 *  \details The function returns a pointer for a record structure, containing
 *           a flat copy of key-values.
 *  \param p_show A pointer to a record structure.
 *  \param p_init A pointer to a record structure.
 *  \see init_p_show()
 *
 *  \return p_show A pointer for a record structure for selected key-values.
 */
record *
init_p_show(record *p_show, record *p_init);

/*!
 *  \brief pick_p_show The function returns a pointer for a record structure.
 *  \details The function returns a pointer for a record structure, containing
 *           selected key-values (display solution stored as p_show).
 *  \param p_tree The float array.
 *  \param p_show The length of the float array.
 *  \see pick_p_show()
 *
 *  \return p_show A pointer for a record structure for selected key-values.
 */
record *
pick_p_show(node *p_tree, record *p_show, record *p_init);

/*!
 *  \brief talk_STD The function prints out values for selected keys.
 *  \details The function prints out values for selected keys,
 *          to print out the results for each step.
 *  \param p_init A pointer to the record structure p_init (all key-values).
 *  \param p_AST A pointer to a list of abstract syntax trees (to be evaluated).
 *  \see talk_STD()
 */
void
talk_STD(record *p_init, list *p_AST); //, list *p_OPT);

/*!
 *  \brief talk_OPT The function prints out values for selected keys.
 *  \details The function prints out values for selected keys,
 *          to print out the results for each step.
 *  \param p_init A pointer to the record structure p_init (all key-values).
 *  \param p_show A pointer to the record structure p_show (a selection of key-values).
 *  \param p_AST A pointer to a list of abstract syntax trees (to be evaluated).
 *  \see talk_OPT()
 */
void
talk_OPT(record *p_init, record *p_show, list *p_AST); //, list *p_OPT);*/

/*!
 *  \brief xfree_records_eqns The function frees memory for a given record structure (eqns onlys).
 *  \details Iterate and free all record(s) for a given linked list of records.
 *           Free memory for records containing labels
 *           or records having no label pointer only (difference).
 *  \param p_first The record pointer to the first element of a record structure.
 *
 *  \see xfree_records_eqns()
 *  \return 0 As negative error code.
 */
int
xfree_records_eqns(record *p_some);

/*!
 *  \brief xfree_records The function frees memory for a given record structure.
 *  \details Iterate and free all record(s) for a given linked list of records.
 *  \param p_first The record pointer to the first element of a record structure.
 *
 *  \see xfree_records()
 *  \return 0 As negative error code.
 */
int
xfree_records(record *p_some);

/*!
 *  \brief screen A function to display credentials like author, version and licence.
 *  \details Reusage of comments fields from source files.
 */
void
screen(void);

/*!
 *  \brief xfree_list The function frees the memory for a list only.
 *  \details The function does not touch or free the content nodes.
 *  \param p_first The list pointer to the lists first item.
 *
 *  \see xfree_list()
 *  \return 0 As return code.
 */
int
xfree_list(list *p_first);

/*!
 *  \brief xprint_overview() The function prints an overview about the number of created and freed nodes and lists.
 *  \see xprint_overview()
 */
void
xprint_overview();

/*!
 *  \brief set_p_u_nodes The function enables transferring and tracking list structures between parser and utils.
 *  \param p_list A pointer to a list structure.
 *  \see set_p_u_nodes()
 */
void
set_p_u_nodes(list *p_list);

/*!
 *  \brief get_p_u_nodes() The function enables transferring and tracking list structures between parser and utils.
 *  \see set_record_uflex()
 *
 *  \return (list *) p_u_nodes
 */
list *
get_p_u_nodes();

/*!
 *  \brief set_record_uflex The function enables transferring and tracking record structures between parser and utils.
 *  \param p_rec A pointer to a record structure.
 *  \see set_record_uflex()
 */
void
set_u_flex(record *p_u_flex);

/*!
 *  \brief get_record_uflex The function enables transferring and tracking record structures between parser and utils.
 *  \see set_record_uflex()
 *
 *  \return (record*) p_flex_u
 */
record *
get_u_flex();

/*!
 *  \brief abs The function returns absolute value
 *  \details The function returns the absolute value for a given float.
 *  \param f_val The given float.
 *  \see abs()
 *
 *  \return f_res The absolute value.
 */
float
abs_f(float f_val);

/*!
 *  \brief sum The function returns the sum for a given float array.
 *  \details Default is -0.0;
 *  \param p_f_arr The float array.
 *  \param i_len The length of the float array.
 *  \see sum()
 *
 *  \return f_sum The sum for a given array.
 */
float
sum(float *p_f_arr, int i_len);

/*!
 *  \brief mean The function returns the mean for a given float array.
 *  \details Sum divided by length of array.
 *  \param p_f_arr The float array.
 *  \param i_len The length of the float array.
 *  \see mean()
 *
 *  \return f_mean The arithmetic mean.
 */
float
mean(float *p_f_arr, int i_len);

/*!
 *  \brief variance The function returns the variance for a given float array.
 *  \details Using mean for calculation.
 *  \param p_f_arr The float array.
 *  \param i_len The length of the float array.
 *  \see variance()
 *
 *  \return f_var The variance.
 */
float
variance(float *p_f_arr, int i_len);

/*!
 *  \brief stdev The function returns the standard deviation for a given variance.
 *  \details Using square root for calculation.
 *  \param f_variance The variance.
 *  \see stdev()
 *  \see Standard Deviation Calculator
 *  \see https://www.calculator.net/standard-deviation-calculator.html
 *       accessed: 2022-03-18T15-35-24Z mer
 *
 *  \return f_stdev The standard deviation.
 */
float
stdev(float f_variance);

/*!
 *  \brief random_range The function returns a random value in range of two floats.
 *  \details Depends on initial seed.
 *  \param f_i The left side of the interval.
 *  \param f_j The right side of the interval.
 *  \see random_range()
 *
 *  \return f_res The random value for an interval dependend on an initial seed.
 */
float
random_range(float f_s, float f_i, float f_j);

/*!
 *  \brief set_interactive The function sets interactive mode.
 *  \details b_interative == true for interactive mode.
 *  \param b_interactive
 *  \see set_interactive()
 */
void
set_interactive(bool *b_interactive);

/*!
 *  \brief get_interactive The function gets interactive mode.
 *  \details b_interative == true for interactive mode.
 *  \see get_interactive()
 *
 *  \return b_inter The interative mode
 */
bool *
get_interactive();

/*!
 *  \brief set_p_u_eqns The function sets the p_u_init
 *  \details p_u_eqns = p_m_init
 *  \param p_m_eqns
 *  \see set_p_u_eqns()
 */
void
set_p_u_eqns(record *p_m_eqns);

/*!
 *  \brief get_p_u_eqns The function gets the p_u_init
 *  \details Get p_u_eqns for main.c
 *  \see set_interactive()
 *
 *  \return p_u_eqns A record structure pointer
 */
record *
get_p_u_eqns();

/*!
 *  \brief set_p_u_init The function sets the p_u_init
 *  \details p_u_init = p_m_init
 *  \param p_m_init
 *  \see set_p_u_init()
 */
void
set_p_u_init(record *p_m_init);

/*!
 *  \brief get_p_u_init The function gets the p_u_init
 *  \details Get p_u_init for main.c
 *  \see set_interactive()
 *
 *  \return p_u_init A record structure pointer
 */
record *
get_p_u_init();

/*!
 *  \brief set_p_u_show The function sets the p_u_show
 *  \details p_u_show = p_m_init
 *  \param p_m_init
 *  \see set_p_u_show()
 */
void
set_p_u_show(record *p_m_show);

/*!
 *  \brief get_p_u_show The function gets the p_u_show
 *  \details Get p_u_show for main.c
 *  \see set_interactive()
 *
 *  \return p_u_show A record structure pointer
 */
record *
get_p_u_show();


/*!
 *  \brief set_p_u_AST The function sets the p_u_AST
 *  \details p_u_AST = p_m_AST
 *  \param p_m_AST
 *  \see set_p_u_AST()
 */
void
set_p_u_AST(list *p_m_AST);

/*!
 *  \brief get_p_u_AST The function gets the p_u_AST
 *  \details Get p_u_AST for main.c
 *  \see set_interactive()
 *
 *  \return p_u_AST A record structure pointer
 */
list *
get_p_u_AST();

/*!
 *  \brief set_p_u_OPT The function sets the p_u_OPT
 *  \details p_u_OPT = p_m_OPT
 *  \param p_m_OPT
 *  \see set_p_u_OPT()
 */
void
set_p_u_OPT(list *p_m_OPT);

/*!
 *  \brief get_p_u_OPT The function gets the p_u_OPT
 *  \details Get p_u_OPT for main.c
 *  \see set_interactive()
 *
 *  \return p_u_OPT A record structure pointer
 */
list *
get_p_u_OPT();

/*!
 *  \brief The function sets a bool for parsing mode
 *  \details set b_u_pars for main.c
 *  \param b_p_pars
 *  \see set_b_u_pars()
 *
 */
void
set_b_u_pars(bool b_p_pars);

/*!
 *  \brief The function gets a bool for parsing mode
 *  \details Get b_u_pars for main.c
 *  \see get_b_u_pars()
 *
 *  \return b_u_pars A bool for parsing
 */
bool
get_b_u_pars();


/*!
 *  \brief The function sets a bool for writing to a DOT/gv file
 *  \details set b_u_dots for main.c
 *  \param b_p_dots
 *  \see set_b_u_dots()
 *
 */
void
set_b_u_dots(bool b_p_dots);

/*!
 *  \brief The function gets a bool for writing to a DOT/gv file
 *  \details Get b_u_dots for main.c
 *  \see get_b_u_dots()
 *
 *  \return b_u_dots A bool for writing to a DOT/gv file
 */
bool
get_b_u_dots();

/*!
 *  \brief The function sets a bool for exiting interactive mode
 *  \details set b_u_exit for main.c
 *  \param b_p_exit
 *  \see set_b_u_exit()
 *
 */
void
set_b_u_exit(bool b_p_exit);

/*!
 *  \brief The function gets a bool for exiting interactive mode
 *  \details Get b_u_exit for main.c
 *  \see get_b_u_exit()
 *
 *  \return b_u_exit A bool for exiting interactive mode
 */
bool
get_b_u_exit();


// resume 2022-04-04Z12_45_00Z mer
/*
record *p_u_init;
record *p_u_show;

record *p_u_flex;

node *  p_u_root;

// list of all abstract syntax trees
list *p_u_AST;
// list of selected abstract syntax trees
list *p_u_OPT;
list *p_u_nodes;

// file open, file read (fopen, fread)
//enum { SIZE = 4096 };
//size_t bytes_read;
//char buf[SIZE];
//YY_BUFFER_STATE yy_buffer;
// end
*/

void
pre_parse();

int
display();

void
post_parse();

void
set_b_u_show_all(bool show_all);

bool
get_b_u_show_all();

/*
 * switch to exit interactive mode
 * default set to 0
 * exit set to 1
 */
void
set_b_u_reset(bool b_reset);

bool
get_b_u_reset();

void
set_b_u_exit(bool b_reset);

bool
get_b_u_exit();

void
set_b_u_inta(bool b_inta);

bool
get_b_u_inta();

void
set_p_u_buff(char *p_m_buff);

char *
get_p_u_buff();


/*!
 *  \brief The function prints the length for an exiting string
 *  \details print_abs_fn_length (to test strlen)
 *  \see print_abs_fn_length()
 */
void
print_abs_fn_length(char *p_abs_fn);

/*!
 *  \brief The function prints the header for a dot file
 *         using model name as filename
 *  \details space padding is done using the strlen(basename)
 *  \see print_dot_header()
 */
void
print_dot_header(char *p_abs_fn);

/* $ASDPN3: src/utils.h,v 2022Q4 2022/10/05 11:25:09 mer Exp $ */
