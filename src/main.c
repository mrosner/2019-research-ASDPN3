/*******************************************************************************
* Name        : ASDPN3/src/main.c                                              *
* Author      : M.E.Rosner                                                     *
* E-Mail      : marty[at]rosner[dot]io                                         *
* Version     : 2022Q4                                                         *
* Copyright   : Copyright (C) 2019-2022 M.E.Rosner; Berlin; Germany            *
* License     : The MIT License                                                *
* Description : A System Dynamics Process(ing) Notation                        *
*******************************************************************************/

#include "utils.h"
#include "parser.tab.h"
#include "lexer.yy.h"

/* file open, file read (fopen, fread) */
enum { SIZE = 4096 };
size_t bytes_read, bytes_write;
char buf[SIZE];
YY_BUFFER_STATE yy_buffer;
/* end */

/* exit switch */
bool b_m_exit = false;

/* reset switch */
bool b_m_reset = false;

/* interactive mode switch */
bool b_m_inta = false;

/* DOT file switch */
bool b_m_dots = false;

/* parse file switch */
bool b_m_pars = false;

int main(int argc, char *argv[])
{
  int opt;
  const char *ifile;                  /* file to be parsed                   */
  const char *lfile;                  /* license file                        */
  const char *pfile;                  /* file to be parsed and simulated     */
  const char *dfile;                  /* DOT file, to create model graph     */

  char *p_cmd_a = "reset";
  char *p_cmd_b = "sleep 2";

  /*
   * TODO: multiple options (getopt)
   * 'a' (interactive mode)
   * 'c' check model
   * 'd' dot file
   * 'f' file
   * 'h' print help
   * 'i' input model
   * 'l' license
   * 'p' process model
   * 'o' output
   * ':' option needs a value
   * '?' unknown option
   */

  if ( argc == 1 )
  {
    /* unknown option recognized, print usage */
    /* printf("main.c, if ( argc == 1 ) param.\n"); */
    printf("Try 'ASDPN3 -h' for more information.\n");
    exit(EXIT_FAILURE);
  }

  while( (opt = getopt(argc, argv, ":p:d:hilv")) != -1 )
  {
    switch(opt)
    {
      case 'h':
        printf("opt: %c, optarg: %s\n", opt, optarg);
        printf("Usage: %s [OPTION]... [FILE]...\n", argv[0]);
        printf("Parse and process System Dynamics model files.\n");
        printf("\n");
	printf("  -d [FILE]               Create DOT/gv [FILE] of model\n");
	printf("  -h                      Print help\n");
        printf("  -i                      interactive mode\n");
	printf("  -l                      Print license\n");
        printf("  -p [FILE]               Parse from [FILE] only mode\n");
        printf("  -v                      Print current version\n");
	printf("\n");
        printf("Examples:\n");
        printf("  ASDPN3 -d model.mdl > \\\n");
        printf("            graph.dot     Parse & output DOT/gv file.\n");
        printf("  ASDPN3 -i               Start interactive modeling.\n");
        printf("  ASDPN3 -p model.mdl > \\\n");
        printf("            data.csv      Parse & process model, output data to csv.\n");
        printf("  ASDPN3 -l               Print license.\n");
        printf("  ASDPN3 -h               Print this help.\n");
	printf("\n");
        printf("ASDPN3:\n");
        printf("A System Dynamics Process(ing) Notation\n");
	printf("\n");
	printf("License:\n");
        system("head -n3 COPYING.mit");
	exit(EXIT_FAILURE);
        break;
      case 'i':
        printf("ASDPN3:\n");
        printf("A System Dynamics Process(ing) Notation\n");
        printf("\n");
	printf("Version:\n");
        /* printf("2022-04-27T16-31-53Z\n");
         * printf("2022-05-06T10-14-24Z\n");
         * printf("2022-05-24T15-14-54Z\n");
         * printf("2022-07-28T21-37-05Z\n");
         * printf("\n");
         * printf("2022Q3 2022/09/01 18:00:00Z\n");
         */
        printf("2022Q4 2022/10/05 11:25:09Z\n");
        printf("let's start.\n");
        /* start screen */
/* */
//        system(p_cmd_a);
//        screen();
//        system(p_cmd_b);
//        system(p_cmd_a);
/* */
        // printf("opt: %c, optarg: %s\n", opt, optarg);
        // yydebug = 1;

        /* interactive mode true */
        b_m_inta = true;
        set_b_u_inta(b_m_inta);
        /*
        pre_parse();
        while ( b_m_exit == false )
        {
          int inter_ok = yyparse();
          b_m_exit = get_b_u_exit();
          // printf("main.c, interactive -i, inter_ok: %i\n", inter_ok);
          // printf("main.c, interactive -i, i_m_exit: %i\n", i_m_exit);
        }
        // display();
        post_parse();
        yylex_destroy();
        */
        b_m_reset = get_b_u_reset();        
        while (  b_m_exit == false )
        {
          system(p_cmd_a);
          screen();
          system(p_cmd_b);
          system(p_cmd_a);

          pre_parse();
          while ( b_m_reset == false ) // && b_m_exit == false )
          {
            int inter_ok = yyparse();
            b_m_reset = get_b_u_reset();
            // printf("main.c, interactive, inner loop, inter_ok: %i\n", inter_ok);
            //printf("main.c, interactive, inner loop, b_m_reset: %i\n", b_m_reset);
            b_m_exit = get_b_u_exit();
            // printf("main.c, interactive, inner loop, inter_ok: %i\n", inter_ok);
            //printf("main.c, interactive, inner loop, b_m_exit: %i\n", b_m_exit);
          }
          // display();
          post_parse();
          yylex_destroy();

          b_m_reset = get_b_u_reset();
          // printf("main.c, interactive, outer loop, inter_ok: %i\n", inter_ok);
          // printf("main.c, interactive, outer loop, b_m_reset: %i\n", b_m_reset);
          b_m_exit = get_b_u_exit();
          // printf("main.c, interactive, outer loop, inter_ok: %i\n", inter_ok);
          // printf("main.c, interactive, outer loop, b_m_exit: %i\n", b_m_exit);
          b_m_reset = false;
        }
        break;
      case 'l':
        // printf("opt: %c, optarg: %s\n", opt, optarg);
        lfile = optarg;
	printf("License:\n");
        system("head -n3 COPYING.mit");
        // printf("\n");
        break;
      case 'd':
        /* Create DOT/gv [FILE] of model */
        b_m_dots = true;
        set_b_u_dots(b_m_dots);

        // printf("main.c, set_b_u_dots, b_m_dots is ");
        // printf("%s", b_m_dots ? "true" : "false");
        // printf("\n");

        // -p [FILE] parse only
        // printf("opt: %c, optarg: %s\n", opt, optarg);
        dfile = optarg;

        pre_parse();
        FILE *p_dfile;
        p_dfile = fopen(dfile, "r");
        if ( p_dfile == NULL)
        {
          // printf("main.c, can not open file %s\n", pfile);
          return 0;
        }
        bytes_read = fread(buf, sizeof *buf, SIZE, p_dfile);

        yy_buffer = yy_scan_string( buf );
//        printf("digraph graphname {\n");

        int dfile_ok = yyparse();
        // printf("main.c, yyparse(), 'p', ok! pfile_ok: %i\n", pfile_ok);

//        printf("}\n");
        
        yy_delete_buffer(yy_buffer);
        
        // TODO: file error handling.
        
        // close file
        fclose(p_dfile);
        post_parse();
        yylex_destroy();
        break;
      case 'p':
        // TODO: b_m_pars parse switch
        b_m_pars = true;
        set_b_u_pars(b_m_pars);
            
        /* -p [FILE] parse only */
        // printf("opt: %c, optarg: %s\n", opt, optarg);
        pfile = optarg;

        pre_parse();
        FILE *p_pfile;
        p_pfile = fopen(pfile, "r");
        if ( p_pfile == NULL)
        {
          /* printf("main.c, can not open file %s\n", pfile); */
          return 0;
        }
        bytes_read = fread(buf, sizeof *buf, SIZE, p_pfile);

        yy_buffer = yy_scan_string( buf );
        int pfile_ok = yyparse();

//  record *p_u_eqns = NULL;
//  p_u_eqns = get_p_u_eqns();
//  print_keys(p_u_eqns);


        // printf("main.c, yyparse(), 'p', ok! pfile_ok: %i\n", pfile_ok);
        yy_delete_buffer(yy_buffer);

        /*
         * TODO: file error handling.
         */
        /* close file */
        fclose(p_pfile);
        post_parse();
        yylex_destroy();
        break;
      case 'v':
        // printf("opt: %c, optarg: %s\n", opt, optarg);
	printf("Version:\n");
        /* printf("2022-04-27T16-31-53Z\n");
         * printf("2022-05-06T10-14-24Z\n");
         * printf("2022-05-24T15-14-54Z\n");
         * printf("2022-07-29T13-00-00Z\n");
         * printf("2022Q3 2022/09/01 18:00:00Z\n");
         */
        printf("2022Q4 2022/10/05 11:25:09Z\n");
        printf("\n");
        break;
      case ':':
        printf("missing argument.\n");
        break;
      case '?':
        /* unknown option recognized, print usage */
        printf("Try 'ASDPN3 -h' for more information.\n");
        exit(EXIT_FAILURE);
    }
  }
  return 0;
}

/* $ASDPN3: src/main.c,v 2022Q4 2022/10/05 11:25:09 mer Exp $ */
