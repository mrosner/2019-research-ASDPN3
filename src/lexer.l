/*******************************************************************************
* Name        : ASDPN3/src/lexer.l                                             *
* Author      : M.E.Rosner                                                     *
* E-Mail      : marty[at]rosner[dot]io                                         *
* Version     : 2022Q4                                                         *
* Copyright   : Copyright (C) 2019-2022 M.E.Rosner; Berlin; Germany            *
* License     : The MIT License                                                *
* Description : A System Dynamics Process(ing) Notation                        *
*******************************************************************************/

%option header-file="lexer.yy.h"
%option noyywrap
%option yylineno

%{
/* #include <stdio.h> */

/* AST, OPT, nodes, math, etc. */
#include "utils.h"
#include "parser.tab.h"

record *p_l_flex = NULL;

%}

%%
"begin"|"BEGIN"                 {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_BEG;               }
"end"|"END"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_END;               }
"cloud"|"CLOUD"                 {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_CLD;               }
"clo"|"CLO"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_CLD;               }
"cld"|"CLD"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_CLD;               }
"stock"|"STOCK"                 {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_STK;               }
"sto"|"STO"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_STK;               }
"stk"|"STK"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_STK;               }
"flow"|"FLOW"                   {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_FLW;               }
"flo"|"FLO"       {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_FLW;               }
"flw"|"FLW"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_FLW;               }
"aux"|"AUX"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_AUX;               }
"auxil"|"AUXIL"                 {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_AUX;               }
"param"|"PARAM"                 {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_PRM;               }
"par"|"PAR"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_PRM;               }
"prm"|"PRM"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_PRM;               }
"eqn"|"EQN"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_EQN;               }
"equal"|"EQUAL"                 {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_EQN;               }
"use"|"USE"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_EQN;               }
"fun"|"FUN"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_FUN;               }
"func"|"FUNC"                   {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_FUN;               }
"function"|"FUNCTION"           {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_FUN;               }
"arc"|"ARC"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_ARC;               }
"edge"|"EDGE"                   {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_ARC;               }
"from"|"FROM"                   {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_FRM;               }
"to"|"TO"                       {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T__TO;               }
"time"|"TIME"                   {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_TIM;               }
"with"|"WITH"                   {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_WTH;               }
"neu"|"NEU"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_NEU;               }
"neutral"|"NEUTRAL"             {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_POS;               }
"neg"|"NEG"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_NEG;               }
"negative"|"NEGATIVE"           {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_NEG;               }
"pos"|"POS"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_POS;               }
"positive"|"POSITIVE"           {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_POS;               }
"influence"|"INFLUENCE"         {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_INF;               }
"abs"|"ABS"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_ABS;               }
"min"|"MIN"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_MIN;               }
"max"|"MAX"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_MAX;               }
"sum"|"SUM"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_SUM;               }
"sin"|"SIN"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_SIN;               }
"cos"|"COS"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_COS;               }
"sqrt"|"SQRT"                   {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_SRT;               }
"sd"|"stdev"                    {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_DEV;               }
"SD"|"STDEV"                    {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_DEV;               }
"dn"|"dnorm"                    {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_DN_;               }
"DN"|"DNORM"                    {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_DN_;               }
"sigmoid"|"SIGMOID"             {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_MOI;               }
"scurve"|"SCURVE"               {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_MOI;               }
"mean"|"MEAN"                   {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_MEA;               }
"e"|"E"                         {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_EUL;               }
"pi"|"PI"                       {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_PI_;               }
"rand"|"random"                 {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_RND;               }
"RAND"|"RANDOM"                 {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_RND;               }
"range"|"RANGE"                 {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_RNG;               }
"rng"|"RNG"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_RNG;               }
"rrange"|"RRANGE"               {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_RR_;               }
"rr"|"RR"                       {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_RR_;               }
"randrange"|"RANDRANGE"         {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_RR_;               }
"?"|"if"                        {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_ITE;               }
"if_then_else"|"IF_THEN_ELSE"   {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_ITE;               }
"if-then-else"|"IF-THEN-ELSE"   {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_ITE;               }
"ifthenelse"|"IFTHENELSE"       {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_ITE;               }
"i_t_e"|"I_T_E"                 {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_ITE;               }
"i-t-e"|"I-T-E"                 {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_ITE;               }
"ite"|"ITE"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_ITE;               }
 /* NULL - There is no pointer necessary */
 /* [\n]+                           { */
"\n"                            {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_EOL;               }
[ \t]                           {
    ;                           }
 /* %show | %SHOW is obsolete */
"%show"|"%SHOW"                 {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_DIS;               }
"net"|"NET"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_NET;               }
"display"|"DISPLAY"             {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_DIS;               }
"dis"|"DIS"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_DIS;               }
"dsp"|"DSP"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_DIS;               }
"all"|"ALL"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_ALL;               }
"#"(.|"\["|"\]"|"\."|"\;"|"\:"|"\-"|"\/"|"\("|"\)"|"\{"|"\}"|"\'"|"\|"|"\""|"\'")* {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_CMT;               }
 /* "#"(.|\.)* (expandable to preferences) */
"{"                            {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_C_S;               }
"}"                            {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_C_E;               }
".."                            {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_DDD;               }
"exec"|"EXEC"                   {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_EXE;               }
"in"|"IN"                       {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T__IN;               }
"out"|"OUT"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_OUT;               }
"inout"|"INOUT"                 {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_I_O;               }
"for"|"FOR"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_FOR;               }
"\', \'"|"\", \""       {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_STR;               }
"opt"|"OPT"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_OPT;               }
"simul"|"SIMUL"                 {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_SIM;               }
"sim"|"SIM"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_SIM;               }
"model"|"MODEL"                 {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_MDL;               }
"mdl"|"MDL"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_MDL;               }
"mod"|"MOD"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_MDL;               }
"def"|"DEF"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_DFN;               }
"define"|"DEFINE"               {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_DFN;               }
"return"|"RETURN"               {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_RET;               }
"del"|"DEL"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_DEL;               }
"qit"|"QIT"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_XIT;               }
"quit"|"QUIT"               {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_XIT;               }
"exit"|"EXIT"                   {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_XIT;               }
"xit"|"XIT"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_XIT;               }
"reset"|"RESET"                 {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_RST;               }
"rst"|"RST"                     {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_RST;               }
"<"                             {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_LT_;               }
"lt"|"LT"                       {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_LT_;               }
"less-than"|"LESS-THAN"         {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_LT_;               }
">"                             {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_GT_;               }
"gt"|"GT"                       {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_GT_;               }
"greater-than"|"GREATER-THAN"   {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_GT_;               }

":"                             {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_CLN;               }
";"                             {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_SEM;               }
","                             {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_CMA;               }

 /* 1 | 0.2; .3; .33; 0.12 (primarily floats) */
[0-9]+|([0-9]*"."[0-9]+)        {
    yylval.snd = make_node(e_FLT_, NULL, atof((char *)yytext), NULL, NULL, NULL);
    return T_DGT;               }

 /*
  * identifiers / variabels
  * first    part: i.e. s_teac (makes use of prefixes)
  * second   part: i.e. a, b, heat, cup (multiple chars)
  * another regex: [a-zA-Z]([-_]|[0-9a-zA-Z])*[0-9a-zA-Z_]+
  * [a-zA-Z][0-9a-zA-Z_-]*[0-9a-zA-Z_]+|[a-zA-Z]* OBSOLETE.
  */
[a-zA-Z][0-9a-zA-Z_-]*[0-9a-zA-Z_]+|[a-zA-Z]+ {
    char *p_label = calloc(1, strlen(yytext) + 1);
    /*
     * location for records (global variables)
     * get, set variables
     */
    p_l_flex = (record *) get_u_flex();
    /*
     * add this variable (T_IDS) to record structure
     */
    p_l_flex = make_record(p_label, 0.0f, p_l_flex);
    strcpy(p_label, (char *) yytext);
    set_u_flex(p_l_flex);
    yylval.snd = make_node(e_VAR_, p_label, 0.0f, NULL, NULL, NULL);
    p_l_flex = get_u_flex();
    /*
     * free memory
     * with xfree_records(record *p_first)
     */
    return T_IDS;               }
 /*
"="                             {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_SAM;               }
*/
"="|":="|"<-"                   {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_DEF;               }
"-"                             {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_SUB;               }
"+"                             {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_ADD;               }
"/"                             {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_DIV;               }
"*"                             {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_MUL;               }
"%"                             {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_MOD;               }

"("                             {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_LPS;               }
")"                             {
    yylval.snd = make_node(e_STR_, NULL, 0.0f, NULL, NULL, NULL);
    return T_RPS;               }

.                               {
    fprintf(stderr,"unknown / unspecified at: %d\n",yylineno);
                                }
%%

/* $ASDPN3: src/lexer.l,v 2022Q4 2022/10/05 11:25:09 mer Exp $ */
