/*******************************************************************************
* Name        : ASDPN3/src/parser.y                                            *
* Author      : M.E.Rosner                                                     *
* E-Mail      : marty[at]rosner[dot]io                                         *
* Version     : 2022Q4                                                         *
* Copyright   : Copyright (C) 2019-2022 M.E.Rosner; Berlin; Germany            *
* License     : The MIT License                                                *
* Description : A System Dynamics Process(ing) Notation                        *
*******************************************************************************/

%{
// #include <stdio.h>
// #include <unistd.h>

#include "utils.h"
#include "parser.tab.h"
#include "lexer.yy.h"

/* from utils */
node   *p_p_root = NULL;
record *p_p_init = NULL;
record *p_p_show = NULL;
record *p_p_eqns = NULL;

record *p_flex_p = NULL;

list *p_p_AST = NULL;
list *p_p_OPT = NULL;
// list *p_p_EQN = NULL;
list *p_p_nodes;

float res_default = 0.0f;               /* alternative with -888.888888f */

//bool b_p_show_all = false;

bool b_p_dots = false;
bool b_p_exit = false;
bool b_p_reset = false;
bool b_p_inta = false;
bool b_p_pars = false;

/* end counter */
int i_end = 0;

void yyerror(char const *msg);

int yywrap();

%}

%union { float flt; node *snd; }        /* char *sym; */

/* basic tokens */
%token <snd> T_IDS
%token <snd> T_CMA
%token <snd> T_CMT
%token <snd> T_DEF
%token <snd> T_SAM
%token <snd> T_EOL
%token <snd> T_SEM
%token <snd> T_DGT
%token <snd> T_LPS
%token <snd> T_RPS
%token <snd> T_END
%token <snd> T_DFN
%token <snd> T_MDL

/* functions */
%token <snd> T_MIN T_MAX T_SUM
%token <snd> T_SIN T_COS
%token <snd> T_MOI T_MEA
%token <snd> T_SRT T_DEV
%token <snd> T_RND
%token <snd> T_RR_
%token <snd> T_RNG
%token <snd> T_DN_
%token <snd> T_PI_
%token <snd> T_EUL

/* system dynamics */
%token <snd> T_BEG
%token <snd> T_CLD
%token <snd> T_STK
%token <snd> T_FLW
%token <snd> T_AUX
%token <snd> T_PRM
%token <snd> T_EQN
%token <snd> T_FUN

/* simulation specific */
%token <snd> T_TIM
%token <snd> T_SIM
%token <snd> T_DIS
%token <snd> T_OPT

/* graph specific */
%token <snd> T_ARC
%token <snd> T_FRM
%token <snd> T__TO
%token <snd> T_WTH
%token <snd> T_NEG
%token <snd> T_POS
%token <snd> T_NEU
%token <snd> T_INF
%token <snd> T_NET

/* additional */
%token <snd> T_STR
%token <snd> T_CLN
%token <snd> T_RET
%token <snd> T_FOR
%token <snd> T__IN
%token <snd> T_EXE
%token <snd> T_DDD
%token <snd> T_OUT
%token <snd> T_I_O
%token <snd> T_DEL
%token <snd> T_XIT
%token <snd> T_RST
%token <snd> T_ALL
%token <snd> T_ABS

/* if-then-else */
%token <snd> T_ITE
%token <snd> T_LT_
%token <snd> T_GT_

/* comments */
/*
 * TODO: overlapping / line breaking comments
 * start comment
 * end comment #{, #}
 */
%token <snd> T_C_S
%token <snd> T_C_E

/* operators */
%left T_SUB T_ADD
%left T_DIV T_MUL
%left T_MOD

/*precedence for unary minus */
%left UMINUS

 /* nonterminals */
%type <snd> lines line_
/* %type <snd> _end_ */
%type <snd> commt
%type <snd> funct
%type <snd> expre
%type <snd> defin
%type <snd> arity
%type <snd> funde
%type <snd> fhead
%type <snd> fbody
%type <snd> ffoot
/*%type <snd> fhead */
/*%type <snd> fbody*/
/*%type <snd> _end_*/
/*%type <snd> funXZ*/
/*%type <snd> simXZ*/


%start lines

%%

/*inter:
    lines
  | funXZ
;
*/
lines:
    /*funXZ*/
    /*fhead*/
    line_
  {
    node *n_line_ = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>1, NULL);
    $$=n_line_;
  }
  | lines line_
  {
    node *n_line_ = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>2, NULL);
    node *n_lines = make_node(e_LEFT, NULL, 0.0f, n_line_, NULL, $<snd>1);
    $$ = n_lines;
    p_p_root = $$;
  }
  | funde
  {
  }
  | lines funde
  {
  }
;

/*
 * fun pos_r = abs_rand():
 *     pos_r := abs(random()); # 1.00;
 * end;
 */
funde:
    fhead fbody ffoot
;

fhead:
    T_FUN T_IDS T_DEF T_IDS T_LPS arity T_RPS T_CLN       T_EOL
  | T_FUN T_IDS T_DEF T_IDS T_LPS       T_RPS T_CLN       T_EOL
  | T_FUN T_IDS T_DEF T_IDS T_LPS arity T_RPS T_CLN commt T_EOL
  | T_FUN T_IDS T_DEF T_IDS T_LPS       T_RPS T_CLN commt T_EOL

;

fbody:
                expre T_SEM       T_EOL
  |             expre T_SEM commt T_EOL
  | T_IDS T_DEF expre T_SEM       T_EOL
  | T_IDS T_DEF expre T_SEM commt T_EOL
/*  | funct T_SEM T_EOL */
;

ffoot:
    T_END T_SEM       T_EOL
  | T_END T_SEM commt T_EOL
;

/*
 * end;
 * end
 */
/*
_end_:
    T_END             T_EOL
  | T_END       commt T_EOL
  | T_END T_SEM       T_EOL
  | T_END T_SEM commt T_EOL
;
*/

line_:
    T_EOL
  /*| simXZ*/
  /* function signature */
  /* SEE funde
  | T_FUN T_IDS T_LPS arity T_RPS T_CLN T_IDS T_CLN T_EOL
  | T_FUN T_IDS T_DEF T_IDS T_LPS       T_RPS T_CLN T_EOL
  | T_FUN T_IDS T_DEF T_IDS T_LPS arity T_RPS T_CLN T_EOL
  */
  /* for value in array */
  | T_FOR T_IDS T__IN T_IDS T_CLN
  /* define line after in function */
  | defin             T_SEM       T_EOL
  | defin             T_SEM commt T_EOL
  /* end - _end_ */
  | T_END T_SEM       T_EOL
  {
    b_p_dots = get_b_u_dots();
    /* end count is required to avoid
     * multiple closing '}' for -d option */
    i_end = i_end + 1;
    if ( b_p_dots == true && i_end == 1 )
    {
      printf("}\n");
    }
  }
  | T_END T_SEM commt T_EOL
  {
    b_p_dots = get_b_u_dots();
    /* end count is required to avoid
     * multiple closing '}' for -d option */
    i_end = i_end + 1;
    if ( b_p_dots == true && i_end == 1 )
    {
      printf("}\n");
    }
  }
  /* comment */
  | commt T_EOL
  | expre T_SEM       T_EOL
  | expre T_SEM commt T_EOL
/*
 * TODO: add self-defined functions
 * basic testing purpose (obsolete, but remains)
 *  | expre T_EOL
 *  | stats T_EOL
 *  | param T_EOL
 *  | funct T_EOL
 *
 * teacup = def init();
 *  | T_IDS T_DEF T_DFN T_IDS T_LPS       T_RPS T_SEM T_EOL
 *  | T_IDS T_DEF T_DFN T_IDS T_LPS       T_RPS T_SEM commt T_EOL
 *
 * TODO: signatures for functions
 * | param T_EOL
 * | param commt T_EOL
 *
 * a, b, c, d = init()                          # "with n=20 steps"
 * a, b = teacup(a, b, c, d)                    # run model teacup
 *
 * | param T_DEF funct
 *
 * TODO: arity newline break
 * a, b = pred(a, b, c, d, e,
 *             f, g, h, i, j,
 *             l, m, n, o )
 * | fpart T_EOL
 * | fpart commt T_EOL
 * | param T_CMA T_EOL
 * | param T_CMA commt T_EOL
 */
/* mod teacup; */
  | T_MDL T_IDS T_SEM       T_EOL
  {
    b_p_dots = get_b_u_dots();
    if ( b_p_dots == true )
    {
      print_dot_header($<snd>2->p_label);
      printf("\n");
      printf("digraph %s {\n", $<snd>2->p_label);
    }
  }
  | T_MDL T_IDS T_SEM commt T_EOL
  {
    b_p_dots = get_b_u_dots();
    if ( b_p_dots == true )
    {
      print_dot_header($<snd>2->p_label);
      printf("\n");
      printf("digraph %s {\n", $<snd>2->p_label);
    }
  }
/* begin model teacup; */
  | T_BEG T_MDL T_IDS T_SEM       T_EOL
  {
    b_p_dots = get_b_u_dots();
    if ( b_p_dots == true )
    {
      print_dot_header($<snd>2->p_label);
      printf("\n");
      printf("digraph %s {\n", $<snd>3->p_label);
    }
  }
  | T_BEG T_MDL T_IDS T_SEM commt T_EOL
  {
    b_p_dots = get_b_u_dots();
    if ( b_p_dots == true )
    {
      print_dot_header($<snd>2->p_label);
      printf("\n");
      printf("digraph %s {\n", $<snd>3->p_label);
    }
  }
/* end; */
/*  | _end_ */
/* end model; */
  | T_END T_MDL       T_SEM T_EOL
  {
    b_p_dots = get_b_u_dots();
    /* end count is required to avoid
     * multiple closing '}' for -d option */
    i_end = i_end + 1;
    if ( b_p_dots == true && i_end == 1 )
    {
      printf("}\n");
    }
  }
  | T_END T_MDL       T_SEM commt T_EOL
  {
    b_p_dots = get_b_u_dots();
    /* end count is required to avoid
     * multiple closing '}' for -d option */
    i_end = i_end + 1;
    if ( b_p_dots == true && i_end == 1 )
    {
      printf("}\n");
    }
  }
/* cloud  sink_01;                              # A sink node */
  | T_CLD arity T_SEM       T_EOL
  | T_CLD arity T_SEM commt T_EOL
/*
 * overview lines
 *
 * mod teacup;
 * stock   a = 95.0;                            # temperature
 * flow    b;                                   # heat loss to room
 * auxil   x;                                   # x (auxiliary)
 * param   c = 70.0;                            # room temperature
 * param   d = 10.0;                            # characteristic time
 * eqn     b = ( a - c ) / d;                   # equations
 * eqn     a = a - b;                           # step by step
 */

/*
 * stock   a = 180.0, b = 120.0;                # stock a, b
 *
 * | T_STK T_IDS T_DEF T_DGT T_SEM       T_EOL
 * | T_STK T_IDS T_DEF T_DGT T_SEM commt T_EOL
 */
  | T_STK defin             T_SEM       T_EOL
  {
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>3);
    node *n_defin = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    node *n_t_stk = make_node(e_LEFT, NULL, 0.0f, n_defin, NULL, $<snd>1);
    $$=n_t_stk;
  }
  | T_STK defin             T_SEM commt T_EOL
  {
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>5, NULL);
    node *n_commt = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>4);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_commt, NULL, $<snd>3);
    node *n_defin = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    node *n_t_stk = make_node(e_LEFT, NULL, 0.0f, n_defin, NULL, $<snd>1);
    $$=n_t_stk;
  }
/*
 * flow    a = 2.0, b = 0.0;                    # "a - heat loss room; b"
 * flow    f_a, f_b;                            # "multiple flow variables"
 *
 * | T_FLW T_IDS T_DEF T_DGT T_SEM       T_EOL
 * | T_FLW T_IDS T_DEF T_DGT T_SEM commt T_EOL
 */
  | T_FLW defin             T_SEM       T_EOL
  {
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>3);
    node *n_defin = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    node *n_t_flw = make_node(e_LEFT, NULL, 0.0f, n_defin, NULL, $<snd>1);
    $$=n_t_flw;
  }
  | T_FLW defin             T_SEM commt T_EOL
  {
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>5, NULL);
    node *n_commt = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>4);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_commt, NULL, $<snd>3);
    node *n_defin = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    node *n_t_flw = make_node(e_LEFT, NULL, 0.0f, n_defin, NULL, $<snd>1);
    $$=n_t_flw;
  }
  | T_FLW arity             T_SEM       T_EOL
  {
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>3);
    node *n_arity = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    node *n_t_flw = make_node(e_LEFT, NULL, 0.0f, n_arity, NULL, $<snd>1);
    $$=n_t_flw;
  }
  | T_FLW arity             T_SEM commt T_EOL
  {
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>5, NULL);
    node *n_commt = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>4);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_commt, NULL, $<snd>3);
    node *n_arity = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    node *n_t_flw = make_node(e_LEFT, NULL, 0.0f, n_arity, NULL, $<snd>1);
    $$=n_t_flw;
  }
/*
 * auxil   x = 0.0, y = 2.0, z = 0.5;           # "x - an artificial int"
 * auxil   x;                                   # "x - an artificial int"
 *
 * | T_AUX T_IDS T_SEM       T_EOL
 * | T_AUX T_IDS T_SEM commt T_EOL
 * | T_AUX T_IDS T_DEF T_DGT T_SEM       T_EOL
 * | T_AUX T_IDS T_DEF T_DGT T_SEM commt T_EOL
 */
  | T_AUX defin             T_SEM       T_EOL
  {
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>3);
    node *n_defin = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    node *n_t_aux = make_node(e_LEFT, NULL, 0.0f, n_defin, NULL, $<snd>1);
    $$=n_t_aux;
  }
  | T_AUX defin             T_SEM commt T_EOL
  {
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>5, NULL);
    node *n_commt = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>4);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_commt, NULL, $<snd>3);
    node *n_defin = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    node *n_t_aux = make_node(e_LEFT, NULL, 0.0f, n_defin, NULL, $<snd>1);
    $$=n_t_aux;
  }
  | T_AUX arity             T_SEM       T_EOL
  {
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>3);
    node *n_arity = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    node *n_t_aux = make_node(e_LEFT, NULL, 0.0f, n_arity, NULL, $<snd>1);
    $$=n_t_aux;
  }
  | T_AUX arity             T_SEM commt T_EOL
  {
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>5, NULL);
    node *n_commt = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>4);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_commt, NULL, $<snd>3);
    node *n_arity = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    node *n_t_aux = make_node(e_LEFT, NULL, 0.0f, n_arity, NULL, $<snd>1);
    $$=n_t_aux;
  }
/*
 * param   a = 2.99, b = 0.34, c =  70.0;       # "c - room temperature"
 *
 * | T_PRM T_IDS T_DEF T_DGT T_SEM       T_EOL
 * | T_PRM T_IDS T_DEF T_DGT T_SEM commt T_EOL
 */
  | T_PRM defin              T_SEM       T_EOL
  {
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>3);
    node *n_defin = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    node *n_t_prm = make_node(e_LEFT, NULL, 0.0f, n_defin, NULL, $<snd>1);
    $$=n_t_prm;
  }
  | T_PRM defin              T_SEM commt T_EOL
  {
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>5, NULL);
    node *n_commt = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>4);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_commt, NULL, $<snd>3);
    node *n_defin = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    node *n_t_prm = make_node(e_LEFT, NULL, 0.0f, n_defin, NULL, $<snd>1);
    $$=n_t_prm;
  }
  | T_PRM arity             T_SEM       T_EOL
  {
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>3);
    node *n_arity = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    node *n_t_prm = make_node(e_LEFT, NULL, 0.0f, n_arity, NULL, $<snd>1);
    $$=n_t_prm;
  }
  | T_PRM arity             T_SEM commt T_EOL
  {
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>5, NULL);
    node *n_commt = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>4);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_commt, NULL, $<snd>3);
    node *n_arity = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    node *n_t_prm = make_node(e_LEFT, NULL, 0.0f, n_arity, NULL, $<snd>1);
    $$=n_t_prm;
  }
/*
 * equal   b = a / d - c / d                    # "equation for f_heat node"
 *                                              # serving both using param
 * | T_EQN arity T_SEM       T_EOL
 * | T_EQN arity T_SEM commt T_EOL
 */
  | T_EQN defin             T_SEM       T_EOL
  {
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>3);
    node *n_defin = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    node *n_t_eqn = make_node(e_LEFT, NULL, 0.0f, n_defin, NULL, $<snd>1);

    // xprint_tree(n_defin);

    // printf("\n\n");
    // printf("parser.y, n_defin->left->left->i_id, %i\n", n_defin->left->right->right->i_id);
    // printf("parser.y, n_defin->left->left->p_label, %s\n", n_defin->left->right->right->p_label);

//    printf("parser.y, i_id, %i\n",    n_defin->right->left->left->i_id);
//    printf("parser.y, p_label, %s\n", n_defin->right->left->left->p_label);
//    printf("parser.y, e_opera, %i\n", n_defin->right->left->->e_opera);

    // printf("parser.y, i_id, %i\n",    n_defin->right->right->i_id);
    // printf("parser.y, p_label, %s\n", n_defin->right->right->p_label);
    // printf("parser.y, e_opera, %i\n", n_defin->right->right->e_opera);

    p_p_eqns = get_p_u_eqns();
    char *p_label = n_defin->right->right->p_label;
    char *p_new_labelA = (char *) calloc(1, strlen(p_label) +1);
    strcpy(p_new_labelA, p_label);
    p_p_eqns = make_record(p_new_labelA, 0.0f, p_p_eqns);
    set_p_u_eqns(p_p_eqns);

    //xprint_enum();
    //xprint_node(n_defin->right->left->right);
    
    // store EQN trees in a list, using get / set.
//    p_p_EQN = get_p_u_EQN();
//    p_p_EQN = make_list(n_defin, p_p_EQN);
//    set_p_u_EQN(p_p_EQN);

    $$=n_t_eqn;
  }
  | T_EQN defin             T_SEM commt T_EOL
  {
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>5, NULL);
    node *n_commt = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>4);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_commt, NULL, $<snd>3);
    node *n_defin = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    // xprint_tree(n_defin);
    // printf("parser.y, n_defin->left->right->p_label, %s\n", n_defin->left->right->p_label);
    //    xprint_tree(n_defin);
    node *n_t_eqn = make_node(e_LEFT, NULL, 0.0f, n_defin, NULL, $<snd>1);

    p_p_eqns = get_p_u_eqns();
    p_p_eqns = make_record(n_defin->right->right->p_label, 0.0f, p_p_eqns);
    set_p_u_eqns(p_p_eqns);

    $$=n_t_eqn;
  }
/*
 * edge   from TeaC to Heat with negative influence;
 * edge   from Heat to Sink_01 with negative influence;
 * edge   from Char to Heat;
 * edge   from Room to Heat;
 */
  | T_NET T_ARC T_FRM T_IDS T__TO T_IDS                   T_SEM       T_EOL
  {
    /* TODO: dot / gz */
    b_p_dots = get_b_u_dots();

    // printf("parser.y, get_b_u_dots, b_p_dots is ");
    // printf("%s", b_p_dots ? "true" : "false");
    // printf("\n");

      /* printf("parser.c, b_p_dots\n"); */
    if ( b_p_dots == true )
    {
      printf("  %s -> %s;\n", $<snd>4->p_label, $<snd>6->p_label);
    }

   // strcat(buf, "}\n");
   // strcat(buf, $<snd>4->p_label);
   // strcat(buf, " -> ");
   // strcat(buf, $<snd>6->p_label);
   // strcat(buf, ";");
   // printf("parser.c, %s\n", buf); */
   // set_p_u_buff(buf);
  }
  | T_NET T_ARC T_FRM T_IDS T__TO T_IDS                   T_SEM commt T_EOL
  {
      /* TODO: dot / gz */
    b_p_dots = get_b_u_dots();

    // printf("parser.y, get_b_u_dots, b_p_dots is ");
    // printf("%s", b_p_dots ? "true" : "false");
    // printf("\n");

      /* printf("parser.c, b_p_dots\n"); */
    if ( b_p_dots == true )
    {
      printf("  %s -> %s;\n", $<snd>4->p_label, $<snd>6->p_label);
    }
  }
  | T_NET T_ARC T_FRM T_IDS T__TO T_IDS T_WTH T_NEG T_INF T_SEM       T_EOL
  {
      /* TODO: dot / gz */
    b_p_dots = get_b_u_dots();

    // printf("parser.y, get_b_u_dots, b_p_dots is ");
    // printf("%s", b_p_dots ? "true" : "false");
    // printf("\n");

      /* printf("parser.c, b_p_dots\n"); */
    if ( b_p_dots == true )
    {
      printf("  %s -> %s;\n", $<snd>4->p_label, $<snd>6->p_label);
    }
  }
  | T_NET T_ARC T_FRM T_IDS T__TO T_IDS T_WTH T_NEG T_INF T_SEM commt T_EOL
  {
      /* TODO: dot / gz */
    b_p_dots = get_b_u_dots();

    // printf("parser.y, get_b_u_dots, b_p_dots is ");
    // printf("%s", b_p_dots ? "true" : "false");
    // printf("\n");

      /* printf("parser.c, b_p_dots\n"); */
    if ( b_p_dots == true )
    {
      printf("  %s -> %s;\n", $<snd>4->p_label, $<snd>6->p_label);
    }
  }
  | T_NET T_ARC T_FRM T_IDS T__TO T_IDS T_WTH T_POS T_INF T_SEM       T_EOL
  {
      /* TODO: dot / gz */
    b_p_dots = get_b_u_dots();

    // printf("parser.y, get_b_u_dots, b_p_dots is ");
    // printf("%s", b_p_dots ? "true" : "false");
    // printf("\n");

      /* printf("parser.c, b_p_dots\n"); */
    if ( b_p_dots == true )
    {
      printf("  %s -> %s;\n", $<snd>4->p_label, $<snd>6->p_label);
    }
  }
  | T_NET T_ARC T_FRM T_IDS T__TO T_IDS T_WTH T_POS T_INF T_SEM commt T_EOL
  {
      /* TODO: dot / gz */
    b_p_dots = get_b_u_dots();

    // printf("parser.y, get_b_u_dots, b_p_dots is ");
    // printf("%s", b_p_dots ? "true" : "false");
    // printf("\n");

      /* printf("parser.c, b_p_dots\n"); */
    if ( b_p_dots == true )
    {
      printf("  %s -> %s;\n", $<snd>4->p_label, $<snd>6->p_label);
    }
  }
/*
 * net    arc( s_a, f_b );                      # a comment
 */
  | T_NET T_ARC T_LPS T_IDS T_CMA T_IDS             T_RPS T_SEM       T_EOL
  {
      /* TODO: dot / gz */
    b_p_dots = get_b_u_dots();

    // printf("parser.y, get_b_u_dots, b_p_dots is ");
    // printf("%s", b_p_dots ? "true" : "false");
    // printf("\n");

      /* printf("parser.c, b_p_dots\n"); */
    if ( b_p_dots == true )
    {
      printf("  %s -> %s;\n", $<snd>4->p_label, $<snd>6->p_label);
    }
  }
  | T_NET T_ARC T_LPS T_IDS T_CMA T_IDS             T_RPS T_SEM commt T_EOL
  {
      /* TODO: dot / gz */
    b_p_dots = get_b_u_dots();

    // printf("parser.y, get_b_u_dots, b_p_dots is ");
    // printf("%s", b_p_dots ? "true" : "false");
    // printf("\n");

      /* printf("parser.c, b_p_dots\n"); */
    if ( b_p_dots == true )
    {
      printf("  %s -> %s;\n", $<snd>4->p_label, $<snd>6->p_label);
    }
  }
/*
 * net    arc( s_a, f_b, pos );                 # a comment
 */
  | T_NET T_ARC T_LPS T_IDS T_CMA T_IDS T_CMA influ T_RPS T_SEM       T_EOL
  {
      /* TODO: dot / gz */
    b_p_dots = get_b_u_dots();

    // printf("parser.y, get_b_u_dots, b_p_dots is ");
    // printf("%s", b_p_dots ? "true" : "false");
    // printf("\n");

      /* printf("parser.c, b_p_dots\n"); */
    if ( b_p_dots == true )
    {
      printf("  %s -> %s;\n", $<snd>4->p_label, $<snd>6->p_label);
    }
  }
  | T_NET T_ARC T_LPS T_IDS T_CMA T_IDS T_CMA influ T_RPS T_SEM commt T_EOL
  {
      /* TODO: dot / gz */
    b_p_dots = get_b_u_dots();

    // printf("parser.y, get_b_u_dots, b_p_dots is ");
    // printf("%s", b_p_dots ? "true" : "false");
    // printf("\n");

      /* printf("parser.c, b_p_dots\n"); */
    if ( b_p_dots == true )
    {
      printf("  %s -> %s;\n", $<snd>4->p_label, $<snd>6->p_label);
    }
  }
/*
 * opt    teacup;                               # some options
 */
  | T_OPT T_IDS T_SEM
  | T_OPT T_IDS commt T_SEM
/*
 * time   steps = 45;
 * time   steps = 4..45;
 */
  | T_TIM T_IDS T_DEF T_DGT             T_SEM       T_EOL
  | T_TIM T_IDS T_DEF T_DGT             T_SEM commt T_EOL
  | T_TIM T_IDS T_DEF T_DGT T_DDD T_DGT T_SEM       T_EOL
  | T_TIM T_IDS T_DEF T_DGT T_DDD T_DGT T_SEM commt T_EOL
/*
 * sim steps =      45;
 * sim steps = 4 .. 45;
 * sim steps = 4 to 45;
 *
 * | T_SIM T_IDS T_DEF T_DGT             T_SEM       T_EOL
 * | T_SIM T_IDS T_DEF T_DGT             T_SEM commt T_EOL
 * | T_SIM T_IDS T_DEF T_DGT T__TO T_DGT T_SEM commt T_EOL
 */
  | T_SIM defin                         T_SEM       T_EOL
  {
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>3);
    node *n_defin = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    node *n_t_sim = make_node(e_LEFT, NULL, 0.0f, n_defin, NULL, $<snd>1);
    $$=n_t_sim;
    // float f_test_ = eval(n_t_sim, p_p_init);
    // printf("parser.y, sim time steps = %f;\n", f_test_);
  }
  | T_SIM defin                         T_SEM commt T_EOL
  {
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>5, NULL);
    node *n_commt = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>4);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_commt, NULL, $<snd>3);
    node *n_defin = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    node *n_t_sim = make_node(e_LEFT, NULL, 0.0f, n_defin, NULL, $<snd>1);
    $$=n_t_sim;
    // float f_test_ = eval(n_t_sim, p_p_init);
    // printf("parser.y, sim time steps = %f;\n", f_test_);
  }
  | T_SIM T_IDS T_DEF T_DGT T_DDD T_DGT T_SEM       T_EOL
  | T_SIM T_IDS T_DEF T_DGT T_DDD T_DGT T_SEM commt T_EOL
  | T_SIM T_IDS T_DEF T_DGT T__TO T_DGT T_SEM       T_EOL
  | T_SIM T_IDS T_DEF T_DGT T__TO T_DGT T_SEM commt T_EOL
/*
 * TODO: a way orchestrate models using in, out, in/out
 * in     a, b, c, d;
 * out    a, b;
 * inout  t, d, x;
*/
  | T__IN arity T_SEM       T_EOL
  | T__IN arity T_SEM commt T_EOL
  | T_OUT arity T_SEM       T_EOL
  | T_OUT arity T_SEM commt T_EOL
  | T_I_O arity T_SEM       T_EOL
  | T_I_O arity T_SEM commt T_EOL
/*
 * dis x_x, x_y;
*/
  | T_DIS T_SEM commt T_EOL
  {
    // printf("parser.y, T_PRT variables T_SEM T_EOL\n");
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_commt = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>3);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_commt, NULL, $<snd>2);
    node *n_t_dis = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>1);
    $$=n_t_dis;
    set_b_u_show_all(true);
    //b_p_dots = get_b_u_dots();
    //if ( b_p_dots == false ) // display();
    //{
    //  int i_dis = display();
    //}
    int i_dis = display();    
    set_b_u_show_all(false);
  }
  | T_DIS T_SEM       T_EOL
  {
    // printf("parser.y, T_PRT variables T_SEM T_EOL\n");
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>3, NULL);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>2);
    node *n_t_dis = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>1);
    $$=n_t_dis;
    set_b_u_show_all(true);
    /* differenciate between interactive and parse only */
    //if ( b_p_inta )
    //{
      // printf("parser.y, T_DIS arity T_SEM T_EOL, b_p_inta: ");
      // printf("%s", b_p_inta ? "true" : "false");
      // printf("\n");
      //
      // not in dot mode
      //
      //b_p_dots = get_b_u_dots();
      //if ( b_p_dots == false ) // display();
      //{
        // int i_dis_inta = display();      
        //int i_dis_inta = display(); // display();
      //}
    //}
    //else
    //{
      //
      // printf("parser.y, T_DIS arity T_SEM T_EOL, b_p_inta: ");
      // printf("%s", b_p_inta ? "true" : "false");
      // printf("\n");
      //
      // not in dot mode
      //
      //b_p_dots = get_b_u_dots();
      // if ( b_p_dots == false ) // display();
      //{
      //  int i_dis_curr = display();
      //}
    //}
    int i_dis = display();    
    set_b_u_show_all(false);
  }
  | T_DIS arity T_SEM       T_EOL
  {
    // printf("parser.y, T_PRT variables T_SEM T_EOL\n");
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>3);
    node *n_arity = make_node(e_OPTS, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    //printf("parser.y, T_DIS, $<snd>2->right->p_label %s\n", $<snd>2->right->p_label);
    node *n_t_dis = make_node(e_LEFT, NULL, 0.0f, n_arity, NULL, $<snd>1);
    $$=n_t_dis;

    p_p_show = get_p_u_show();
    xfree_records(p_p_show);
    p_p_show = make_record(NULL, 0.0f, NULL);
    set_p_u_show(p_p_show);

//    p_p_eqns = get_p_u_eqns();
//    p_p_eqns = make_record(n_defin->right->right->p_label, 0.0f, p_p_eqns);
//    set_p_u_eqns(p_p_eqns);

    p_p_OPT = get_p_u_OPT();
    p_p_OPT = make_list(n_arity, p_p_OPT);
    set_p_u_OPT(p_p_OPT);

    b_p_inta = get_b_u_inta();
    /* differenciate between interactive and parse only */
    if ( b_p_inta )
    {
      /* printf("parser.y, T_DIS arity T_SEM T_EOL, b_p_inta: ");
       * printf("%s", b_p_inta ? "true" : "false");
       * printf("\n");
       */
      /* temporarly changed to
       * int i_dis_inta = display();
       */
      /* not in dot mode */
      b_p_dots = get_b_u_dots();
      if ( b_p_dots == false ) // display();
      {
        int i_dis_curr = display();
      }
    }
    else
    {
      /* printf("parser.y, T_DIS arity T_SEM T_EOL, b_p_inta: ");
       * printf("%s", b_p_inta ? "true" : "false");
       * printf("\n");
       */
      b_p_dots = get_b_u_dots();
      if ( b_p_dots == false ) // display();
      {
        int i_dis_curr = display();
      }
    }
    xfree_AST_list(p_p_OPT);
    p_p_OPT = NULL;
    p_p_OPT = make_list(NULL, NULL);
    set_p_u_OPT(p_p_OPT);
  }
  | T_DIS arity T_SEM commt T_EOL
  {
    // printf("parser.y, T_PRT variables T_SEM T_EOL\n");
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>5, NULL);
    node *n_commt = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>4);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_commt, NULL, $<snd>3);
    node *n_arity = make_node(e_OPTS, NULL, 0.0f, $<snd>2, NULL, n_t_sem);
    /* printf("parser.y, dis, $<snd>2->center->p_label %s\n",           \
      $<snd>2->center->p_label); */
    node *n_t_dis = make_node(e_LEFT, NULL, 0.0f, n_arity, NULL, $<snd>1);
    $$=n_t_dis;

    p_p_show = get_p_u_show();
    xfree_records(p_p_show);
    p_p_show = make_record(NULL, 0.0f, NULL);
    set_p_u_show(p_p_show);

    p_p_OPT = get_p_u_OPT();
    p_p_OPT = make_list(n_arity, p_p_OPT);
    set_p_u_OPT(p_p_OPT);

    b_p_dots = get_b_u_dots();
    if ( b_p_dots == false ) // display();
    {
      int i_dis = display();
    }

    xfree_AST_list(p_p_OPT);
    p_p_OPT = NULL;
    p_p_OPT = make_list(NULL, NULL);
    set_p_u_OPT(p_p_OPT);
  }
/* del x_x; */
  | T_DEL T_IDS T_SEM       T_EOL
  {
    // printf("parser.y, T_DEL T_IDS T_SEM T_EOL\n");
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>3);
    node *n_t_ids = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    node *n_t_del = make_node(e_OPTS, NULL, 0.0f, n_t_ids, NULL, $<snd>1);
    $$=n_t_del;
    char *p_delete_key = $<snd>2->p_label;
    p_p_init = get_p_u_init();
    p_p_init = del_record(p_p_init, p_delete_key);
    set_p_u_init(p_p_init);
    // delete from eqns records    
    p_p_eqns = get_p_u_eqns();
    p_p_eqns = del_record(p_p_eqns, p_delete_key);
    set_p_u_eqns(p_p_eqns);
  }
  | T_DEL T_IDS T_SEM commt T_EOL
  {
    // printf("parser.y, T_DEL T_IDS T_SEM commt T_EOL\n");
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>5, NULL);
    node *n_commt = make_node(e_NONE, NULL, 0.0f, n_t_eol, NULL, $<snd>4);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_commt, NULL, $<snd>3);
    node *n_t_ids = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>2);
    node *n_t_del = make_node(e_OPTS, NULL, 0.0f, n_t_ids, NULL, $<snd>1);
    $$=n_t_del;
    char *p_delete_key = $<snd>2->p_label;
    p_p_init = get_p_u_init();
    p_p_init = del_record(p_p_init, p_delete_key);
    set_p_u_init(p_p_init);
  }
/* xit; */
  | T_XIT T_SEM       T_EOL
  {
    // printf("parser.y, T_XIT T_SEM T_EOL\n");
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>3, NULL);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_t_eol, NULL, $<snd>2);
    node *n_t_xit = make_node(e_OPTS, NULL, 0.0f, n_t_sem, NULL, $<snd>1);
    $$=n_t_xit;
    // TODO: set exit bool;
    // printf("parser.y, T_XIT, set exit boot and leave in main.\n");

    /* set exit switch to 1 */
    b_p_exit = true;
    set_b_u_exit(b_p_exit);
    YYABORT;
  }
  | T_XIT T_SEM commt T_EOL
  {
    // printf("parser.y, T_XIT T_SEM commt T_EOL\n");
    node *n_t_eol = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_commt = make_node(e_NONE, NULL, 0.0f, n_t_eol, NULL, $<snd>3);
    node *n_t_sem = make_node(e_LEFT, NULL, 0.0f, n_commt, NULL, $<snd>2);
    node *n_t_xit = make_node(e_LEFT, NULL, 0.0f, n_t_sem, NULL, $<snd>1);
    $$=n_t_xit;
    // TODO: set exit bool;
    // printf("parser.y, T_XIT, set exit boot and leave in main.\n");

    /* set exit switch to 1 */
    b_p_exit = true;
    set_b_u_exit(b_p_exit);
    YYABORT;
  }
  | T_RST T_SEM commt T_EOL
  {
    //post_parse();
    //pre_parse();
    /* set exit switch to 1 */
//    b_p_exit = true;
//    set_b_u_exit(b_p_exit);

    b_p_reset = true;
    set_b_u_reset(b_p_reset);
        
    YYABORT;
  }
  | T_RST T_SEM       T_EOL
  {
    //post_parse();    
    //pre_parse();
    /* set exit switch to 1 */
//    b_p_exit = true;
//    set_b_u_exit(b_p_exit);

    b_p_reset = true;
    set_b_u_reset(b_p_reset);
    YYABORT;
  }
;

/*
 * a = 2.00, b = 3.00
 */
defin:
    T_IDS T_DEF expre
  {
    node *n_expre = make_node(e_EXPR, NULL, 0.0f, NULL, $<snd>3, NULL);
    node *n_t_def = make_node(e_LEFT, NULL, 0.0f, n_expre, NULL, $<snd>2);
    node *n_t_ids = make_node(e_LEFT, NULL, 0.0f, n_t_def, NULL, $<snd>1);
    $$=n_t_ids;

    /*
    printf("parser.y, define, test print_keys and values..\n");
    print_keys(p_p_init);
    print_values(p_p_init);
    printf("parser.y, define, test print_keys and values done!\n");
    */

    /*
     * if variable (T_IDS) is not a valid key in the key/value store p_p_init,
     * value for expression (expre) will be evaluated, label extracted,
     * and the key/value will be stored in p_p_init using make_record.
     *
     * TODO: maybe, add an alternative else branch (for updating key)
     */
    /* get mode first interactive or parse */
    // b_p_inta = get_b_u_inta();
    p_p_init = get_p_u_init();
    /* standard to set values */
    if( false == is_valid_key(p_p_init, $<snd>1->p_label) )
    {
      // printf("parser.y, defin, before eval\n");
      float f_expre = eval(n_expre, p_p_init);
      // printf("parser.y, defin, f_expre: %f\n", f_expre);
      char *p_new_labelA = (char *) calloc(1, strlen($<snd>1->p_label) +1);
      strcpy(p_new_labelA, $<snd>1->p_label);
      // printf("parser.y, defin, p_new_labelA %s\n", p_new_labelA);
      p_p_init = make_record(p_new_labelA, f_expre, p_p_init);
      set_p_u_init(p_p_init);
    }
    else
    {
      /* if interactive, let variables values be updateable */
      //if ( b_p_inta == true )
      //{
      //  float f_expre = eval(n_expre, p_p_init);
      //  p_p_init = update_record(p_p_init, $<snd>1->p_label, f_expre);
      //  set_p_u_init(p_p_init);
      //}
    }

    // printf("parser.y, defin, T_IDS: %s\n",$<snd>1->p_label);

    /* p_p_AST starts here */
    char *p_new_labelC = (char *) calloc(1, strlen($<snd>1->p_label) +1);
    strcpy(p_new_labelC, $<snd>1->p_label);

    /* another way copying the label using strdup()
    printf("parser.y, defin, p_new_labelC AST %s\n", p_new_labelC);
    p_new_label = strdup($<snd>1->p_label);
    node *n_id_define_expr = make_node(e_EXPR, $<snd>1->p_label, 0.0f, NULL, $<snd>3 ,NULL);
    */

    node *n_three = (node *) $<snd>3;
    node *n_id_define_expr = make_node(e_EXPR, p_new_labelC, 111.11f, NULL, n_three, NULL);
    p_p_AST = get_p_u_AST();
    p_p_AST = make_list(n_id_define_expr, p_p_AST);
    set_p_u_AST(p_p_AST);
  }
  | defin T_CMA T_IDS T_DEF expre
  {
    node *n_expre = make_node(e_EXPR, NULL, 0.0f, NULL, $<snd>5, NULL);
    node *n_t_def = make_node(e_LEFT, NULL, 0.0f, n_expre, NULL, $<snd>4);
    node *n_t_ids = make_node(e_LEFT, NULL, 0.0f, n_t_def, NULL, $<snd>3);
    node *n_t_cma = make_node(e_LEFT, NULL, 0.0f, n_t_ids, NULL, $<snd>2);
    node *n_defin = make_node(e_LEFT, NULL, 0.0f, n_t_cma, NULL, $<snd>1);

    /*
     * obsolete notice.
     * p_nodes = make_list(n_expre, p_nodes);
     * p_nodes changed and renamed to p_pp_nodes
     */

    $$=n_defin;
    float f_expre = eval(n_expre, p_p_init);

    /*
     * ..see above
     * if variable (T_IDS) is not a valid key in the key/value store p_p_init,
     * value for expression (expre) will be evaluated, label extracted,
     * and the key/value will be stored in p_p_init using make_record.
     *
     * TODO: maybe, add an alternative else branch (for updating key)
     */
    p_p_init = get_p_u_init();
    if ( false == is_valid_key(p_p_init, $<snd>3->p_label) )
    {
      char *p_new_labelB = (char *) calloc(1, strlen($<snd>3->p_label) +1);
      strcpy(p_new_labelB, $<snd>3->p_label);
      // printf("parser.y, defin, p_new_labelB%s\n", p_new_labelB);
      p_p_init = make_record(p_new_labelB, f_expre, p_p_init);
      set_p_u_init(p_p_init);
    }
  }
;

/*
 * arity (German: Stelligkeit); n-value as function parameters
 * i.e. min(4,2,..,n)
 */
arity:
    expre
  {
    node *n_expre = make_node(e_EXPR, NULL, 0.0f, NULL, $<snd>1, NULL);
    $$=n_expre;
  }
  | arity T_CMA expre
  {
    node *n_expre = make_node(e_EXPR, NULL, 0.0f, NULL, $<snd>3, NULL);
    node *n_t_cma = make_node(e_AND_, NULL, 0.0f, n_expre, NULL, $<snd>2);
    node *n_arity = make_node(e_LEFT, NULL, 0.0f, $<snd>1, NULL, n_t_cma);
    $$=n_arity;
  }
;


/*
 * fun variance(array, length): varia
 * fun varia = variance(array, length)
 */
/*
fhead:
    T_FUN T_IDS T_LPS arity T_RPS T_CLN T_IDS T_EOL
  | T_FUN T_IDS T_SAM T_IDS T_LPS arity T_RPS T_EOL
;
*/

/*fbody:
    commt
  | expre T_SEM       T_EOL
  | expre             T_EOL
  | fbody expre T_SEM T_EOL
  | fbody expre       T_EOL
  | defin T_SEM       T_EOL
  | defin             T_EOL
  | fbody defin T_SEM T_EOL
  | fbody defin       T_EOL
;
*/

/*
ffoot:
    _end_
;*/

/*funXZ:*/
  /*  fhead  */ /* _end_*/
    /*fhead*/ /*fbody*/ /* _end_ */
/*  | fbody*/
/*;  */

funct:
/*  standard function
 *  i.e. a-function-name()
 */
    T_IDS T_LPS       T_RPS
/*  standard function
 *  i.e. a-function-name(f, o, r)
 */

/* does not harmonize with "lt"|"LT"
        or                 "gt"|"GT"
*/
  | T_IDS T_LPS arity T_RPS
  {
  }
  | T_EUL T_LPS       T_RPS
  {
    node *n_t_rps = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>3, NULL);
    node *n_t_lps = make_node(e_LEFT, NULL, 0.0f, n_t_rps, NULL, $<snd>2);
    node *n_t_eul = make_node(e_EUL_, NULL, 0.0f, n_t_lps, NULL, $<snd>1);
    $$=n_t_eul;
    // float f_expre = eval(n_t_eul, p_p_init);
    // printf("parser.y, funct, e(): %f\n", f_expre);
  }
  | T_RNG T_LPS T_DGT T_CMA T_DGT             T_RPS
  {
    node *n_t_rps = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>6, NULL);
    node *n_t_dgB = make_node(e_LEFT, NULL, 0.0f, n_t_rps, NULL, $<snd>5);
    node *n_t_cma = make_node(e_LEFT, NULL, 0.0f, n_t_dgB, NULL, $<snd>4);
    node *n_t_dgA = make_node(e_LEFT, NULL, 0.0f, n_t_cma, NULL, $<snd>3);
    node *n_t_lps = make_node(e_LEFT, NULL, 0.0f, n_t_dgA, NULL, $<snd>2);
    node *n_t_rng = make_node(e_LEFT, NULL, 0.0f, n_t_lps, NULL, $<snd>1);
    $$=n_t_rng;
    // float f_test_ = eval(n_t_rng, p_p_init);
    // printf("parser.y, funct, min(): %f\n", f_test_);
  }
  | T_RNG T_LPS T_DGT T_CMA T_DGT T_CMA T_DGT T_RPS
  {
    node *n_t_rps = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>8, NULL);
    node *n_t_dgC = make_node(e_LEFT, NULL, 0.0f, n_t_rps, NULL, $<snd>7);
    node *n_t_cmB = make_node(e_LEFT, NULL, 0.0f, n_t_dgC, NULL, $<snd>6);
    node *n_t_dgB = make_node(e_LEFT, NULL, 0.0f, n_t_cmB, NULL, $<snd>5);
    node *n_t_cmA = make_node(e_LEFT, NULL, 0.0f, n_t_dgB, NULL, $<snd>4);
    node *n_t_dgA = make_node(e_LEFT, NULL, 0.0f, n_t_cmA, NULL, $<snd>3);
    node *n_t_lps = make_node(e_LEFT, NULL, 0.0f, n_t_dgA, NULL, $<snd>2);
    node *n_t_rng = make_node(e_LEFT, NULL, 0.0f, n_t_lps, NULL, $<snd>1);
    $$=n_t_rng;
    // float f_test_ = eval(n_t_rng, p_p_init);
    // feature: range for steps; range(0, 35);
    // printf("parser.y, funct, min(): %f\n", f_test_);
  }
  | T_MIN T_LPS arity T_RPS
  {
    node *n_t_rps = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n__stel = make_node(e_MIN_, NULL, 0.0f, $<snd>3, NULL, n_t_rps);
    node *n_t_lps = make_node(e_LEFT, NULL, 0.0f, n__stel, NULL, $<snd>2);
    node *n_t_min = make_node(e_LEFT, NULL, 0.0f, n_t_lps, NULL, $<snd>1);
    $$=n_t_min;
    // float f_test_ = eval(n_t_min, p_p_init);
    // printf("parser.y, funct, min(): %f\n", f_test_);
  }
  | T_MAX T_LPS arity T_RPS
  {
    node *n_t_rps = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_arity = make_node(e_MAX_, NULL, 0.0f, $<snd>3, NULL, n_t_rps);
    node *n_t_lps = make_node(e_LEFT, NULL, 0.0f, n_arity, NULL, $<snd>2);
    node *n_t_max = make_node(e_LEFT, NULL, 0.0f, n_t_lps, NULL, $<snd>1);
    $$=n_t_max;
    // float f_test_ = eval(n_t_max, p_p_init);
    // printf("parser.y, funct, max(): %f\n", f_test_);
  }
  | T_RR_ T_LPS arity T_RPS
  {
    node *n_t_rps = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_arity = make_node(e_RR__, NULL, 0.0f, $<snd>3, NULL, n_t_rps);
    node *n_t_lps = make_node(e_LEFT, NULL, 0.0f, n_arity, NULL, $<snd>2);
    node *n_t_rr_ = make_node(e_LEFT, NULL, 0.0f, n_t_lps, NULL, $<snd>1);
    $$=n_t_rr_;
    // float f_test_ = eval(n_t_rr_, p_p_init);
    // printf("parser.y, funct, rrange(): %f\n", f_test_);
  }
  | T_SUM T_LPS arity T_RPS
  {
    node *n_t_rps = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_arity = make_node(e_SUM_, NULL, 0.0f, $<snd>3, NULL, n_t_rps);
    node *n_t_lps = make_node(e_LEFT, NULL, 0.0f, n_arity, NULL, $<snd>2);
    /* e_LEFT - evaluate left node only*/
    node *n_t_sum = make_node(e_LEFT, NULL, 0.0f, n_t_lps, NULL, $<snd>1);
    $$=n_t_sum;
    // float f_test_ = eval(n_t_sum, p_p_init);
    // printf("parser.y, funct, sum(): %f\n", f_test_);
  }
  | T_SIN T_LPS arity T_RPS
  {
    node *n_t_rps = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_arity = make_node(e_SIN_, NULL, 0.0f, $<snd>3, NULL, n_t_rps);
    node *n_t_lps = make_node(e_LEFT, NULL, 0.0f, n_arity, NULL, $<snd>2);
    node *n_t_sin = make_node(e_LEFT, NULL, 0.0f, n_t_lps, NULL, $<snd>1);
    $$=n_t_sin;
    // float f_test_ = eval(n_t_sin, p_p_init);
    // printf("parser.y, funct, sin(): %f\n", f_test_);
  }
  | T_COS T_LPS arity T_RPS
  {
    node *n_t_rps = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_arity = make_node(e_COS_, NULL, 0.0f, $<snd>3, NULL, n_t_rps);
    node *n_t_lps = make_node(e_LEFT, NULL, 0.0f, n_arity, NULL, $<snd>2);
    node *n_t_cos = make_node(e_LEFT, NULL, 0.0f, n_t_lps, NULL, $<snd>1);
    $$=n_t_cos;
    // float f_test_ = eval(n_t_cos, p_p_init);
    // printf("parser.y, funct, cos(): %f\n", f_test_);
  }
  | T_MOI T_LPS arity T_RPS
  {
    node *n_t_rps = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_arity = make_node(e_MOID, NULL, 0.0f, $<snd>3, NULL, n_t_rps);
    node *n_t_lps = make_node(e_LEFT, NULL, 0.0f, n_arity, NULL, $<snd>2);
    node *n_t_moi = make_node(e_LEFT, NULL, 0.0f, n_t_lps, NULL, $<snd>1);
    $$=n_t_moi;
    // float f_test_ = eval(n_t_moi, p_p_init);
    // printf("parser.y, funct, sigmoid(): %f\n", f_test_);
  }
  | T_MEA T_LPS arity T_RPS
  {
    node *n_t_rps = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_arity = make_node(e_MEAN, NULL, 0.0f, $<snd>3, NULL, n_t_rps);
    node *n_t_lps = make_node(e_LEFT, NULL, 0.0f, n_arity, NULL, $<snd>2);
    node *n_t_mea = make_node(e_LEFT, NULL, 0.0f, n_t_lps, NULL, $<snd>1);
    $$=n_t_mea;
    // float f_test_ = eval(n_t_mea, p_p_init);
    // printf("parser.y, funct, mean(): %f\n", f_test_);
  }
  | T_ABS T_LPS expre T_RPS
  {
    node *n_t_rps = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n__expr = make_node(e_ABS_, NULL, 0.0f, $<snd>3, NULL, n_t_rps);
    node *n_t_lps = make_node(e_LEFT, NULL, 0.0f, n__expr, NULL, $<snd>2);
    node *n_t_abs = make_node(e_LEFT, NULL, 0.0f, n_t_lps, NULL, $<snd>1);
    $$=n_t_abs;
    // float f_test_ = eval(n_t_srt, p_p_init);
    // printf("parser.y, funct, sqrt(): %f\n", f_test_);
  }
  | T_SRT T_LPS expre T_RPS
  {
    node *n_t_rps = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n__expr = make_node(e_SQRT, NULL, 0.0f, $<snd>3, NULL, n_t_rps);
    node *n_t_lps = make_node(e_LEFT, NULL, 0.0f, n__expr, NULL, $<snd>2);
    node *n_t_srt = make_node(e_LEFT, NULL, 0.0f, n_t_lps, NULL, $<snd>1);
    $$=n_t_srt;
    // float f_test_ = eval(n_t_srt, p_p_init);
    // printf("parser.y, funct, sqrt(): %f\n", f_test_);
  }
  | T_DEV T_LPS arity T_RPS
  {
    node *n_t_rps = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_arity = make_node(e_DEV_, NULL, 0.0f, $<snd>3, NULL, n_t_rps);
    node *n_t_lps = make_node(e_LEFT, NULL, 0.0f, n_arity, NULL, $<snd>2);
    node *n_t_dev = make_node(e_LEFT, NULL, 0.0f, n_t_lps, NULL, $<snd>1);
    $$=n_t_dev;
    // float f_test_ = eval(n_t_dev, p_p_init);
    // printf("parser.y, funct, stdev(): %f\n", f_test_);
  }
  | T_DN_ T_LPS arity T_RPS
  {
    node *n_t_rps = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>4, NULL);
    node *n_arity = make_node(e_DN__, NULL, 0.0f, $<snd>3, NULL, n_t_rps);
    node *n_t_lps = make_node(e_LEFT, NULL, 0.0f, n_arity, NULL, $<snd>2);
    node *n_t_dn_ = make_node(e_LEFT, NULL, 0.0f, n_t_lps, NULL, $<snd>1);
    $$=n_t_dn_;
    // float f_test_ = eval(n_t_dn_, p_p_init);
    // printf("parser.y, funct, dnorm(): %f\n", f_test_);
  }
  | T_PI_ T_LPS       T_RPS
  {
    node *n_t_rps = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>3, NULL);
    node *n_t_lps = make_node(e_LEFT, NULL, 0.0f, n_t_rps, NULL, $<snd>2);
    node *n_t_pi_ = make_node(e_PI__, NULL, 0.0f, n_t_lps, NULL, $<snd>1);
    $$=n_t_pi_;
    // float f_expre = eval(n_t_pi_, p_p_init);
    // printf("parser.y, funct, pi(): %f\n", f_expre);
  }
  | T_RND T_LPS       T_RPS
  {
    node *n_t_rps = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>3, NULL);
    node *n_t_lps = make_node(e_LEFT, NULL, 0.0f, n_t_rps, NULL, $<snd>2);
    node *n_t_ran = make_node(e_RAND, NULL, 0.0f, n_t_lps, NULL, $<snd>1);
    $$=n_t_ran;
    // float f_test_ = eval(n_t_ran, p_p_init);
    // printf("parser.y, funct, rand(): %f\n", f_test_);
  }
  | T_ITE T_LPS expre T_CMA expre T_CMA expre T_RPS
  {
    node *n_t_rps = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>8, NULL);
    node *n_t_exC = make_node(e_LEFT, NULL, 0.0f, n_t_rps, NULL, $<snd>7);
    node *n_t_cmB = make_node(e_LEFT, NULL, 0.0f, n_t_exC, NULL, $<snd>6);
    node *n_t_exB = make_node(e_LEFT, NULL, 0.0f, n_t_cmB, NULL, $<snd>5);
    node *n_t_cmA = make_node(e_LEFT, NULL, 0.0f, n_t_exB, NULL, $<snd>4);
    node *n_t_exA = make_node(e_LEFT, NULL, 0.0f, n_t_cmA, NULL, $<snd>3);
    node *n_t_lps = make_node(e_LEFT, NULL, 0.0f, n_t_exA, NULL, $<snd>2);
    node *n_t_ite = make_node(e_ITE_, NULL, 0.0f, n_t_lps, NULL, $<snd>1);
    $$=n_t_ite;
  }
  | T_LT_ T_LPS expre T_CMA expre T_RPS
  {
    node *n_t_rps = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>6, NULL);
    node *n_t_exB = make_node(e_LEFT, NULL, 0.0f, n_t_rps, NULL, $<snd>5);
    node *n_t_cma = make_node(e_LEFT, NULL, 0.0f, n_t_exB, NULL, $<snd>4);
    node *n_t_exA = make_node(e_LEFT, NULL, 0.0f, n_t_cma, NULL, $<snd>3);
    node *n_t_lps = make_node(e_LEFT, NULL, 0.0f, n_t_exA, NULL, $<snd>2);
    node *n_t_lt_ = make_node(e_LT__, NULL, 0.0f, n_t_lps, NULL, $<snd>1);
    $$=n_t_lt_;
    /* printf("parser.y, T_LT_ T_LPS expre T_CMA expre T_RPS\n"); */
  }
  | T_GT_ T_LPS expre T_CMA expre T_RPS
  {
    node *n_t_rps = make_node(e_NONE, NULL, 0.0f, NULL, $<snd>6, NULL);
    node *n_t_exB = make_node(e_LEFT, NULL, 0.0f, n_t_rps, NULL, $<snd>5);
    node *n_t_cma = make_node(e_LEFT, NULL, 0.0f, n_t_exB, NULL, $<snd>4);
    node *n_t_exA = make_node(e_LEFT, NULL, 0.0f, n_t_cma, NULL, $<snd>3);
    node *n_t_lps = make_node(e_LEFT, NULL, 0.0f, n_t_exA, NULL, $<snd>2);
    node *n_t_gt_ = make_node(e_GT__, NULL, 0.0f, n_t_lps, NULL, $<snd>1);
    $$=n_t_gt_;
    // printf("parser.y, $<snd>3->p_label: %s\n", $<snd>3->p_label);
    b_p_pars = get_b_u_pars();
    if ( b_p_pars ) // display();
    {
      // printf("parser.y, Option -p flag b_p_pars, b_u_pars, WORKS! \n");
      // printf("parser.y, T_GT_ T_LPS expre T_CMA expre T_RPS\n");
    }
  }
;

expre:
    T_DGT
  {
    // p_label = strcpy(p_label, "T_DGT");
    node *n_t_dgt = make_node(e_EXPR, NULL, 0.0f, NULL, $<snd>1, NULL);
    // printf("parser.y, expre, T_DGT: %f  \n", $<snd>1->f_value);
    $$=n_t_dgt;
  }
  | T_IDS
  {
    node *n_t_ids = make_node(e_EXPR, NULL, 0.0f, NULL, $<snd>1, NULL);
    $$=n_t_ids;
  }
  | T_STR
  | T_LPS expre T_RPS
  {
    node *n_expre = make_node(e_BRAC, NULL, 0.0f, $<snd>1, $<snd>2, $<snd>3);
    $$=n_expre;
    // float f_expre = eval(n_expre, p_p_init);
    // printf("parser.y, expre, ( %f )\n", f_expre);
  }
  | expre T_ADD expre
  {
    node *n_expre_t_add_expre = make_node(e_ADD_, NULL, 0.0f, $<snd>1, $<snd>2, $<snd>3);
    $$=n_expre_t_add_expre;
    // float f_expre = eval(n_expre_t_add_expre, p_p_init);
    // printf("parser.y, expre, add: %f\n", f_expre);
  }
  | expre T_SUB expre
  {
    node *n_expre_t_sub_expre = make_node(e_SUB_, NULL, 0.0f, $<snd>1, $<snd>2, $<snd>3);
    $$=n_expre_t_sub_expre;
    // float f_expre = eval(n_expre_t_sub_expre, p_p_init);
    // printf("parser.y, expre, sub: %f\n", f_expre);
  }
  | expre T_MUL expre /* { $$ = $1 * $3; } */
  {
    node *n_expre_t_mul_expre = make_node(e_MUL_, NULL, 0.0f, $<snd>1, $<snd>2, $<snd>3);
    $$=n_expre_t_mul_expre;
    // float f_expre = eval(n_expre_t_mul_expre, p_p_init);
    // printf("parser.y, expre, mul: %f\n", f_expre);
  /* 1 - 2 * 3; */
  }
  | expre T_DIV expre
  {
    node *n_expre_t_div_expre = make_node(e_DIV_, NULL, 0.0f, $<snd>1, $<snd>2, $<snd>3);
    $$=n_expre_t_div_expre;
    // float f_expre = eval(n_expre_t_div_expre, p_p_init);
    // printf("parser.y, expre, div: %f\n", f_expre);
  }
/*
  | expre T_LT_ expre
  {
    node *n_expre_t_lt_expre = make_node(e_LT__, NULL, 0.0f, $<snd>1, $<snd>2, $<snd>3);
    $$=n_expre_t_lt_expre;
  }
  | expre T_GT_ expre
  {
    node *n_expre_t_gt_expre = make_node(e_GT__, NULL, 0.0f, $<snd>1, $<snd>2, $<snd>3);
    $$=n_expre_t_gt_expre;
  }
*/
  | expre T_MOD expre
  {
    node *n_expre_t_mod_expre = make_node(e_MOD_, NULL, 0.0f, $<snd>1, $<snd>2, $<snd>3);
    $$=n_expre_t_mod_expre;
    // float f_expre = eval(n_expre_t_mod_expre, p_p_init);
    // printf("parser.y, expre, mod: %f\n", f_expre);
  }
  | T_SUB expre %prec UMINUS
  {
    node *n_expre = make_node(e_SUB_, NULL, 0.0f, NULL, $<snd>2, NULL);
    node *n_t_sub = make_node(e_LEFT, NULL, 0.0f, n_expre, NULL, $<snd>1);
    $$=n_t_sub;
    // float f_expre = eval(n_t_sub, p_p_init);
    // printf("parser.y, expre, div: %f\n", f_expre);
  }
/*
 * functions are expressions (see below)
 * 'All functions (at least the formulas represented by them)
 * are expressions, but not all expressions are functions.'
 * url:      https://math.stackexchange.com/questions/2670634/difference-between-expression-and-function
 * url:      https://www-fourier.ujf-grenoble.fr/~parisse/giac/doc/en/cascmd_en/node139.html
 * url:      https://www.wias-berlin.de/events/Leibniz16/thiele.pdf
 * accessed: 2022-01-21T16_52_40Z mrosner
 *
 * see funct above
 * | T_IDS T_LPS T_RPS
 *
 * next line remains
  | expre T_DEF expre
 *
 * added funct to expre
 */
  | funct
;

commt:
    T_CMT
  | T_C_S T_C_E
;

influ:
    T_NEU
  | T_POS
  | T_NEG
;

%%

void yyerror(char const *msg)
{
  fprintf(stderr, "Line: %d; %s.\n", yylineno, msg);
}
/*
void yyabort(char const *msg)
{
  fprintf("%s", msg);
  exit(0);
}
*/

int yywrap()
{
  return 1;
}

/* $ASDPN3: src/parser.y,v 2022Q4 2022/10/05 11:25:09 mer Exp $ */
