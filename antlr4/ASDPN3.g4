/*******************************************************************************
* Name        : ASDPN3/src/ASDPN3.g4                                           *
* Author      : M.E.Rosner                                                     *
* E-Mail      : marty[at]rosner[dot]io                                         *
* Version     : 2022Q3                                                         *
* Copyright   : Copyright (C) 2019-2022 M.E.Rosner; Berlin; Germany            *
* License     :                                                                *
* Description : A System Dynamics Process(ing) Notation                        *
*******************************************************************************/

grammar ASDPN3;

lines:
      line_
    | lines line_
    | funde
    | lines funde
    ;

line_:
      T_EOL
    /*| simXZ*/
    /* function signature */  
    | T_FUN T_IDS T_LPS arity T_RPS T_CLN T_IDS T_CLN T_EOL
    | T_FUN T_IDS T_DEF T_IDS T_LPS       T_RPS T_CLN T_EOL  
    | T_FUN T_IDS T_DEF T_IDS T_LPS arity T_RPS T_CLN T_EOL  
    /* for value in array */
    | T_FOR T_IDS T__IN T_IDS T_CLN
    /* define line after in function */
    | defin             T_SEM       T_EOL
    | defin             T_SEM commt T_EOL  
    /* end - _end_ */
    | T_END T_SEM       T_EOL
    | T_END T_SEM commt T_EOL  
    /* comment */
    | commt T_EOL
    | expre T_SEM       T_EOL
    | expre T_SEM commt T_EOL
  /*
   * TODO: add self-defined functions
   * basic testing purpose (obsolete, but remains)
   *  | expre T_EOL
   *  | stats T_EOL
   *  | param T_EOL
   *  | funct T_EOL
   *
   * teacup = def init();
   *  | T_IDS T_DEF T_DFN T_IDS T_LPS       T_RPS T_SEM T_EOL
   *  | T_IDS T_DEF T_DFN T_IDS T_LPS       T_RPS T_SEM commt T_EOL 
   *
   * TODO: signatures for functions
   * | param T_EOL
   * | param commt T_EOL
   *
   * a, b, c, d = init()                          # "with n=20 steps"
   * a, b = teacup(a, b, c, d)                    # run model teacup
   *
   * | param T_DEF funct
   *
   * TODO: arity newline break
   * a, b = pred(a, b, c, d, e,
   *             f, g, h, i, j,
   *             l, m, n, o )
   * | fpart T_EOL
   * | fpart commt T_EOL
   * | param T_CMA T_EOL
   * | param T_CMA commt T_EOL
   */
  /* mod teacup; */
    | T_MDL T_IDS T_SEM       T_EOL
    | T_MDL T_IDS T_SEM commt T_EOL  
  /* begin model teacup; */
    | T_BEG T_MDL T_IDS T_SEM       T_EOL
    | T_BEG T_MDL T_IDS T_SEM commt T_EOL  
  /* end; */
  /*  | _end_ */
  /* end model; */
    | T_END T_MDL       T_SEM T_EOL
    | T_END T_MDL       T_SEM commt T_EOL  
  /* cloud  sink_01;                              # A sink node */
    | T_CLD arity T_SEM       T_EOL
    | T_CLD arity T_SEM commt T_EOL
  /*
   * overview lines
   *
   * mod teacup;
   * stock   a = 95.0;                            # temperature
   * flow    b;                                   # heat loss to room 
   * auxil   x;                                   # x (auxiliary)
   * param   c = 70.0;                            # room temperature 
   * param   d = 10.0;                            # characteristic time
   * eqn     b = ( a - c ) / d;                   # equations 
   * eqn     a = a - b;                           # step by step
   */
  
  /*
   * stock   a = 180.0, b = 120.0;                # stock a, b
   *
   * | T_STK T_IDS T_DEF T_DGT T_SEM       T_EOL
   * | T_STK T_IDS T_DEF T_DGT T_SEM commt T_EOL
   */
    | T_STK defin             T_SEM       T_EOL
    | T_STK defin             T_SEM commt T_EOL
  /*
   * flow    a = 2.0, b = 0.0;                    # "a - heat loss room; b"
   * flow    f_a, f_b;                            # "multiple flow variables"
   *
   * | T_FLW T_IDS T_DEF T_DGT T_SEM       T_EOL
   * | T_FLW T_IDS T_DEF T_DGT T_SEM commt T_EOL
   */  
    | T_FLW defin             T_SEM       T_EOL
    | T_FLW defin             T_SEM commt T_EOL
    | T_FLW arity             T_SEM       T_EOL
    | T_FLW arity             T_SEM commt T_EOL
  /*
   * auxil   x = 0.0, y = 2.0, z = 0.5;           # "x - an artificial int"
   * auxil   x;                                   # "x - an artificial int"
   *
   * | T_AUX T_IDS T_SEM       T_EOL
   * | T_AUX T_IDS T_SEM commt T_EOL
   * | T_AUX T_IDS T_DEF T_DGT T_SEM       T_EOL
   * | T_AUX T_IDS T_DEF T_DGT T_SEM commt T_EOL
   */
    | T_AUX defin             T_SEM       T_EOL
    | T_AUX defin             T_SEM commt T_EOL
    | T_AUX arity             T_SEM       T_EOL
    | T_AUX arity             T_SEM commt T_EOL
  /*
   * param   a = 2.99, b = 0.34, c =  70.0;       # "c - room temperature"
   *
   * | T_PRM T_IDS T_DEF T_DGT T_SEM       T_EOL 
   * | T_PRM T_IDS T_DEF T_DGT T_SEM commt T_EOL
   */
    | T_PRM defin              T_SEM       T_EOL
    | T_PRM defin              T_SEM commt T_EOL
    | T_PRM arity             T_SEM       T_EOL
    | T_PRM arity             T_SEM commt T_EOL
  /*
   * equal   b = a / d - c / d                    # "equation for f_heat node"
   *                                              # serving both using param
   * | T_EQN arity T_SEM       T_EOL
   * | T_EQN arity T_SEM commt T_EOL
   */
    | T_EQN defin             T_SEM       T_EOL
    | T_EQN defin             T_SEM commt T_EOL
  /*
   * edge   from TeaC to Heat with negative influence;
   * edge   from Heat to Sink_01 with negative influence;
   * edge   from Char to Heat;
   * edge   from Room to Heat;
   */  
    | T_NET T_ARC T_FRM T_IDS T__TO T_IDS                   T_SEM       T_EOL
    | T_NET T_ARC T_FRM T_IDS T__TO T_IDS                   T_SEM commt T_EOL
    | T_NET T_ARC T_FRM T_IDS T__TO T_IDS T_WTH T_NEU T_INF T_SEM       T_EOL  
    | T_NET T_ARC T_FRM T_IDS T__TO T_IDS T_WTH T_NEU T_INF T_SEM commt T_EOL
    | T_NET T_ARC T_FRM T_IDS T__TO T_IDS T_WTH T_NEG T_INF T_SEM       T_EOL
    | T_NET T_ARC T_FRM T_IDS T__TO T_IDS T_WTH T_NEG T_INF T_SEM commt T_EOL
    | T_NET T_ARC T_FRM T_IDS T__TO T_IDS T_WTH T_POS T_INF T_SEM       T_EOL
    | T_NET T_ARC T_FRM T_IDS T__TO T_IDS T_WTH T_POS T_INF T_SEM commt T_EOL
  /*
   * net    arc( s_a, f_b );                      # a comment
   */
    | T_NET T_ARC T_LPS T_IDS T_CMA T_IDS             T_RPS T_SEM       T_EOL
    | T_NET T_ARC T_LPS T_IDS T_CMA T_IDS             T_RPS T_SEM commt T_EOL
  /*
   * net    arc( s_a, f_b, pos );                 # a comment
   */
    | T_NET T_ARC T_LPS T_IDS T_CMA T_IDS T_CMA influ T_RPS T_SEM       T_EOL
    | T_NET T_ARC T_LPS T_IDS T_CMA T_IDS T_CMA influ T_RPS T_SEM commt T_EOL
  /*
   * opt    teacup;                               # some options
   */
    | T_OPT T_IDS T_SEM
    | T_OPT T_IDS commt T_SEM
  /*
   * time   steps = 45;
   * time   steps = 4..45;
   */
    | T_TIM T_IDS T_DEF T_DGT             T_SEM       T_EOL
    | T_TIM T_IDS T_DEF T_DGT             T_SEM commt T_EOL
    | T_TIM T_IDS T_DEF T_DGT T_DDD T_DGT T_SEM       T_EOL
    | T_TIM T_IDS T_DEF T_DGT T_DDD T_DGT T_SEM commt T_EOL    
  /*
   * sim steps =      45;
   * sim steps = 4 .. 45;
   * sim steps = 4 to 45;
   *
   * | T_SIM T_IDS T_DEF T_DGT             T_SEM       T_EOL
   * | T_SIM T_IDS T_DEF T_DGT             T_SEM commt T_EOL
   * | T_SIM T_IDS T_DEF T_DGT T__TO T_DGT T_SEM commt T_EOL
   */
    | T_SIM defin                         T_SEM       T_EOL  
    | T_SIM defin                         T_SEM commt T_EOL
    | T_SIM T_IDS T_DEF T_DGT T_DDD T_DGT T_SEM       T_EOL
    | T_SIM T_IDS T_DEF T_DGT T_DDD T_DGT T_SEM commt T_EOL
    | T_SIM T_IDS T_DEF T_DGT T__TO T_DGT T_SEM       T_EOL
    | T_SIM T_IDS T_DEF T_DGT T__TO T_DGT T_SEM commt T_EOL
  /*
   * TODO: a way orchestrate models using in, out, in/out
   * in     a, b, c, d;
   * out    a, b;
   * inout  t, d, x;
  */
    | T__IN arity T_SEM       T_EOL
    | T__IN arity T_SEM commt T_EOL
    | T_OUT arity T_SEM       T_EOL
    | T_OUT arity T_SEM commt T_EOL
    | T_I_O arity T_SEM       T_EOL
    | T_I_O arity T_SEM commt T_EOL
  /*
   * dis x_x, x_y;
  */
    | T_DIS T_SEM commt T_EOL
    | T_DIS T_SEM       T_EOL
    | T_DIS arity T_SEM       T_EOL
    | T_DIS arity T_SEM commt T_EOL
  /* del x_x; */
    | T_DEL T_IDS T_SEM       T_EOL
    | T_DEL T_IDS T_SEM commt T_EOL
  /* xit; */
    | T_XIT T_SEM       T_EOL
    | T_XIT T_SEM commt T_EOL
    | T_RST T_SEM commt T_EOL
    | T_RST T_SEM       T_EOL
    ;

/*
 * fun pos_r = abs_rand():
 *     pos_r := abs(random()); # 1.00;
 * end;
 */
funde:
      fhead fbody ffoot
  ;
 
fhead:
      T_FUN T_IDS T_DEF T_IDS T_LPS arity T_RPS T_CLN       T_EOL
    | T_FUN T_IDS T_DEF T_IDS T_LPS       T_RPS T_CLN       T_EOL
    | T_FUN T_IDS T_DEF T_IDS T_LPS arity T_RPS T_CLN commt T_EOL
    | T_FUN T_IDS T_DEF T_IDS T_LPS       T_RPS T_CLN commt T_EOL

  ;

fbody:
                  expre T_SEM       T_EOL
    |             expre T_SEM commt T_EOL
    | T_IDS T_DEF expre T_SEM       T_EOL
    | T_IDS T_DEF expre T_SEM commt T_EOL
/*  | funct T_SEM T_EOL */
  ;

ffoot:
      T_END T_SEM       T_EOL
    | T_END T_SEM commt T_EOL
  ;

  /* 
   * a = 2.00, b = 3.00
   */
defin:
      T_IDS T_DEF expre
    | defin T_CMA T_IDS T_DEF expre
    ;    

  /* 
   * arity (German: Stelligkeit); n-value as function parameters
   * i.e. min(4,2,..,n)
   */
arity:
      expre
    | arity T_CMA expre
    ;

  /*
   * fun variance(array, length): varia
   * fun varia = variance(array, length)
   */

  /*
  fhead:
      T_FUN T_IDS T_LPS arity T_RPS T_CLN T_IDS T_EOL
    | T_FUN T_IDS T_SAM T_IDS T_LPS arity T_RPS T_EOL
    ;
  */

  /*fbody:
      commt
    | expre T_SEM       T_EOL
    | expre             T_EOL
    | fbody expre T_SEM T_EOL  
    | fbody expre       T_EOL
    | defin T_SEM       T_EOL
    | defin             T_EOL  
    | fbody defin T_SEM T_EOL
    | fbody defin       T_EOL
    ;
  */

  /*
  ffoot:
      _end_
    ;
  */

  /* funXZ: */
    /*  fhead  */ /* _end_*/
      /*fhead*/ /*fbody*/ /* _end_ */
  /*  | fbody*/
  /*  ;  */

funct:
  /*  standard function
   *  i.e. a-function-name()
   */
      T_IDS T_LPS       T_RPS
  /*  standard function
   *  i.e. a-function-name(f, o, r)
   */
    | T_IDS T_LPS arity T_RPS
    | T_EUL T_LPS       T_RPS  
    | T_RNG T_LPS T_DGT T_CMA T_DGT             T_RPS
    | T_RNG T_LPS T_DGT T_CMA T_DGT T_CMA T_DGT T_RPS
    | T_MIN T_LPS arity T_RPS
    | T_MAX T_LPS arity T_RPS
    | T_RR_ T_LPS arity T_RPS
    | T_SUM T_LPS arity T_RPS
    | T_SIN T_LPS arity T_RPS
    | T_COS T_LPS arity T_RPS
    | T_MOI T_LPS arity T_RPS
    | T_MEA T_LPS arity T_RPS
    | T_ABS T_LPS expre T_RPS
    | T_SRT T_LPS expre T_RPS
    | T_DEV T_LPS arity T_RPS
    | T_DN_ T_LPS arity T_RPS
    | T_PI_ T_LPS       T_RPS
    | T_RND T_LPS       T_RPS
    | T_ITE T_LPS expre T_CMA expre T_CMA expre T_RPS    
    ;

expre:
      T_DGT
    | T_IDS
    | T_STR
    | T_LPS expre T_RPS
    | expre T_ADD expre
    | expre T_SUB expre
    | expre T_MUL expre /* { $$ = $1 * $3; } */
    | expre T_DIV expre
    | expre T_MOD expre
    | expre T_LT_ expre    
    | expre T_GT_ expre    
  /* T_SUB expre %prec UMINUS */
    | T_SUB expre
  /*
   * functions, expressions (see below)
   * see:  https://math.stackexchange.com/questions/2670634/difference-between-expression-and-function   
   * see:  https://www-fourier.ujf-grenoble.fr/~parisse/giac/doc/en/cascmd_en/node139.html
   * see:  https://www.wias-berlin.de/events/Leibniz16/thiele.pdf
   * time: 2022-01-21T16_52_40Z mrosner
   * 
   * see funct above
   * | T_IDS T_LPS T_RPS
   * 
   * next line remains
    | expre T_DEF expre
   *
   * added funct to expre
   */
    | funct
    ;

commt:
      T_CMT
    | T_CMT str_s
    ;

influ:
      T_NEU
    | T_POS
    | T_NEG
    ;

str_s:
      str__
    | str_s str__
    ;

str__:      
      T_BEG
    | T_END
    | T_CLD
    | T_STK
    | T_FLW
    | T_AUX
    | T_PRM
    | T_EQN
    | T_FUN
    | T_ARC
    | T_FRM
    | T__TO
    | T_TIM
    | T_WTH
    | T_NEU
    | T_NEG
    | T_POS
    | T_INF
    | T_ABS
    | T_MIN
    | T_MAX
    | T_SUM
    | T_SIN
    | T_COS
    | T_SRT
    | T_DEV
    | T_DN_
    | T_MOI
    | T_MEA
    | T_EUL
    | T_PI_
    | T_RND
    | T_RNG
    | T_RR_
    //| T_EOL
    | T_DIS
    | T_NET
    | T_ALL
    | T_CMT
    | T_C_S
    | T_C_E
    | T_DDD
    | T_EXE
    | T__IN
    | T_OUT
    | T_I_O
    | T_FOR
    | T_STR
    | T_OPT
    | T_SIM
    | T_MDL
    | T_DFN
    | T_RET
    | T_DEL
    | T_XIT
    | T_RST
    | T_CLN
    | T_SEM 
    | T_CMA
    // | T_INT
    | T_DGT
    | T_IDS
    // | T_SAM
    | T_DEF
    | T_SUB
    | T_ADD
    | T_DIV
    | T_MUL
    | T_MOD
    | T_LPS
    | T_RPS
    | T_DOT
    | T_LBR
    | T_RBR
    | T_APO
    ;

T_WSP: [ \t\r]+ -> skip ; // skip spaces, tabs
    
/* start here */
T_BEG: 'begin'|'BEGIN' ;
T_END: 'end'|'END' ;
T_CLD: 'cloud'|'CLOUD'|'clo'|'CLO'|'cld'|'CLD' ;
T_STK: 'stock'|'STOCK'|'sto'|'STO'|'stk'|'STK' ;
T_FLW: 'flow'|'FLOW'|'flo'|'FLO'|'flw'|'FLW' ;
T_AUX: 'aux'|'AUX'|'auxil'|'AUXIL' ;
T_PRM: 'param'|'PARAM'|'par'|'PAR'|'prm'|'PRM' ;
T_EQN: 'eqn'|'EQN'|'equal'|'EQUAL'|'use'|'USE' ;
T_FUN: 'fun'|'FUN'|'func'|'FUNC'|'function'|'FUNCTION' ;
T_ARC: 'arc'|'ARC'|'edge'|'EDGE' ;
T_FRM: 'from'|'FROM' ;
T__TO: 'to'|'TO' ;
T_TIM: 'time'|'TIME' ;
T_WTH: 'with'|'WITH' ;
T_NEU: 'neu'|'NEU'|'neutral'|'NEUTRAL' ;
T_NEG: 'neg'|'NEG'|'negative'|'NEGATIVE' ;
T_POS: 'pos'|'POS'|'positive'|'POSITIVE' ;
T_INF: 'inf'|'INF'|'influence'|'INFLUENCE' ;
T_ABS: 'abs'|'ABS' ;
T_MIN: 'min'|'MIN'|'minimum'|'MINIMUM' ;
T_MAX: 'max'|'MAX'|'maximum'|'MAXIMUM' ;
T_SUM: 'sum'|'SUM' ;
T_SIN: 'sin'|'SIN' ;
T_COS: 'cos'|'COS' ;
T_SRT: 'sqrt'|'SQRT' ;
T_DEV: 'sd'|'stdev'|'SD'|'STDEV' ;
T_DN_: 'dn'|'dnorm'|'DN'|'DNORM' ;
T_MOI: 'sigmoid'|'SIGMOID'|'scurve'|'SCURVE' ;
T_MEA: 'mean'|'MEAN' ;
T_EUL: 'e'|'E' ;
T_PI_: 'pi'|'PI' ;
T_RND: 'rand'|'random'|'RAND'|'RANDOM' ;
T_RNG: 'range'|'RANGE'|'rng'|'RNG' ;
T_RR_: 'rrange'|'RRANGE'|'rr'|'RR'|'randrange'|'RANDRANGE' ;
T_EOL: '\n' ;

T_DIS: '%show'|'%SHOW'|'display'|'DISPLAY'|'dis'|'DIS'|'dsp'|'DSP' ;
T_NET: 'net'|'NET' ;
T_ALL: 'all'|'ALL' ;

T_CMT: '#'.? ;
T_C_S: '{' ;
T_C_E: '}' ;
T_DDD: '..' ;
T_EXE: 'exec'|'EXEC' ;
T__IN: 'in'|'IN' ;
T_OUT: 'out'|'OUT' ;
T_I_O: 'inout'|'INOUT' ;
T_FOR: 'for'|'FOR' ;
T_STR: '", "' ;
T_OPT: 'opt'|'OPT' ;
T_SIM: 'simul'|'SIMUL'|'sim'|'SIM' ;
T_MDL: 'model'|'MODEL'|'mdl'|'MDL'|'mod'|'MOD' ;
T_DFN: 'def'|'DEF'|'define'|'DEFINE' ;
T_RET: 'return'|'RETURN' ;
T_DEL: 'del'|'DEL' ;
T_XIT: 'qit'|'QIT'|'quit'|'QUIT'|'exit'|'EXIT'|'xit'|'XIT' ;
T_RST: 'reset'|'RESET'|'rst'|'RST' ;
T_CLN: ':' ;
T_SEM: ';' ;
T_CMA: ',' ;

// T_INT: [0-9]+ ;
T_DGT: [0-9]+|([0-9]*'.'[0-9]+) ;
T_IDS: [a-zA-Z]*[0-9a-zA-Z_-]*[0-9a-zA-Z_]+|[a-zA-Z]+ ;
// T_SAM: '=' ;
T_DEF: '='|':='|'<-' ;
T_SUB: '-' ;
T_ADD: '+' ;
T_DIV: '/' ;
T_MUL: '*' ;
T_MOD: '%' ;
T_LPS: '(' ;
T_RPS: ')' ;

T_DOT: '.' ;
T_LBR: '[' ;
T_RBR: ']' ;
T_APO: '\'' ;

T_LT_: '<'|'less-than'|'lt'|'LESS-THAN'|'LT' ;
T_GT_: '>'|'greater-than'|'gt'|'GREATER-THAN'|'GT' ;

T_ITE: 'if'|'IF'|'if_then_else'|'IF_THEN_ELSE'|'if-then-else'|'IF-THEN-ELSE'|'ifthenelse'|'IFTHENELSE'|'i_t_e'|'I_T_E'|'i-t-e'|'I-T-E'|'ite'|'ITE' ;

// T_ERR . ;

/* $ASDPN3: antlr4/ASDPN3.g4,v 2022Q3 2022/09/01 18:00:00 mer Exp $ */
